USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptElig]    Script Date: 18/11/2020 10:45:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptElig] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT "Eligibility"."Fileno", "Eligibility"."English"
    FROM   "CTS"."dbo"."Eligibility" "Eligibility"
    WHERE  "Eligibility"."Fileno"=@FileNo

END
