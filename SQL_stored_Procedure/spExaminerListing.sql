USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerListing]    Script Date: 18/11/2020 12:24:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerListing] @ExamID int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH examiners AS 
	(
		SELECT DISTINCT Exams.ID
		, Exams.Session
		, ClinicalSlotExaminers.ExaminerID
		, Exams.Centre
		, CONVERT(VARCHAR(10),  Exams.Examdate, 103)  AS "Examsdate"
		, ClinicalSlotExaminers.SortOrder
		, ClinicalSlot.Colour + ' GROUP' "Colour"
		, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'AM' WHEN ClinicalSlot.AMPM = 0 THEN 'PM' END AS "AM/PM"
		, 'Examining' "Type"
		, Venues.HospitalName
		FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
		INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
		INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID 
		INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		WHERE Exams.ID = @ExamID
		AND ExaminerID NOT IN (9175,9612,9613,9555,10213)

		UNION 

		SELECT DISTINCT Exams.ID
		, Exams.Session
		, ClinicalObservers.ExaminerID
		, Exams.Centre
		, CONVERT(VARCHAR(10),Exams.Examdate, 103)  AS "Examsdate"
		, '' "SortOrder"
	    , '' "Colour"
	    , CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
	    , 'Observing' "Type"
		, Venues.HospitalName
		FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
		INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		LEFT OUTER JOIN Venues On Venues.Id = ClinicalVenues.VenueID
		INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
		--INNER JOIN ClinicalObserverRotations ON ClinicalObservers.ID = ClinicalObserverRotations.ClinicalObserverID
	    --INNER JOIN ClinicalSlot ON ClinicalObserverRotations.SetNo = ClinicalSlot.ID
		WHERE Exams.ID = @ExamID
		AND ExaminerID NOT IN (9175,9612,9613,9555,10213)
	)
	SELECT "Addr_Personal"."Id"
	 ,  "Addr_Personal"."Title"+' '+"Addr_Personal"."Givennames"+' '+"Addr_Personal"."Surname" +' ('+ "addr_speciality"."Speciality"+')' "Name"
	 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
	 , "addr_addresses"."Line1"+' '+"addr_addresses"."Line2"+' '+ "addr_addresses"."Line3"+' '+"addr_addresses"."Line4" "Line Address"
	 , "addr_addresses"."Suburb"+' '+"addr_addresses"."State"+' '+"addr_addresses"."Postcode" "State Address"
	 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
	 , e.Examsdate, e.Session, e.Centre
	 , e.Type
	 , e.[AM/PM]
	 , e.SortOrder
	 , e.HospitalName
	 , "Addr_Personal".Surname
	 , "Addr_Personal".Givennames
	FROM   "CTS"."dbo"."Addr_Personal" "Addr_Personal"
	INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	INNER JOIN examiners e ON e.ExaminerID = Addr_Personal.Id
	WHERE   "addr_addresses"."MailAddr"='Y'

END
