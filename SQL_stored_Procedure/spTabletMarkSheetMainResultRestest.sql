USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTabletMarkSheetMainResultRestest]    Script Date: 18/11/2020 3:34:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTabletMarkSheetMainResultRestest] @ExamId int, @FileNo int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT DISTINCT 
  'Station'+STR("vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ClinicalSlotScenarios_Sortorder") "Station"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ClinicalSlotCandidates_Period"
 , CASE 
    WHEN  pilot= 0 AND "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."Results_Clinical_Exam_Stations" = 'P' THEN 'Pass'
	WHEN  pilot=0 AND "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."Results_Clinical_Exam_Stations" = 'N' THEN 'Fail'
	WHEN pilot = 1 THEN 'Assessed, not Scored'
	WHEN "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."Results_Clinical_Exam_Stations" = 'REST' Then 'NA           Rest Station'  
	END "Station_Result"
  , CASE 
   WHEN  "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ScenType" IN ('Child Health')
     THEN '*   '+"vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ScenType"
   WHEN  "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ScenType" IN ('Womens Health Gynaecology','Womens Health Obstetrics')
     THEN '**   '+"vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ScenType"
   ELSE 
   "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ScenType"
   END "ScenType"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."Results_ClinicalLastGrade"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ExamID"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."Fileno"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ClinicalIndividualExam"
, CONVERT(VARCHAR,"vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ExamsDate",106) "ExamDate"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest".GivenNames+' '+"vwResults_ClinicalStationClinicalSlotCandidates_Retest".FamilyName "Name"
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest".ExamsCentre
 , "vwResults_ClinicalStationClinicalSlotCandidates_Retest".ExamSession
 , CASE WHEN "vwResults_ClinicalStationClinicalSlotCandidates_Retest".ClinicalSlot_AMPM = 1 THEN 'Morning Session'
        WHEN "vwResults_ClinicalStationClinicalSlotCandidates_Retest".ClinicalSlot_AMPM = 0 THEN 'Afternoon Session' 
  END "AMPM_Session"
  ,"vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ClinicalSlotScenarios_Sortorder"
 FROM   "CTS"."dbo"."vwResults_ClinicalStationClinicalSlotCandidates_Retest" "vwResults_ClinicalStationClinicalSlotCandidates_Retest"
 WHERE  "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."ExamID"=@ExamId
 AND "vwResults_ClinicalStationClinicalSlotCandidates_Retest"."Fileno"=@FileNo

END
