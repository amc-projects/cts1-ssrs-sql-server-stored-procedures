USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptClin2004]    Script Date: 18/11/2020 10:31:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptClin2004] @FileNo int 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT "Results_ClinicalSerialize"."ExamSession"
 , "Results_ClinicalSerialize"."ExamCentre"
 , CONVERT(varchar,"Results_ClinicalSerialize"."Examdate",103) "Examdate"
  , "Results_ClinicalSerialize"."Examdate" "exam_date"
 , "Results_ClinicalSerialize"."Passfail"
 , "Results_ClinicalSerialize"."Station1"
 , "Results_ClinicalSerialize"."Station10"
 , "Results_ClinicalSerialize"."Station11"
 , "Results_ClinicalSerialize"."Station12"
 , "Results_ClinicalSerialize"."Station13"
 , "Results_ClinicalSerialize"."Station14"
 , "Results_ClinicalSerialize"."Station15"
 , "Results_ClinicalSerialize"."Station16"
 , "Results_ClinicalSerialize"."Station2"
 , "Results_ClinicalSerialize"."Station3"
 , "Results_ClinicalSerialize"."Station4"
 , "Results_ClinicalSerialize"."Station5"
 , "Results_ClinicalSerialize"."Station6"
 , "Results_ClinicalSerialize"."Station7"
 , "Results_ClinicalSerialize"."Station8"
 , "Results_ClinicalSerialize"."Station9"
 , CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 IS NULL AND Results_ClinicalSerialize.Station1='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 IS NULL AND Results_ClinicalSerialize.Station1!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 = 0 AND Results_ClinicalSerialize.Station1='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 = 1 AND Results_ClinicalSerialize.Station1!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_1 = 0 AND Results_ClinicalSerialize.Station1='N' THEN 0
END "s1"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 IS NULL AND Results_ClinicalSerialize.Station2='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 IS NULL AND Results_ClinicalSerialize.Station2!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 = 0 AND Results_ClinicalSerialize.Station2='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 = 1 AND Results_ClinicalSerialize.Station2!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_2 = 0 AND Results_ClinicalSerialize.Station2='N' THEN 0
END "s2"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 IS NULL AND Results_ClinicalSerialize.Station3='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 IS NULL AND Results_ClinicalSerialize.Station3!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 = 0 AND Results_ClinicalSerialize.Station3='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 = 1 AND Results_ClinicalSerialize.Station3!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_4 = 0 AND Results_ClinicalSerialize.Station3='N' THEN 0
END "s3"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 IS NULL AND Results_ClinicalSerialize.Station4='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 IS NULL AND Results_ClinicalSerialize.Station4!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 = 0 AND Results_ClinicalSerialize.Station4='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 = 1 AND Results_ClinicalSerialize.Station4!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_5 = 0 AND Results_ClinicalSerialize.Station4='N' THEN 0
END "s4"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 IS NULL AND Results_ClinicalSerialize.Station5='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 IS NULL AND Results_ClinicalSerialize.Station5!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 = 0 AND Results_ClinicalSerialize.Station5='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 = 1 AND Results_ClinicalSerialize.Station5!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_6 = 0 AND Results_ClinicalSerialize.Station5='N' THEN 0
END "s5"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 IS NULL AND Results_ClinicalSerialize.Station6='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 IS NULL AND Results_ClinicalSerialize.Station6!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 = 0 AND Results_ClinicalSerialize.Station6='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 = 1 AND Results_ClinicalSerialize.Station6!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_7 = 0 AND Results_ClinicalSerialize.Station6='N' THEN 0
END "s6"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 IS NULL AND Results_ClinicalSerialize.Station7='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 IS NULL AND Results_ClinicalSerialize.Station7!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 = 0 AND Results_ClinicalSerialize.Station7='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 = 1 AND Results_ClinicalSerialize.Station7!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_9 = 0 AND Results_ClinicalSerialize.Station7='N' THEN 0
END "s7"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 IS NULL AND Results_ClinicalSerialize.Station8='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 IS NULL AND Results_ClinicalSerialize.Station8!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 = 0 AND Results_ClinicalSerialize.Station8='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 = 1 AND Results_ClinicalSerialize.Station8!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_10 = 0 AND Results_ClinicalSerialize.Station8='N' THEN 0
END "s8"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 IS NULL AND Results_ClinicalSerialize.Station9='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 IS NULL AND Results_ClinicalSerialize.Station9!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 = 0 AND Results_ClinicalSerialize.Station9='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 = 1 AND Results_ClinicalSerialize.Station9!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_11 = 0 AND Results_ClinicalSerialize.Station9='N' THEN 0
END "s9"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 IS NULL AND Results_ClinicalSerialize.Station10='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 IS NULL AND Results_ClinicalSerialize.Station10!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 = 0 AND Results_ClinicalSerialize.Station10='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 = 1 AND Results_ClinicalSerialize.Station10!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_12 = 0 AND Results_ClinicalSerialize.Station10='N' THEN 0
END "s10"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 IS NULL AND Results_ClinicalSerialize.Station11='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 IS NULL AND Results_ClinicalSerialize.Station11!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 = 0 AND Results_ClinicalSerialize.Station11='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 = 1 AND Results_ClinicalSerialize.Station11!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_14 = 0 AND Results_ClinicalSerialize.Station11='N' THEN 0
END "s11"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 IS NULL AND Results_ClinicalSerialize.Station12='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 IS NULL AND Results_ClinicalSerialize.Station12!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 = 0 AND Results_ClinicalSerialize.Station12='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 = 1 AND Results_ClinicalSerialize.Station12!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_15 = 0 AND Results_ClinicalSerialize.Station12='N' THEN 0
END "s12"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 IS NULL AND Results_ClinicalSerialize.Station13='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 IS NULL AND Results_ClinicalSerialize.Station13!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 = 0 AND Results_ClinicalSerialize.Station13='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 = 1 AND Results_ClinicalSerialize.Station13!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_16 = 0 AND Results_ClinicalSerialize.Station13='N' THEN 0
END "s13"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 IS NULL AND Results_ClinicalSerialize.Station14='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 IS NULL AND Results_ClinicalSerialize.Station14!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 = 0 AND Results_ClinicalSerialize.Station14='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 = 1 AND Results_ClinicalSerialize.Station14!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_17 = 0 AND Results_ClinicalSerialize.Station14='N' THEN 0
END "s14"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 IS NULL AND Results_ClinicalSerialize.Station15='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 IS NULL AND Results_ClinicalSerialize.Station15!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 = 0 AND Results_ClinicalSerialize.Station15='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 = 1 AND Results_ClinicalSerialize.Station15!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_19 = 0 AND Results_ClinicalSerialize.Station15='N' THEN 0
END "s15"
, CASE WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 IS NULL AND Results_ClinicalSerialize.Station16='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 IS NULL AND Results_ClinicalSerialize.Station16!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 = 1 THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 = 0 AND Results_ClinicalSerialize.Station16='P' THEN 1
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 = 1 AND Results_ClinicalSerialize.Station16!='P' THEN 0
WHEN Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 IS NOT NULL AND Results_ClinicalSerialize.clinicalmarksheetcandidatescenario_pilot_20 = 0 AND Results_ClinicalSerialize.Station16='N' THEN 0
END "s16"
 , "vwResults_ClinicalSerialize_Retest"."Station1"  "RStation1"
 , "vwResults_ClinicalSerialize_Retest"."Station2"  "RStation2"
 , "vwResults_ClinicalSerialize_Retest"."Station3"  "RStation3"
 , "vwResults_ClinicalSerialize_Retest"."Station4"  "RStation4"
 , "vwResults_ClinicalSerialize_Retest"."Station5"  "RStation5"
 , "vwResults_ClinicalSerialize_Retest"."Station6"  "RStation6" 
 , "vwResults_ClinicalSerialize_Retest"."Station7"  "RStation7"
 , "vwResults_ClinicalSerialize_Retest"."Station8"  "RStation8"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_1_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_2_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_3_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_4_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_5_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_6_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_7_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_8_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_9_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_10_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_11_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_12_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_13_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_14_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_15_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_16_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_17_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_18_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_19_ScenType"
 , "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_20_ScenType"
 , "vwResults_ClinicalSerialize_Retest"."ExamSession" "RExamSession"
 , CONVERT(varchar,"vwResults_ClinicalSerialize_Retest"."Examdate",103) "RExamdate"
 , "vwResults_ClinicalSerialize_Retest"."ExamCentre" "RExamCentre"
 , "vwResults_ClinicalSerialize_Retest"."Passfail" "RPassfail"
 , "Results_ClinicalSerialize"."FileNo"
 , "Results_ClinicalSerialize"."Results_Clinical_LastGrade"
 , "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_1", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_12", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_14", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_15", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_16", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_2", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_4", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_5", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_6", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_7", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_9", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_17", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_19", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_20", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_10", "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_11"
 , CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_1_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_2_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_3_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_4_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_5_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_6_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_7_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_8_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_9_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_10_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_11_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_12_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_13_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_14_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_15_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_16_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_17_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_18_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_19_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_20_ScenType" IN ('Child Health', 'Paediatrics') THEN 1 ELSE 0 END "ChildMainCount"
   ,
   
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_1_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station1" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_2_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station2" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_4_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station3" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_5_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station4" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_6_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station5" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_7_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station6" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_9_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station7" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_10_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station8" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_11_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station9" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_12_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station10" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_14_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station11" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_15_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station12" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_16_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station13" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_17_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station14" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_19_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station15" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_20_ScenType" IN ('Child Health', 'Paediatrics')  AND "Results_ClinicalSerialize"."Station16" = 'P' THEN 1 ELSE 0 END 
"ChildPassCount"
, CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_1_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_2_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_3_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_4_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_5_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_6_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_7_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_8_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_9_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_10_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_11_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_12_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_13_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_14_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_15_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_16_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_17_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_18_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_19_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_20_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology') THEN 1 ELSE 0 END "WomenMainCount"
   ,
   
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_1_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station1" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_2_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station2" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_4_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station3" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_5_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station4" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_6_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station5" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_7_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station6" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_9_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station7" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_10_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station8" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_11_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station9" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_12_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station10" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_14_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station11" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_15_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station12" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_16_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station13" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_17_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station14" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_19_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station15" = 'P' THEN 1 ELSE 0 END +
   CASE WHEN "Results_ClinicalSerialize"."ClinicalSlotScenario_ScenType_20_ScenType" IN ('Obstetrics','Gynaecology','Womens Health Obstetrics','Womens Health Gynaecology')  AND "Results_ClinicalSerialize"."Station16" = 'P' THEN 1 ELSE 0 END 
"WomenPassCount"
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station1" = 'P' THEN 1 ELSE 0 END Rs1
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station2" = 'P' THEN 1 ELSE 0 END Rs2
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station3" = 'P' THEN 1 ELSE 0 END Rs3
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station4" = 'P' THEN 1 ELSE 0 END Rs4
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station5" = 'P' THEN 1 ELSE 0 END Rs5
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station6" = 'P' THEN 1 ELSE 0 END Rs6
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station7" = 'P' THEN 1 ELSE 0 END Rs7
, CASE  WHEN "vwResults_ClinicalSerialize_Retest"."Station8" = 'P' THEN 1 ELSE 0 END Rs8

 FROM   "CTS"."dbo"."Results_ClinicalSerialize" "Results_ClinicalSerialize" LEFT OUTER JOIN "CTS"."dbo"."vwResults_ClinicalSerialize_Retest" "vwResults_ClinicalSerialize_Retest" ON "Results_ClinicalSerialize"."Results_Clinical_ID"="vwResults_ClinicalSerialize_Retest"."Results_Clinical_ID"
 WHERE  "Results_ClinicalSerialize"."FileNo"=@FileNo
-- WHERE "vwResults_ClinicalSerialize_Retest"."Examdate" IS NOT NULL
-- ORDER BY "Results_ClinicalSerialize"."Examdate"



END
