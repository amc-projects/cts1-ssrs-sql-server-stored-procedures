USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidatesLabelList]    Script Date: 12/11/2021 11:50:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidatesLabelList] @ExamId int, @Grade nvarchar(20)
	
AS
DECLARE 
	@l_exam_type int;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ExamTypeId int;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;

	---This is CTS Exam Type in Face to Face
	SELECT  @ExamTypeId = ExamTypeID
	FROM dbo.Exams WHERE id = @l_cts_exam_id
	
	--This returns Online or Face to Face
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	BEGIN
	 SELECT rc.Fileno
		, "Candidates"."Address"
	    , CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		   THEN 'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ SUBSTRING("GivenNames",CHARINDEX(' ',"GivenNames")+1,1)+' '+"Candidates"."FamilyName" 
		   ELSE
		   'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ "Candidates"."FamilyName"
		   END "Name"
		, "Candidates"."InvalidAddr"
		, "iMedCountries"."Description"
		, "Candidates"."City"+' '+ "Candidates"."State"+' '+"Candidates"."PostCode" "City"
		,  ROW_NUMBER() OVER(ORDER BY "Candidates"."FamilyName","Candidates"."GivenNames" ASC)%2  AS Row#
		, clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		, "Candidates"."GivenNames"
		, "Candidates"."FamilyName"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON exam_assignments.candidate_id="Candidates"."Fileno"
		INNER JOIN "CTS"."dbo".Results_Clinical rc ON rc.FileNo = exam_assignments.candidate_id
		AND rc.ExamID = clinical_exams.cts_exam_id
		LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
		WHERE clinical_exams.id = @ExamId AND rc.LastGrade = @Grade
		AND candidate_id IS NOT NULL
	END
	ELSE
	BEGIN 
	 IF @ExamTypeId = 2 
	 BEGIN
		SELECT Results_Clinical.Fileno
		, "Candidates"."Address"
	    , CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		   THEN 'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ SUBSTRING("GivenNames",CHARINDEX(' ',"GivenNames")+1,1)+' '+"Candidates"."FamilyName" 
		   ELSE
		   'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ "Candidates"."FamilyName"
		   END "Name"
		, "Candidates"."InvalidAddr"
		, "iMedCountries"."Description"
		, "Candidates"."City"+' '+ "Candidates"."State"+' '+"Candidates"."PostCode" "City"
		,  ROW_NUMBER() OVER(ORDER BY "Candidates"."FamilyName","Candidates"."GivenNames" ASC)%2  AS Row#
		,  Exams.Session
		, Exams.Centre
		, "Candidates"."GivenNames"
		, "Candidates"."FamilyName"
		FROM Results_Clinical INNER JOIN Exams ON Results_Clinical.ExamID = Exams.ID
		INNER JOIN Candidates ON Results_Clinical.FileNo = Candidates.Fileno
		LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
		WHERE Results_Clinical.LastGrade = @Grade AND Exams.ID = @l_cts_exam_id
      END
      IF  @ExamTypeId = 3
	  BEGIN
		SELECT 
		   Results_Clinical.FileNo
		, "Candidates"."Address"
        , CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		   THEN 'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ SUBSTRING("GivenNames",CHARINDEX(' ',"GivenNames")+1,1)+' '+"Candidates"."FamilyName" 
		   ELSE
		   'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ "Candidates"."FamilyName"
		   END "Name"
		, "Candidates"."InvalidAddr"
		, "iMedCountries"."Description"
		, "Candidates"."City"+' '+ "Candidates"."State"+' '+"Candidates"."PostCode" "City"
		,  ROW_NUMBER() OVER(ORDER BY "Candidates"."FamilyName","Candidates"."GivenNames" ASC)%2  AS Row#
		,  Exams.Session
		, Exams.Centre
		, "Candidates"."GivenNames"
		, "Candidates"."FamilyName"
		FROM Results_Clinical INNER JOIN Exams ON Results_Clinical.ExamID = Exams.ID
		INNER JOIN Candidates ON Results_Clinical.FileNo = Candidates.Fileno
		LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
		INNER JOIN Results_Clinical_Retest ON Results_Clinical.ID = Results_Clinical_Retest.ResultID
		WHERE Results_Clinical_Retest.Passfail = @Grade AND Results_Clinical_Retest.RetestExamID = @l_cts_exam_id
      END
	  END
END
