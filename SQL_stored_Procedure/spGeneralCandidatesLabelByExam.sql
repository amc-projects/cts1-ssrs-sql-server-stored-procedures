USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spGeneralCandidatesLabelByExam]    Script Date: 18/11/2020 2:38:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGeneralCandidatesLabelByExam] @ExamId int
	
AS
DECLARE 
	@l_exam_type int;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @l_exam_type = ExamTypeID FROM Exams
	WHERE Exams.id = @ExamID

	IF @l_exam_type = 2 

		SELECT Results_Clinical.Fileno
		, "Candidates"."Address"
	    , CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		   THEN 'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ SUBSTRING("GivenNames",CHARINDEX(' ',"GivenNames")+1,1)+' '+"Candidates"."FamilyName" 
		   ELSE
		   'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ "Candidates"."FamilyName"
		   END "Name"
		, "Candidates"."InvalidAddr"
		--, "iMedCountries"."Description"
		, ISNULL("Candidates"."City",'')+' '+ ISNULL("Candidates"."State",'')+' '+ISNULL("Candidates"."PostCode",'') "CityAddr"
		,  ROW_NUMBER() OVER(ORDER BY "Candidates"."FamilyName","Candidates"."GivenNames" ASC)%2  AS Row#
		,  Exams.Session
		, Exams.Centre
		, "Candidates"."GivenNames"
		, "Candidates"."FamilyName"
		, "Locations".Description
		FROM Results_Clinical INNER JOIN Exams ON Results_Clinical.ExamID = Exams.ID
		INNER JOIN Candidates ON Results_Clinical.FileNo = Candidates.Fileno
		LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
		LEFT OUTER JOIN "CTS"."dbo"."Locations" "Locations" ON "Candidates"."Location"="Locations"."ID"
		WHERE  Exams.ID = @ExamId

    ELSE IF  @l_exam_type = 3
		SELECT 
		   Results_Clinical.FileNo
		, "Candidates"."Address"
        , CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		   THEN 'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ SUBSTRING("GivenNames",CHARINDEX(' ',"GivenNames")+1,1)+' '+"Candidates"."FamilyName" 
		   ELSE
		   'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ "Candidates"."FamilyName"
		   END "Name"
		, "Candidates"."InvalidAddr"
		--, "iMedCountries"."Description"
		, ISNULL("Candidates"."City",'')+' '+ ISNULL("Candidates"."State",'')+' '+ISNULL("Candidates"."PostCode",'') "City"
		,  ROW_NUMBER() OVER(ORDER BY "Candidates"."FamilyName","Candidates"."GivenNames" ASC)%2  AS Row#
		,  Exams.Session
		, Exams.Centre
		, "Candidates"."GivenNames"
		, "Candidates"."FamilyName"
		, "Locations".Description
		FROM Results_Clinical INNER JOIN Exams ON Results_Clinical.ExamID = Exams.ID
		INNER JOIN Candidates ON Results_Clinical.FileNo = Candidates.Fileno
		LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
		INNER JOIN Results_Clinical_Retest ON Results_Clinical.ID = Results_Clinical_Retest.ResultID
		LEFT OUTER JOIN "CTS"."dbo"."Locations" "Locations" ON "Candidates"."Location"="Locations"."ID"
		WHERE Results_Clinical_Retest.RetestExamID = @ExamId

	
END
