USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptSpecLog]    Script Date: 18/11/2020 11:12:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptSpecLog] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SELECT "SpecialistLog"."Date"
	, CONVERT(varchar,"SpecialistLog".Date,103) "SpecLogDate"
   , "SpecialistEval"."FileNo"
   , "SpecialistCollege"."CollegeName"
   , "Specialty"."SpecialityName"
   , "SpecLogReasons"."Description"
   , "SpecialistEval"."SpecEvalID"
   , "SpecAON"."ID"
   , "SpecAON"."Areatitle"
   FROM   (((("CTS"."dbo"."SpecialistEval" "SpecialistEval" INNER JOIN "CTS"."dbo"."SpecialistLog" "SpecialistLog" ON "SpecialistEval"."SpecEvalID"="SpecialistLog"."SpecEvalID") INNER JOIN "CTS"."dbo"."SpecialistCollege" "SpecialistCollege" ON "SpecialistEval"."SpecCollCode"="SpecialistCollege"."SpecCollCode") INNER JOIN "CTS"."dbo"."Specialty" "Specialty" ON ("SpecialistEval"."SpecCollCode"="Specialty"."SpecCollCode") AND ("SpecialistEval"."SpecCode"="Specialty"."SpecCode")) LEFT OUTER JOIN "CTS"."dbo"."SpecAON" "SpecAON" ON "SpecialistEval"."SpecEvalID"="SpecAON"."SpecEvalID") INNER JOIN "CTS"."dbo"."SpecLogReasons" "SpecLogReasons" ON "SpecialistLog"."SpecLogReasonsID"="SpecLogReasons"."SpecLogReasonsID"
   WHERE  "SpecialistEval"."FileNo"=@FileNo
 --ORDER BY "vwCandidateTranscriptCA"."FileNo"



END
