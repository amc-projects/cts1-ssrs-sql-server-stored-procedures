USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCollectionLetters]    Script Date: 18/11/2020 12:09:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCollectionLetters] @FileNo int, @ReturnDate nvarchar(30)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT "Candidates"."Fileno"
	, 'Dr '+"Candidates"."GivenNames"+' '+"Candidates"."FamilyName" "Name"
	, "Candidates"."FamilyName"
	, "Candidates"."State"+' '+"Candidates"."PostCode" "State"
	, "Certs"."Status"
	, "Eligibility"."Diploma"
	, "Eligibility"."Name" "Eligibility_Name"
	, "Eligibility"."ID"
	, "Eligibility"."Englishseen"
	, "Candidates"."Address"
	, "Candidates"."City"
	, "iMedCountries"."Description"
	--, CAST(DAY(@ReturnDate) AS VARCHAR(2)) + ' ' + DATENAME(MM, @ReturnDate) + ' '+ CAST(YEAR(@ReturnDate) AS VARCHAR(4)) AS "ReturnDate"
	,  DATENAME(weekday, GETDATE())+', '+ CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MM, GETDATE()) + ', ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS "TodaysDate"
	FROM   (("CTS"."dbo"."Candidates" "Candidates" INNER JOIN "CTS"."dbo"."Certs" "Certs" ON "Candidates"."Fileno"="Certs"."Fileno") 
	INNER JOIN "CTS"."dbo"."Eligibility" "Eligibility" ON "Candidates"."Fileno"="Eligibility"."Fileno")
	LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
	WHERE  "Certs"."Status"='SAFE'
	AND Candidates.FileNo = @FileNo

END
