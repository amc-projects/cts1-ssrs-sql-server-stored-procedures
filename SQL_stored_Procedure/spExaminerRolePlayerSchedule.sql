USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerRolePlayerSchedule]    Script Date: 17/11/2021 12:17:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerRolePlayerSchedule] @ExamId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	 BEGIN
	   SELECT 
	      "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') "Name"
		 , CASE WHEN "addr_speciality"."Speciality" IS NOT NULL AND "Addr_Personal"."Surname" !='REST STATION'THEN '['+"addr_speciality"."Speciality"+']' 
		   ELSE '' END "Speciality"
		 , CONVERT(VARCHAR(15),  clinical_exams.date, 106)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		, 'Examiners' "Type"
		, CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		  END "AM/PM"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 , exam_part_stations.number "SortOrder"
		 , '' SlotNo
                 , clinical_exams.exam_type_id "ExamTypeID"
	     , CONVERT(VARCHAR(10),  clinical_exams.date, 106) "MeetingDate"
               	 ,  '' "MeetingTime"   
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.examiner_id
	    INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	    INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	    WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId
		AND examiner_id IS NOT NULL

		UNION 
        SELECT 
	     "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') "Name"
		 , CASE WHEN "addr_speciality"."Speciality" IS NOT NULL AND "Addr_Personal"."Surname" !='REST STATION'THEN '['+"addr_speciality"."Speciality"+']' 
		   ELSE '' END "Speciality"
		 , CONVERT(VARCHAR(15),  clinical_exams.date, 106)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		, 'Observers' "Type"
		, CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		  END "AM/PM"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 , '' "SortOrder"
		 , '' SlotNo
         , clinical_exams.exam_type_id "ExamTypeID" 
	    , CONVERT(VARCHAR(15),  clinical_exams.date, 106) "MeetingDate"
		,  '' "MeetingTime"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
		AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
		--INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
	    INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	    INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	    WHERE   "addr_addresses"."MailAddr"='Y'
        AND  clinical_exams.id =@ExamId
     	AND role IN ('QA Examiner', 'Chair') 

        UNION

		SELECT 
	     "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') "Name"
		 ,  ''  "Speciality"
		 , CONVERT(VARCHAR(15),  clinical_exams.date, 106)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		, 'Role Players' "Type"
		, CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		  END "AM/PM"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 , exam_part_stations.number "SortOrder"
		 , '' SlotNo
                 , clinical_exams.exam_type_id "ExamTypeID"
	         , CONVERT(VARCHAR(15),  clinical_exams.date, 106) "MeetingDate"
		,  '' "MeetingTime"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.simulated_patient_id
	    INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	    INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	    WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId

		END
        ELSE
		BEGIN
		-- Insert statements for procedure here
		WITH examiners AS 
		(
			SELECT DISTINCT Exams.ID
			, Exams.Session
			, ClinicalSlotExaminers.ExaminerID
			, Exams.Centre
			, CONVERT(VARCHAR,  Exams.Examdate, 106)  AS "Examsdate"
			, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'AM' WHEN ClinicalSlot.AMPM = 0 THEN 'PM' END AS "AM/PM"
			, 'Examiners' "Type"
			, Venues.HospitalName
			, ClinicalSlotExaminers.SortOrder
			, ClinicalSlot.SlotNo
			, "ClinicalPanelMeetings"."MeetingTime"
			, CONVERT(VARCHAR,"ClinicalPanelMeetings"."MeetingDate",106) "MeetingDate"
			, Exams.ExamTypeID
			FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
			INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID 
			INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
			LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
			LEFT OUTER JOIN "CTS"."dbo"."ClinicalPanelMeetings" "ClinicalPanelMeetings"
			ON "ClinicalPanelMeetings"."VenueID"="Venues"."ID" AND "ClinicalPanelMeetings".ExamID = Exams.ID
			WHERE Exams.ID = @l_cts_exam_id
			AND ExaminerID NOT IN (9175,9612,9613,10213)
		 
			UNION 

			SELECT DISTINCT Exams.ID
			, Exams.Session
			, ClinicalObservers.ExaminerID
			, Exams.Centre
			, CONVERT(VARCHAR,Exams.Examdate, 106)  AS "Examsdate"
			, CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
			, 'Observers' "Type"
			, Venues.HospitalName
			, 0 SortOrder
			, 0 SlotNo
			, "ClinicalPanelMeetings"."MeetingTime"
			, CONVERT(VARCHAR,"ClinicalPanelMeetings"."MeetingDate",106) "MeetingDate"
			, Exams.ExamTypeID
			FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
			INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
			--INNER JOIN ClinicalObserverRotations ON ClinicalObservers.ID = ClinicalObserverRotations.ClinicalObserverID
		   -- INNER JOIN ClinicalSlot ON ClinicalObserverRotations.SetNo = ClinicalSlot.ID
			LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
			LEFT OUTER JOIN "CTS"."dbo"."ClinicalPanelMeetings" "ClinicalPanelMeetings"
			ON "ClinicalPanelMeetings"."VenueID"="Venues"."ID" AND "ClinicalPanelMeetings".ExamID = Exams.ID
			WHERE Exams.ID =  @l_cts_exam_id
			AND ExaminerID NOT IN (9175,9612,9613,10213)

			UNION 

			SELECT DISTINCT Exams.ID
			, Exams.Session
			, ClinicalSlotRolePlayers.ExaminerID
			, Exams.Centre
			, CONVERT(VARCHAR,Exams.Examdate, 106)  AS "Examsdate"
			, CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
			, 'Role Players' "Type"
			, Venues.HospitalName
			, ClinicalSlotRolePlayers.SortOrder
			, cs.SlotNo
			, "ClinicalPanelMeetings"."MeetingTime"
			, CONVERT(VARCHAR,"ClinicalPanelMeetings"."MeetingDate",106) "MeetingDate"
			, Exams.ExamTypeID
			FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
			INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot cs ON cs.ClinicalVenueID = ClinicalVenues.ID
			INNER JOIN "CTS"."dbo"."ClinicalSlotRolePlayers" "ClinicalSlotRolePlayers" ON "ClinicalSlotRolePlayers".ClinicalSlotID = cs.ID
			LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
			LEFT OUTER JOIN "CTS"."dbo"."ClinicalPanelMeetings" "ClinicalPanelMeetings"
			ON "ClinicalPanelMeetings"."VenueID"="Venues"."ID" AND "ClinicalPanelMeetings".ExamID = Exams.ID
			WHERE Exams.ID =  @l_cts_exam_id
			AND ExaminerID NOT IN (9175,9612,9613,10213, 14622) 
		)
		SELECT  DISTINCT "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') "Name"
		 , CASE WHEN "addr_speciality"."Speciality" IS NOT NULL AND e.Type!= 'Role Players' AND "Addr_Personal"."Surname" !='REST STATION'THEN '['+"addr_speciality"."Speciality"+']' 
		   ELSE '' END "Speciality"
		 , e.Examsdate
		 , e.Session
		 , e.Centre
		 , e.Type
		 , e.[AM/PM]
		 , e.HospitalName
		 , e.SortOrder
		 , e.SlotNo
		 , e.ExamTypeID
		 , e.MeetingDate
		 , e.MeetingTime
		FROM   "CTS"."dbo"."Addr_Personal" "Addr_Personal"
		LEFT OUTER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
		--INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
		INNER JOIN examiners e ON e.ExaminerID = Addr_Personal.Id
		--WHERE   "addr_addresses"."MailAddr"='Y'
       END
END
