USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spWBACerts]    Script Date: 18/11/2020 3:40:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spWBACerts] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

	SELECT "Candidates"."GivenNames"+' '+UPPER("Candidates"."FamilyName") "Name"
	, "Certs"."Certno"
	, "Images"."Image"
	,  "Candidates".FileNo  
	, "Candidates"."GivenNames"
	, "Candidates"."FamilyName"
	, CASE 
	  WHEN CAST(DAY(Issuedate) AS VARCHAR(2)) IN (1,21,31) THEN 'this '+CAST(DAY(Issuedate) AS VARCHAR(2))+'st day of' + ' ' + DATENAME(MM, Issuedate) + ' ' + CAST(YEAR(Issuedate) AS VARCHAR(4))
	  WHEN CAST(DAY(Issuedate) AS VARCHAR(2)) IN (2,22) THEN 'this '+CAST(DAY(Issuedate) AS VARCHAR(2))+'nd day of' + ' ' + DATENAME(MM, Issuedate) + ' ' + CAST(YEAR(Issuedate) AS VARCHAR(4))
	  WHEN CAST(DAY(Issuedate) AS VARCHAR(2)) IN (3,23) THEN 'this '+CAST(DAY(Issuedate) AS VARCHAR(2))+'rd day of' + ' ' + DATENAME(MM, Issuedate) + ' ' + CAST(YEAR(Issuedate) AS VARCHAR(4))
	  ELSE 'this '+CAST(DAY(Issuedate) AS VARCHAR(2))+'th day of' + ' ' + DATENAME(MM, Issuedate) + ' ' + CAST(YEAR(Issuedate) AS VARCHAR(4))
	END "IssueDate"
	FROM WBA INNER JOIN Certs ON WBA.FileNo = Certs.FileNo
	INNER JOIN "CTS"."dbo"."Candidates" ON "Candidates".FileNo = Certs.FileNo
	INNER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
	INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno"
	WHERE WBA.Result = 'P' AND Certs.IssueDate IS NOT NULL AND Certs.Status='I' AND Certs.Issuedthrough IS NOT NULL

END
