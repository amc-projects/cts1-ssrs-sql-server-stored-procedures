USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spSurrenderList]    Script Date: 18/11/2020 3:28:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSurrenderList] @ExamID int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT "Exams"."Session", "Candidates"."Fileno"
 , "Exams"."ExamTypeID"
 , "ClinicalSlot"."AMPM"
 , "ClinicalSlot"."Colour"
 , "ClinicalSlot"."GroupText"
 , CONVERT(VARCHAR, "ClinicalDates"."ExamDate",103) "ExamDate"
 , "ClinicalSlotCandidates"."Period"
 ,  UPPER("Candidates"."FamilyName")+', '+UPPER(SUBSTRING("Candidates"."GivenNames",1,1)) "Name"
 , "Images"."Image"
  , CASE 
	WHEN "ClinicalSlot"."AMPM" = 1 THEN 'MORNING SESSION'
    WHEN "ClinicalSlot"."AMPM" = 0 THEN 'AFTERNOON SESSION' 
  END "AM_PM_Session" 
 FROM   ((((("CTS"."dbo"."Exams" "Exams" INNER JOIN "CTS"."dbo"."ClinicalDates" "ClinicalDates" ON "Exams"."ID"="ClinicalDates"."ExamID")
 INNER JOIN "CTS"."dbo"."ClinicalVenues" "ClinicalVenues" ON "ClinicalDates"."ID"="ClinicalVenues"."ClinicalDateID") 
 INNER JOIN "CTS"."dbo"."ClinicalSlot" "ClinicalSlot" ON "ClinicalVenues"."ID"="ClinicalSlot"."ClinicalVenueID") 
 INNER JOIN "CTS"."dbo"."ClinicalSlotCandidates" "ClinicalSlotCandidates" ON "ClinicalSlot"."ID"="ClinicalSlotCandidates"."ClinicalSlotID")
 INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "ClinicalSlotCandidates"."FileNo"="Candidates"."Fileno") 
 LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
 WHERE "Exams".id = @ExamID
 AND "Candidates".Fileno != 1234

END
