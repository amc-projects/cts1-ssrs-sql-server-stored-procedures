USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTabletMarkSheetRetest]    Script Date: 18/11/2020 3:34:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTabletMarkSheetRetest] @ExamId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT "vwResults_ClinicalClinicalSlotCandidates_Retest"."GivenNames"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."FamilyName"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."Fileno"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."ExamID"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."Results_ClinicalLastGrade"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."ClinicalSlotScenarios_Sortorder"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."clinicalscenarios_id"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."ClinicalSlotCandidate_id"
 , "vwResults_ClinicalClinicalSlotCandidates_Retest"."ExamIDMain"
 , "Exams".Centre
 , "Exams".Session
 , CONVERT(varchar,"Exams".Examdate,106) "ExamDate"
 , CASE WHEN "vwResults_ClinicalClinicalSlotCandidates_Retest".ClinicalSlot_AMPM = 1 THEN 'Morning Session'
        WHEN "vwResults_ClinicalClinicalSlotCandidates_Retest".ClinicalSlot_AMPM = 0 THEN 'Afternoon Session' 
  END "AMPM_Session" 
 FROM   "CTS"."dbo"."vwResults_ClinicalClinicalSlotCandidates_Retest" "vwResults_ClinicalClinicalSlotCandidates_Retest"
 INNER JOIN "CTS"."dbo"."Exams" ON "Exams".Id = "vwResults_ClinicalClinicalSlotCandidates_Retest"."ExamID"
 WHERE  "vwResults_ClinicalClinicalSlotCandidates_Retest"."ExamID"=@ExamId
-- AND "vwResults_ClinicalClinicalSlotCandidates_Retest"."Fileno" = @FileNo
 

END
