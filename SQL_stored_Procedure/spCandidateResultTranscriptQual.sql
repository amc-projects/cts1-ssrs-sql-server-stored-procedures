USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptQual]    Script Date: 18/11/2020 11:09:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptQual] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
   SELECT "vwQualification_iMed"."MedSchool", "vwQualification_iMed"."CtrlUni", "vwQualification_iMed"."PrimaryQual", "vwQualification_iMed"."CtyTrained", "vwQualification_iMed"."QualYear", "vwQualification_iMed"."FileNo"
   FROM   "CTS"."dbo"."vwQualification_iMed" "vwQualification_iMed"
   WHERE  "vwQualification_iMed"."FileNo"=@FileNo

END
