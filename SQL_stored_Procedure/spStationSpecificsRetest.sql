USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spStationSpecificsRetest]    Script Date: 18/11/2020 3:24:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStationSpecificsRetest] @ExamId int, @FileNo int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT DISTINCT "vwcmcs"."ExaminersSortOrder"
 , "vwcmcs"."Scenario_PlainCondition"
 , "vwcmcs"."ExamID", "vwcmcs"."FileNo"
 , "vwcmcs"."Station1"
 , "vwcmcs"."Station2"
 , "vwcmcs"."Station3"
 , "vwcmcs"."Station4"
 , "vwcmcs"."Station5"
 , "vwcmcs"."Station6"
 , "vwcmcs"."Station7"
 , "vwcmcs"."Station8"
 , "vwcmcs"."cmcs_rating"
 , "vwcmcs"."clinicalscenarios_id"
 , "vwcmcs"."cmcs_conceded_pass_at"
 , "vwcmcs"."cmcs_pilot"
 , CASE 
    WHEN vwcmcs.ExaminersSortOrder = 1 AND vwcmcs.Station1 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 1 AND vwcmcs.Station1 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 2 AND vwcmcs.Station2 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 2 AND vwcmcs.Station2 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 4 AND vwcmcs.Station3 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 4 AND vwcmcs.Station3 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 5 AND vwcmcs.Station4 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 5 AND vwcmcs.Station4 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 6 AND vwcmcs.Station5 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 6 AND vwcmcs.Station5 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 7 AND vwcmcs.Station6 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 7 AND vwcmcs.Station6 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 9 AND vwcmcs.Station7 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 9 AND vwcmcs.Station7 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 10 AND vwcmcs.Station8 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 10 AND vwcmcs.Station8 = 'P' THEN 'Pass'
   END "Grade"	
 , CASE WHEN "vrccsc".ClinicalSlot_AMPM = 1 THEN 'Morning Session'
        WHEN "vrccsc".ClinicalSlot_AMPM = 0 THEN 'Afternoon Session' 
  END "AMPM_Session"
 , "vwcmcs".Session
 , "vwcmcs".Centre
 , "vrccsc"."GivenNames"
 , "vrccsc"."FamilyName"
 , "vrccsc"."Results_ClinicalLastGrade"
 , CONVERT(varchar,"vwcmcs".Examdate,106) "ExamDate"
 FROM   "CTS"."dbo"."vwclinicalmarksheetcandidatescenarios_retest" "vwcmcs"
 INNER JOIN "CTS"."dbo"."vwResults_ClinicalClinicalSlotCandidates_Retest" "vrccsc"
    ON "vwcmcs".ExamID = "vrccsc".ExamID AND "vwcmcs".FileNo = "vrccsc".Fileno
	AND "vrccsc".clinicalscenarios_id = "vwcmcs".clinicalscenarios_id
 WHERE  "vwcmcs".ExamID = @ExamId AND "vwcmcs"."FileNo"= @FileNo
--AND "vwcmcs"."clinicalscenarios_id"=1
END
