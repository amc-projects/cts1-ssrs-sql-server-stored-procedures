USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCertificateWBAReissue]    Script Date: 18/11/2020 12:09:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCertificateWBAReissue] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

	SELECT Certs.FileNo
	, candidates.GivenNames+' '+UPPER(candidates.FamilyName) "Name"
	, "Certs"."Certno"
	, CONVERT(VARCHAR,"Certs"."Issuedate",106) "IssueDate"
	, "Images"."Image"
	, CONVERT(VARCHAR,GETDATE(),106) "TodayDate"
	FROM WBA INNER JOIN Certs ON WBA.FileNo = Certs.FileNo
    INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "Candidates"."Fileno"="Certs"."Fileno"
    INNER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
	INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno"
	WHERE WBA.Result = 'P' AND Certs.Status = 'I'
    AND Certs.IssueDate IS NOT NULL 

END
