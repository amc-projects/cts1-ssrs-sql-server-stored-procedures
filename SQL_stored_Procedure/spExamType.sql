USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExamType]    Script Date: 15/11/2021 11:04:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE   PROCEDURE [dbo].[spExamType] @ExamID int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_count AS INT;
	SELECT @l_count = COUNT(*) 
	FROM AMC.cts.clinical_exams
	INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
	WHERE clinical_exams.id = @ExamID

	IF @l_count > 0 
	  return 1
    ELSE
	  return 0
END
