USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spStationSpecifics]    Script Date: 18/11/2020 3:23:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStationSpecifics] @ExamId INT, @FileNo INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT "vwcmcs"."ExaminersSortOrder", "vwcmcs"."Scenario_PlainCondition"
 , "vwcmcs"."Station1"
 , "vwcmcs"."Station2"
 , "vwcmcs"."Station3"
 , "vwcmcs"."Station4"
 , "vwcmcs"."Station5"
 , "vwcmcs"."Station6"
 , "vwcmcs"."Station7"
 , "vwcmcs"."Station8"
 , "vwcmcs"."Station9"
 , "vwcmcs"."Station10"
 , "vwcmcs"."Station11"
 , "vwcmcs"."Station12"
 , "vwcmcs"."Station13"
 , "vwcmcs"."Station14"
 , "vwcmcs"."Station15"
 , "vwcmcs"."Station16"
 , "vwcmcs"."FileNo"
 , "vwcmcs"."ExamID"
 , "vwcmcs"."clinicalscenarios_id"
 , "vwcmcs"."cmcs_rating"
 , "vwcmcs"."cmcs_conceded_pass_at"
 , "vwcmcs"."cmcs_pilot"
 , CASE 
    WHEN vwcmcs.ExaminersSortOrder = 1 AND vwcmcs.Station1 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 1 AND vwcmcs.Station1 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 2 AND vwcmcs.Station2 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 2 AND vwcmcs.Station2 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 4 AND vwcmcs.Station3 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 4 AND vwcmcs.Station3 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 5 AND vwcmcs.Station4 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 5 AND vwcmcs.Station4 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 6 AND vwcmcs.Station5 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 6 AND vwcmcs.Station5 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 7 AND vwcmcs.Station6 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 7 AND vwcmcs.Station6 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 9 AND vwcmcs.Station7 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 9 AND vwcmcs.Station7 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 10 AND vwcmcs.Station8 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 10 AND vwcmcs.Station8 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 11 AND vwcmcs.Station9 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 11 AND vwcmcs.Station9 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 12 AND vwcmcs.Station10 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 12 AND vwcmcs.Station10 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 14 AND vwcmcs.Station11 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 14 AND vwcmcs.Station11 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 15 AND vwcmcs.Station12 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 15 AND vwcmcs.Station12 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 16 AND vwcmcs.Station13 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 16 AND vwcmcs.Station13 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 17 AND vwcmcs.Station14 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 17 AND vwcmcs.Station14 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 19 AND vwcmcs.Station15 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 19 AND vwcmcs.Station15 = 'P' THEN 'Pass'
	WHEN vwcmcs.ExaminersSortOrder = 20 AND vwcmcs.Station16 = 'N' THEN 'Fail' 
	WHEN vwcmcs.ExaminersSortOrder = 20 AND vwcmcs.Station16 = 'P' THEN 'Pass'
   END "Grade"	
 , CASE WHEN "vrccsc".ClinicalSlot_AMPM = 1 THEN 'Morning Session'
        WHEN "vrccsc".ClinicalSlot_AMPM = 0 THEN 'Afternoon Session' 
  END "AMPM_Session"
 , "vwcmcs".Session
 , "vwcmcs".Centre
 , "vrccsc"."GivenNames"
 , "vrccsc"."FamilyName"
 , "vrccsc"."Results_ClinicalLastGrade"
 , CONVERT(varchar,"vwcmcs".Examdate,106) "ExamDate"
 FROM   "CTS"."dbo"."vwclinicalmarksheetcandidatescenarios" "vwcmcs"
 INNER JOIN "CTS"."dbo"."vwResults_ClinicalClinicalSlotCandidates" "vrccsc"
    ON "vwcmcs".ExamID = "vrccsc".ExamID AND "vwcmcs".FileNo = "vrccsc".Fileno
	AND "vrccsc".clinicalscenarios_id = "vwcmcs".clinicalscenarios_id
WHERE  "vwcmcs".ExamID = @ExamId AND "vwcmcs"."FileNo"= @FileNo
AND "vrccsc".cmcs_pilot = 0
--AND vwcmcs.clinicalscenarios_id = @ClinicalScenarioId 
	
END
