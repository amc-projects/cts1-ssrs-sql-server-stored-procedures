USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spGeneralCandidateLabel]    Script Date: 18/11/2020 2:37:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGeneralCandidateLabel] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

 SELECT DISTINCT "Candidates"."FamilyName"
 , "Candidates"."Address"
 , "Candidates"."State"
 , "Candidates"."PostCode"
 , "Candidates"."GivenNames"
 , "Candidates"."Fileno"
 , "Candidates"."InvalidAddr"
 ,  ROW_NUMBER() OVER(ORDER BY FamilyName,GivenNames ASC)%2  AS Row#
 , ISNULL("Candidates"."City",'')+' '+ISNULL("Candidates"."State",'')+' '+ISNULL("Candidates"."PostCode",'') "CityAddr"
, CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		THEN 'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ SUBSTRING("GivenNames",CHARINDEX(' ',"GivenNames")+1,1)+' '+"Candidates"."FamilyName" 
		ELSE
		'Dr '+LEFT("Candidates"."GivenNames",1)+ ' '+ "Candidates"."FamilyName"
  END "Name"
  , "Locations"."Description"
 FROM   "CTS"."dbo"."Candidates" "Candidates"
 LEFT OUTER JOIN "CTS"."dbo"."Locations" "Locations" ON "Candidates"."Location"="Locations"."ID"
 INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno"



END
