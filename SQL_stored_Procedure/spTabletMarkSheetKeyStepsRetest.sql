USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTabletMarkSheetKeyStepsRetest]    Script Date: 18/11/2020 3:33:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTabletMarkSheetKeyStepsRetest] @ExamId INT, @FileNo INT, @ExaminerSortOrder INT
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT "vccsks"."status"
 , "vccsks"."ClinicalSlotCandidate_id"
 , "vccsks"."clinicalscenario_id"
 , "vccsks"."clinicalscenarioassessmentkeysteps_questions_key_step_position"
 , "vccsks"."cmcs_conceded_pass_at"
 , "vccsks"."clinicalassessmentkeysteps_assessmentkeysteps_desc"
   FROM  "CTS"."dbo"."vwclinicalmarksheetcandidatescenarios_retest" "vcmcs"
   INNER JOIN "CTS"."dbo"."vwResults_ClinicalStationClinicalSlotCandidates_Retest" "vrccsc"
   ON  "vrccsc"."ExamID"="vcmcs".ExamId  AND "vcmcs".FileNo = "vrccsc".FileNo AND "vrccsc".ClinicalSlotScenarios_Sortorder = "vcmcs".ExaminersSortOrder
   INNER JOIN  "CTS"."dbo"."vwclinicalmarksheetcandidatescenarioassessmentkeystepstatus" "vccsks" ON "vccsks".clinicalscenario_id = "vcmcs".clinicalscenarios_id 
   AND "vccsks".ClinicalSlotCandidate_id = "vrccsc".ClinicalSlotCandidates_id
   WHERE "vrccsc".ExamID = @ExamId AND "vrccsc".FileNo= @FileNo
   AND ExaminersSortOrder = @ExaminerSortOrder
END
