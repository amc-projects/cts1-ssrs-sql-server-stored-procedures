USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spAMCCertificateNotIssuedORInAMCSafe]    Script Date: 18/11/2020 9:53:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAMCCertificateNotIssuedORInAMCSafe]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT "vwCertNotIssueInSafe"."EICS_id", "vwCertNotIssueInSafe"."iMedCountryResidence", "vwCertNotIssueInSafe"."Certno", "vwCertNotIssueInSafe"."Fileno", "vwCertNotIssueInSafe"."PathwayType", "vwCertNotIssueInSafe"."MCQExamPassSession", "vwCertNotIssueInSafe"."ClinOrigExamPassDate", "vwCertNotIssueInSafe"."ClinExamPassSession", "vwCertNotIssueInSafe"."ClinOrigExamPassSession", "vwCertNotIssueInSafe"."Eligibility_Diploma", "vwCertNotIssueInSafe"."Location", "vwCertNotIssueInSafe"."Cert_Status", "vwCertNotIssueInSafe"."Issuedate", "vwCertNotIssueInSafe"."Cert_Comments"
    FROM   "CTS"."dbo"."vwCertNotIssueInSafe" "vwCertNotIssueInSafe"
    WHERE  "vwCertNotIssueInSafe"."Issuedate">={ts '2006-01-01 00:00:00'}
END
