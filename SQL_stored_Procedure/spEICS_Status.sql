USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spEICS_Status]    Script Date: 18/11/2020 12:23:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spEICS_Status] @FileNo int
	-- Add the parameters for the stored procedure here
AS
DECLARE  
  @description varchar(50)
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH q AS
    -- Insert statements for procedure here
	(SELECT  TOP 1 EICS_StatusTypes."Description"
	FROM   "CTS"."dbo"."EICS_Request" "EICS_Request" 
	INNER JOIN "CTS"."dbo"."EICS_Log" "EICS_Log" ON "EICS_Request"."ID"="EICS_Log"."ERCID"
	INNER JOIN "CTS"."dbo"."EICS_StatusTypes" "EICS_StatusTypes" ON "EICS_Log"."StatusTypeID"="EICS_StatusTypes"."ID"
	WHERE  "EICS_Request"."FileNo"= @FileNo
	ORDER BY "EICS_Log"."LogDate" DESC
	)
	, r AS (
	 SELECT Description
	 FROM q
	 UNION
	 SELECT 'No EICS Verification Requested'
	)
	SELECT TOP 1 Description
	FROM r
	ORDER by Description 
    
	RETURN 0
	

END
