USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spRolePlayerSignId]    Script Date: 18/11/2020 2:51:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRolePlayerSignId] @ExamID INT
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ClinicalDates.ExamID
	, ClinicalSlotRolePlayers.SortOrder
	, ClinicalSlot.Colour+' '+'GROUP' "Color"
	, ClinicalSlot.AMPM
	, ClinicalSlotRolePlayers.ExaminerID
	, Addr_Personal.Title+' '+Addr_Personal.Givennames+' '+Addr_Personal.Surname "Name"
	, 'Role-playing' "Type"
	, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'Morning' WHEN ClinicalSlot.AMPM = 0 THEN 'Afternoon' END AS "Slot"
	 FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
	 INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
	 INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
	 INNER JOIN Addr_Personal ON Addr_Personal.Id = ClinicalSlotRolePlayers.ExaminerID
	WHERE ClinicalDates.ExamID = @ExamID AND ClinicalSlotRolePlayers.ExaminerID NOT IN (9555,14622)

END
