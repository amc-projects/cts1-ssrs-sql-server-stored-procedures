USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spRolePlayerPayments]    Script Date: 21/10/2021 12:17:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRolePlayerPayments] @ExamId int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	 BEGIN
	 SELECT  
	 clinical_exams.id "ExamID"
    , ISNULL("Addr_Personal".Title,'')+' '+ISNULL("Addr_Personal".Surname,'') "Name"
		, "Addr_Personal".Surname
		, "Addr_Personal".Givennames
		, exam_assignments.simulated_patient_id "PersonalID"
		, 1 "ContentOrder"
		, clinical_exams.name "ExamSession"
        , UPPER(clinical_venues.name) "ExamCentre"
		, YEAR(clinical_exams.date) "ExamsSeries"
		,  'Role playing' "Venue"
		, UPPER(clinical_venues.name) "HospitalName"
		, CONVERT(VARCHAR(10),  clinical_exams.date, 103) "EventDate"
		,CASE 
			WHEN  AMC.cts.exam_part_sessions.number = 1
				THEN 'AM'
			ELSE 'PM'
		 END TimeDurationDesc
		, tiftu.amount
		, CAST(1 AS REAL) DurationTotal
		, tiftu.unit_numerator
		, tiftu.unit_denominator
		,  tif.description "RolePlayerFeetypes_unit"
		, exam_part_stations.number "Station"
		, '' "Color"
		, Addr_Personal.Initials "Initials"
		, '' "ClinicalslotGroupText"
		,CASE 
			WHEN  AMC.cts.exam_part_sessions.number = 1
				THEN 'AM'
			ELSE 'PM'
		 END AMPM
		, clinical_exams.exam_type_id "ExamTypeID"
		, "addr_addresses"."MasterId"
		, ISNULL("addr_addresses"."Line1",'')+' '+ ISNULL("addr_addresses"."Line2",'')+' '+ISNULL("addr_addresses"."Line3",'')+' '+ ISNULL("addr_addresses"."Line4",'')+' '+ ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ ISNULL("addr_addresses"."Postcode",'') AS "Address"
		, "addr_addresses"."Phone1"
		, "addr_addresses"."Phone2"
		, "addr_addresses"."Mobile"
		, "addr_addresses"."MailAddr"
		, CASE WHEN StatementByASupplier.addr_personal_id IS NOT NULL THEN 'Statement By A Supplier Not Provided'
		ELSE '' END "StatementByASupplier" 
	, (tiftu.amount * CAST(1 AS REAL) * tiftu.unit_numerator) / tiftu.unit_denominator "SubTotal"
	, '$' + CONVERT(varchar,tiftu.amount) +  ' per '  + convert(varchar,tiftu.unit_numerator) + ' ' + CONVERT(varchar,tif.unit) "Sub_Price"    
	 FROM AMC.cts.clinical_exams
	 INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	 INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	 INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	 INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	 INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
	 INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
	 AND exam_assignments.exam_part_session_id = exam_part_sessions.id
	 INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.simulated_patient_id
	 INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	 INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
     INNER JOIN amc.exam_content.scenarios s on exam_part_stations.scenario_id = s.id
     INNER JOIN amc.exam_content.scenarios_simulated_patient_types sspt on s.sp_type_id = sspt.id
     INNER JOIN Address.dbo.TaxInvoiceFeetypes tif on sspt.tax_invoice_fee_type_id = tif.id
	 INNER JOIN Address..TaxInvoiceFeetypeUnits tiftu ON clinical_exams.date BETWEEN tiftu .started_at AND tiftu.ended_at    
			and sspt.tax_invoice_fee_type_id = tiftu.taxinvoicefeetype_id
     LEFT JOIN 
	 Address..StatementByASupplier StatementByASupplier
		ON ((CASE
			WHEN MONTH(clinical_exams.date) > 6 THEN CONVERT(datetime, CAST(YEAR(clinical_exams.date) AS VARCHAR(4)) + '-07-01', 121)
			ELSE CONVERT(datetime, CAST(YEAR(clinical_exams.date)-1 AS VARCHAR(4))   + '-07-01', 121)
	 END ) = StatementByASupplier.financial_year_begin AND addr_personal.ID = StatementByASupplier.addr_personal_id )
	 WHERE
     StatementByASupplier.deleted_at IS NULL 
	 AND  "addr_addresses"."MailAddr"='Y'
	 AND clinical_exams.id = @ExamId
	 END
   ELSE 
   BEGIN
    -- Insert statements for procedure here
	SELECT "vwClinicalRolePlayerTaxInvoice"."ExamID"
	 , "vwClinicalRolePlayerTaxInvoice"."Title"+' '+"vwClinicalRolePlayerTaxInvoice"."Surname" "Name"
	 , "vwClinicalRolePlayerTaxInvoice"."Surname"
	 , "vwClinicalRolePlayerTaxInvoice"."Givennames"
	 , "vwClinicalRolePlayerTaxInvoice"."PersonalID"
	 , "vwClinicalRolePlayerTaxInvoice"."ContentOrder"
	 , "vwClinicalRolePlayerTaxInvoice"."ExamSession"
	 , "vwClinicalRolePlayerTaxInvoice"."ExamCentre"
	 , "vwClinicalRolePlayerTaxInvoice"."ExamsSeries"
	 ,  CASE WHEN "vwClinicalRolePlayerTaxInvoice"."TableName"='ClinicalSlotRolePlayer' THEN 'Role playing' 
	    WHEN "vwClinicalRolePlayerTaxInvoice"."TableName"='ClinicalTrainingRolePlayer' THEN 'Training Night'
		END "Venue"
	 , "vwClinicalRolePlayerTaxInvoice"."HospitalName"
	 , CONVERT(varchar(10),"vwClinicalRolePlayerTaxInvoice"."EventDate",103) "EventDate"
	 , "vwClinicalRolePlayerTaxInvoice"."TimeDurationDesc"
	 , "vwClinicalRolePlayerTaxInvoice"."amount"
	 , "vwClinicalRolePlayerTaxInvoice"."DurationTotal"
	 , "vwClinicalRolePlayerTaxInvoice"."unit_numerator"
	 , "vwClinicalRolePlayerTaxInvoice"."unit_denominator"
	 , "vwClinicalRolePlayerTaxInvoice"."RolePlayerFeetypes_unit"
	 , "vwClinicalRolePlayerTaxInvoice"."Station"
	 , "vwClinicalRolePlayerTaxInvoice"."ClinicalslotColor"+' Group' "Color"
	 , "vwClinicalRolePlayerTaxInvoice"."Initials"
	 , "vwClinicalRolePlayerTaxInvoice"."ClinicalslotGroupText"
	 , "vwClinicalRolePlayerTaxInvoice"."AMPM"
	 , "vwClinicalRolePlayerTaxInvoice"."ExamTypeID"
	 , "addr_addresses"."MasterId"
	 , ISNULL("addr_addresses"."Line1",'')+' '+ ISNULL("addr_addresses"."Line2",'')+' '+ISNULL("addr_addresses"."Line3",'')+' '+ ISNULL("addr_addresses"."Line4",'')+' '+ ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ ISNULL("addr_addresses"."Postcode",'') AS "Address"
	 , "addr_addresses"."Phone1"
	 , "addr_addresses"."Phone2"
	 , "addr_addresses"."Mobile"
	 , "addr_addresses"."MailAddr"
	 , CASE WHEN "vwStatementByASupplierSession".addr_personal_id IS NOT NULL THEN 'Statement By A Supplier Not Provided'
	   ELSE '' END "StatementByASupplier" 
    , (vwClinicalRolePlayerTaxInvoice.amount * vwClinicalRolePlayerTaxInvoice.DurationTotal * vwClinicalRolePlayerTaxInvoice.unit_numerator) / vwClinicalRolePlayerTaxInvoice.unit_denominator "SubTotal"
	, '$' + CONVERT(varchar,vwClinicalRolePlayerTaxInvoice.amount) +  ' per '  + convert(varchar,vwClinicalRolePlayerTaxInvoice.Unit_Denominator) + ' ' + CONVERT(varchar,vwClinicalRolePlayerTaxInvoice.RolePlayerFeetypes_unit) "Sub_Price"
     FROM   "CTS"."dbo"."vwClinicalRolePlayerTaxInvoice" "vwClinicalRolePlayerTaxInvoice"  
	 LEFT OUTER JOIN "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = "vwClinicalRolePlayerTaxInvoice"."PersonalID"
	 LEFT OUTER JOIN "CTS"."dbo"."vwStatementByASupplierSession" "vwStatementByASupplierSession" ON  "vwStatementByASupplierSession"."ExamID"="vwClinicalRolePlayerTaxInvoice"."ExamID" AND "vwStatementByASupplierSession"."addr_personal_id"="vwClinicalRolePlayerTaxInvoice"."PersonalID" AND "vwStatementByASupplierSession"."financial_year_begin" IS  NULL 
     WHERE  "vwClinicalRolePlayerTaxInvoice"."ExamID"= @l_cts_exam_id
	 AND "addr_addresses"."MailAddr"='Y'
    END
END
