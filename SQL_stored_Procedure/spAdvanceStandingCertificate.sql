USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spAdvanceStandingCertificate]    Script Date: 18/11/2020 9:52:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAdvanceStandingCertificate] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- Insert statements for procedure here
	SET NOCOUNT ON;
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	
	SELECT "vwCAAdvStandCert"."GivenNames"+' '+ "vwCAAdvStandCert"."FamilyName" "Name"
	, CONVERT(VARCHAR,"vwCAAdvStandCert"."DateOfBirth",106) "Date_Of_Birth"
	, "vwCAAdvStandCert"."FileNo"
	, "vwCAAdvStandCert"."EICSNumber"
	, CONVERT(VARCHAR,"vwCAAdvStandCert"."StateHistoryDate",106) "StateHistoryDate"
	, "vwCAAdvStandCert"."PathwaySubTypeName"
	, "vwCAAdvStandCert"."CAN_LMCCYNLabel"
	, "vwCAAdvStandCert"."NZ_REXExamYNLabel"
	, "vwCAAdvStandCert"."UK_PLABTestYNLabel"
	, "vwCAAdvStandCert"."US_USMLEStepsYNLabel"
	, "vwCAAdvStandCert"."Ireland_GradYNLabel"
	, "vwCAAdvStandCert"."CAN_LMCCYearQ"
	, "vwCAAdvStandCert"."NZ_REXExamYearQ"
	, "vwCAAdvStandCert"."UK_PLAPTestYearQ"
	, "vwCAAdvStandCert"."US_USMLEStepsYearQ"
	, "vwCAAdvStandCert"."Ireland_GradYearQ"
	, "vwCAAdvStandCert"."CAN_LMCCYearQLabel"
	, "vwCAAdvStandCert"."NZ_REXExamYearQLabel"
	, "vwCAAdvStandCert"."UK_PLAPTestYearQLabel"
	, "vwCAAdvStandCert"."US_USMLEStepsYearQLabel"
	, "vwCAAdvStandCert"."Ireland_GradYearQLabel"
	, "vwCAAdvStandCert"."UK_GradYNLabel"
	, "vwCAAdvStandCert"."UK_GradYearQLabel"
	, "vwCAAdvStandCert"."UK_GradYearQ"
	, "vwCAAdvStandCert"."NZ_InternEvidenceYNLabel"
	, "vwCAAdvStandCert"."UK_FoundationYNLabel"
	, "vwCAAdvStandCert"."UK_TrainingYNLabel"
	, "vwCAAdvStandCert"."US_GMEResidencyYNLabel"
	, "vwCAAdvStandCert"."Ireland_InternEvidenceYNLabel"
	, "vwCAAdvStandCert"."NZ_InternEvidenceYearCLabel"
	, "vwCAAdvStandCert"."UK_FoundationYearCLabel"
	, "vwCAAdvStandCert"."UK_TrainingYearCLabel"
	, "vwCAAdvStandCert"."US_GMEResidencyYearCLabel"
	, "vwCAAdvStandCert"."Ireland_InternEvidenceYearCLabel"
	, "vwCAAdvStandCert"."NZ_InternEvidenceYearC"
	, "vwCAAdvStandCert"."UK_FoundationYearC"
	, "vwCAAdvStandCert"."UK_TrainingYearC"
	, "vwCAAdvStandCert"."US_GMEResidencyYearC"
	, "vwCAAdvStandCert"."Ireland_InternEvidenceYearC"
	, "vwCAAdvStandCert"."QualName"
	, "vwCAAdvStandCert"."MedSchool"+', '+"vwCAAdvStandCert"."CtyTrained" "MedSchool"
	, CONVERT(varchar,"vwCAAdvStandCert"."VerificationDate",106) "VerificationDate"
	, "vwCAAdvStandCert"."CtyTrained"
	, "vwCAAdvStandCert"."PathwaySubTypeId"
	, "vwCAAdvStandCert"."UK_Rotational"
	, "vwCAAdvStandCert"."CAN_LMCCYN"
	, "vwCAAdvStandCert"."NZ_REXExamYN"
	, "vwCAAdvStandCert"."UK_PLABTestYN"
	, "vwCAAdvStandCert"."UK_GradYN"
	, "vwCAAdvStandCert"."US_USMLEStepsYN"
	, "vwCAAdvStandCert"."Ireland_GradYN"
	, "vwCAAdvStandCert"."NZ_InternEvidenceYN"
	, "vwCAAdvStandCert"."UK_FoundationYN"
	, "vwCAAdvStandCert"."UK_TrainingYN"
	, "vwCAAdvStandCert"."US_GMEResidencyYN"
	, "vwCAAdvStandCert"."Ireland_InternEvidenceYN"
	, "vwCAAdvStandCert"."EICSNo"
	, "vwCAAdvStandCert"."Ireland_Rotational"
	, "vwCAAdvStandCert"."US_Rotational"
	, "vwCAAdvStandCert"."CAN_Rotational"
	, "vwCAAdvStandCert"."NZ_Rotational"
 FROM   "CTS"."dbo"."vwCAAdvStandCert" "vwCAAdvStandCert"
 INNER JOIN  #TempList t ON t.FileNo = "vwCAAdvStandCert"."FileNo" 

END
