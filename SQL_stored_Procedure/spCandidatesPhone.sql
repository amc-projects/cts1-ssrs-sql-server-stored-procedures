USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidatesPhone]    Script Date: 29/09/2021 10:39:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidatesPhone] @ExamID int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1

		SELECT CONVERT(VARCHAR(10),  clinical_exams.date, 103)"Examdate"
			, "Candidates"."GivenNames"+' '+UPPER("Candidates"."FamilyName") "Name"
			, "Candidates"."PhoneHome"
			, "Candidates"."PhoneWork"
			, "Candidates"."PhoneMobile"
			, "Candidates"."Fileno"
			, Candidates.GivenNames
			, Candidates.FamilyName
			, clinical_exams.name "Session"
			, UPPER(clinical_venues.name) "Centre"
			, YEAR(clinical_exams.date) "Series"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON exam_assignments.candidate_id="Candidates"."Fileno"
		WHERE clinical_exams.id = @ExamID
		AND candidate_id IS NOT NULL
    ELSE

		SELECT 
		"Exams"."Series"
		, "Exams"."Session"
		, "Exams"."Centre"
		, "Candidates"."GivenNames"+' '+UPPER("Candidates"."FamilyName") "Name"
		, "Candidates"."PhoneHome"
		, "Candidates"."PhoneWork"
		, "Candidates"."PhoneMobile"
		, "Candidates"."Fileno"
		, Candidates.GivenNames
		, Candidates.FamilyName
		, CONVERT(VARCHAR(10),  "Exams".Examdate, 103)"Examdate"
		FROM   (((("CTS"."dbo"."Exams" "Exams" LEFT OUTER JOIN "CTS"."dbo"."ClinicalDates" "ClinicalDates" ON "Exams"."ID"="ClinicalDates"."ExamID")
		LEFT OUTER JOIN "CTS"."dbo"."ClinicalVenues" "ClinicalVenues" ON "ClinicalDates"."ID"="ClinicalVenues"."ClinicalDateID")
		INNER JOIN "CTS"."dbo"."ClinicalSlot" "ClinicalSlot" ON "ClinicalVenues"."ID"="ClinicalSlot"."ClinicalVenueID")
		INNER JOIN "CTS"."dbo"."ClinicalSlotCandidates" "ClinicalSlotCandidates" ON "ClinicalSlot"."ID"="ClinicalSlotCandidates"."ClinicalSlotID")
		INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "ClinicalSlotCandidates"."FileNo"="Candidates"."Fileno"
		WHERE "Exams".id = @l_cts_exam_id
		AND "Candidates"."Fileno" != 1234

   

	
  
END

