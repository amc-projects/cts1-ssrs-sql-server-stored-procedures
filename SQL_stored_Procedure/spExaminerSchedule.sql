USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerSchedule]    Script Date: 18/11/2020 12:29:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerSchedule] @ExamID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH examiners AS 
	(
		SELECT DISTINCT Exams.ID
		, Exams.Session
		, ClinicalSlotExaminers.ExaminerID
		, Exams.Centre
		, CONVERT(VARCHAR,  Exams.Examdate, 106)  AS "Examsdate"
		, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'AM' WHEN ClinicalSlot.AMPM = 0 THEN 'PM' END AS "AM/PM"
		, 'Examiners' "Type"
		, Venues.HospitalName
		, ClinicalSlotExaminers.SortOrder
		, ClinicalSlot.SlotNo
		FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
		INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID 
		INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
		WHERE Exams.ID = @ExamID
		AND ExaminerID NOT IN (9175,9612,9613,9555,10213)

		UNION 

		SELECT DISTINCT Exams.ID
		, Exams.Session
		, ClinicalObservers.ExaminerID
		, Exams.Centre
		, CONVERT(VARCHAR,Exams.Examdate, 106)  AS "Examsdate"
	    , CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
	    , 'Observers' "Type"
		, Venues.HospitalName
		, 0 SortOrder
		, ClinicalSlot.SlotNo
		FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
		INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
		INNER JOIN ClinicalObserverRotations ON ClinicalObservers.ID = ClinicalObserverRotations.ClinicalObserverID
	    INNER JOIN ClinicalSlot ON ClinicalObserverRotations.SetNo = ClinicalSlot.ID
		LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
		WHERE Exams.ID = @ExamID 
		AND ExaminerID NOT IN (9175,9612,9613,9555,10213)

		UNION 

		SELECT DISTINCT Exams.ID
		, Exams.Session
		, ClinicalSlotRolePlayers.ExaminerID
		, Exams.Centre
		, CONVERT(VARCHAR,Exams.Examdate, 106)  AS "Examsdate"
	    , CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
	    , 'Role Players' "Type"
		, Venues.HospitalName
		, ClinicalSlotRolePlayers.SortOrder
		, cs.SlotNo
        FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
		INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalSlot cs ON cs.ClinicalVenueID = ClinicalVenues.ID
		INNER JOIN "CTS"."dbo"."ClinicalSlotRolePlayers" "ClinicalSlotRolePlayers" ON "ClinicalSlotRolePlayers".ClinicalSlotID = cs.ID
		LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
        WHERE Exams.ID = @ExamID
		AND ExaminerID NOT IN (9175,9612,9613,9555,10213, 14622)
	)
	SELECT "Addr_Personal"."Id"
	 ,  "Addr_Personal"."Title"+' '+"Addr_Personal"."Givennames"+' '+"Addr_Personal"."Surname" "Name"
	 , CASE WHEN "addr_speciality"."Speciality" IS NOT NULL AND e.Type!= 'Role Players' THEN '['+"addr_speciality"."Speciality"+']' ELSE '' END "Speciality"
	 , e.Examsdate
	 , e.Session
	 , e.Centre
	 , e.Type
	 , e.[AM/PM]
	 , e.HospitalName
	 , e.SortOrder
	 , e.SlotNo
	FROM   "CTS"."dbo"."Addr_Personal" "Addr_Personal"
	LEFT OUTER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	INNER JOIN examiners e ON e.ExaminerID = Addr_Personal.Id
	WHERE   "addr_addresses"."MailAddr"='Y'


END
