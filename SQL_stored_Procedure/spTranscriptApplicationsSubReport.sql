USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTranscriptApplicationsSubReport]    Script Date: 18/11/2020 3:36:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTranscriptApplicationsSubReport] @FileNo int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT "vwCandidateApplicationTranscript"."ExamsPref1Session"+' - '+"vwCandidateApplicationTranscript"."ExamsPref1Centre" "ExamPref1"
	, "vwCandidateApplicationTranscript"."ExamsPref2Session"+' - '+"vwCandidateApplicationTranscript"."ExamsPref2Centre" "ExamPref2"
	, "vwCandidateApplicationTranscript"."Type"
	, "vwCandidateApplicationTranscript"."applicationstatus"
	, "vwCandidateApplicationTranscript"."FileNo"
	, "vwCandidateApplicationTranscript"."RecordSetOrder"
	, "vwCandidateApplicationTranscript"."ExamsPref1Date"
	, CASE WHEN "vwCandidateApplicationTranscript"."applicationstatus" IN ('accepted', 'delivered') THEN 'Y' ELSE '' END Sched
	, CASE WHEN "vwCandidateApplicationTranscript"."applicationstatus" IN ('declined_waitlist', 'waitlisted') THEN 'Y' ELSE '' END Wait
	, CASE WHEN "vwCandidateApplicationTranscript"."applicationstatus" IN ('declined','declined_waitlist') THEN 'Y' ELSE '' END Reject
    FROM   "CTS"."dbo"."vwCandidateApplicationTranscript" "vwCandidateApplicationTranscript"
    WHERE  "vwCandidateApplicationTranscript"."FileNo"=@FileNo


END
