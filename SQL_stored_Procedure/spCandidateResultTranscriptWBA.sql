USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptWBA]    Script Date: 18/11/2020 11:37:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptWBA] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SELECT "vwWBATranscriptResult"."Authority"
	, "vwWBATranscriptResult"."Hospital"
	, CONVERT(varchar,"vwWBATranscriptResult"."placement_start_date",103) "start_date"
	, CONVERT(varchar,"vwWBATranscriptResult"."placement_meeting_end_date",103) "end_date"
	, CASE WHEN "vwWBATranscriptResult"."final_result" IS NULL OR RTRIM(LTRIM("vwWBATranscriptResult"."final_result"))='' AND "vwWBATranscriptResult"."placement_status" IN ('cancelled', 'withdrawn') THEN "vwWBATranscriptResult"."placement_status"
	  WHEN "vwWBATranscriptResult"."final_result" = 'pass' THEN 'PASS'
	  WHEN "vwWBATranscriptResult"."final_result" = 'fail' THEN 'FAIL'
	  ELSE 'In Progress'
	  END "Result"
	, "vwWBATranscriptResult"."placement_status"
	, "vwWBATranscriptResult"."FileNo"
   FROM   "CTS"."dbo"."vwWBATranscriptResult" "vwWBATranscriptResult"
   WHERE  "vwWBATranscriptResult"."FileNo"= @FileNo
  -- ORDER BY "vwWBATranscriptResult"."placement_start_date"


END
