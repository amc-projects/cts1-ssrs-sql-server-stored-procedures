USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCertificateReissueCA]    Script Date: 18/11/2020 12:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
CREATE PROCEDURE [dbo].[spCertificateReissueCA] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

	SELECT Certs.FileNo
	, candidates.GivenNames+' '+UPPER(candidates.FamilyName) "Name"
	, "Certs"."Certno"
	, CONVERT(VARCHAR,"Certs"."Issuedate",106) "IssueDate"
	, "Images"."Image"
	, CONVERT(VARCHAR,GETDATE(),106) "TodayDate"
	FROM Certs INNER JOIN Candidates ON Certs.FileNo = Candidates.FileNo 
    INNER JOIN vwCredentialCertCA ON vwCredentialCertCA.FileNo = Certs.FileNo
	LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
	INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno"
	WHERE Certs.Status = 'I' 
	AND Certs.IssueDate IS NOT NULL

END
