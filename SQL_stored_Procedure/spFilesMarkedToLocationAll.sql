USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spFilesMarkedToLocationAll]    Script Date: 18/11/2020 2:36:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[spFilesMarkedToLocationAll] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT "Locations"."Description"
	, "Candidates"."Fileno"
	, "Candidates"."FamilyName"
	, "Candidates"."GivenNames"
	, CONVERT(varchar,"Candidates"."MoveDate",103) "MoveDate"
    FROM   "CTS"."dbo"."Locations" "Locations" INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "Locations"."ID"="Candidates"."Location"



END
