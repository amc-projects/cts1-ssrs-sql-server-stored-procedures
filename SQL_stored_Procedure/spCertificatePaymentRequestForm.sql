USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCertificatePaymentRequestForm]    Script Date: 18/11/2020 12:06:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCertificatePaymentRequestForm] @FileNoList varchar(2000)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

    -- Insert statements for procedure here
	SELECT 'Dr '+"vwCredentialCertCAPaymentRequest"."GivenNames"+' '+ "vwCredentialCertCAPaymentRequest"."FamilyName" AS "Name"
	, "vwCredentialCertCAPaymentRequest"."PaymentAmount"
	, "Candidates"."Fileno"
    FROM   "CTS"."dbo"."vwCredentialCertCAPaymentRequest" "vwCredentialCertCAPaymentRequest"
	INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "vwCredentialCertCAPaymentRequest"."FileNo"="Candidates"."Fileno" 
	INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno"
	WHERE PaymentAmount  IN (600, 700, 825)       
	      
END
