USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spRetestResultsConfirmationBOfE]    Script Date: 18/11/2020 2:49:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRetestResultsConfirmationBOfE] (@ExamId int)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	With station1_pilot AS 
	(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 1
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station1_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=1
		)
		, station2_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=2
		)
		, station4_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=4
		)
		, station5_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=5
		)
		, station6_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=6
		)
		, station7_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=7
		)
		, station9_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=9
		)
		, station10_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=10
		)
	
		, station2_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 2
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, station4_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 4
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station5_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 5
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station6_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 6
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station7_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 7
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, station9_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 9
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station10_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 10
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
	

		, result_data  as 
		(
		

		 SELECT Distinct Candidates.fileno FileNo
		 , FamilyName, GivenNames
        , Passfail
		,Station1 "1"
		, Station2 "2"
		, Station3 "4" 
		, Station4 "5" 
		, Station5 "6"
		, Station6 "7"
		, Station7 "9"
		, Station8 "10"
		
		, station1_pilot.clinicalmarksheetcandidatescenario_pilot "Station1_Pilot"
		, station2_pilot.clinicalmarksheetcandidatescenario_pilot "Station2_Pilot"
		, station4_pilot.clinicalmarksheetcandidatescenario_pilot "Station4_Pilot"
		, station5_pilot.clinicalmarksheetcandidatescenario_pilot "Station5_Pilot"
		, station6_pilot.clinicalmarksheetcandidatescenario_pilot "Station6_Pilot"
		, station7_pilot.clinicalmarksheetcandidatescenario_pilot "Station7_Pilot"
		, station9_pilot.clinicalmarksheetcandidatescenario_pilot "Station9_Pilot"
		, station10_pilot.clinicalmarksheetcandidatescenario_pilot "Station10_Pilot"
		, Exams.id "Exams_Id"
		, 'Station 1   '+station1_pilot.ScenType "Station 1 Type"
		, 'Station 2   '+station2_pilot.ScenType  "Station 2 Type"
		, 'Station 4   '+station4_pilot.ScenType "Station 4 Type"
		, 'Station 5   '+station5_pilot.ScenType "Station 5 Type"
		, 'Station 6   '+station6_pilot.ScenType "Station 6 Type"
		, 'Station 7   '+station7_pilot.ScenType "Station 7 Type"
		, 'Station 9   '+station9_pilot.ScenType "Station 9 Type"
		, 'Station 10  '+station10_pilot.ScenType  "Station 10 Type"

		, Exams.ExamTypeID
		, Exams.Session
		, Exams.Centre
		, Year(Exams.Examdate) "ExamDate"
		, Exams.id "Examsid"
		FROM Exams
		INNER JOIN ClinicalDates ON ClinicalDates.ExamID = Exams.ID
		INNER JOIN ClinicalVenues ON ClinicalVenues.ClinicalDateID = ClinicalDates.ID
		INNER JOIN ClinicalSlot ON ClinicalSlot.ClinicalVenueID = ClinicalVenues.ID
		INNER JOIN ClinicalSlotCandidates ON ClinicalSlotCandidates.ClinicalSlotID = ClinicalSlot.ID
		INNER JOIN Candidates ON ClinicalSlotCandidates.FileNo = Candidates.Fileno
		JOIN Results_Clinical_Retest ON Exams.ID = Results_Clinical_Retest.RetestExamID
		JOIN Results_Clinical ON Results_Clinical.ID =  Results_Clinical_Retest.ResultID AND Results_Clinical.FileNo = Candidates.Fileno
		JOIN station1_pilot ON station1_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station2_pilot ON station2_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station4_pilot ON station4_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station5_pilot ON station5_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station6_pilot ON station6_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station7_pilot ON station7_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station9_pilot ON station9_pilot.ClinicalSlot_id = ClinicalSlot.ID
		JOIN station10_pilot ON station10_pilot.ClinicalSlot_id = ClinicalSlot.ID

		WHERE Exams.id = @ExamId
		)

		SELECT 
		  FileNo,UPPER(FamilyName)+', '+GivenNames "Name"
		  ,CASE WHEN Passfail ='FAIL' THEN 'Fail' WHEN Passfail='PASS' THEN 'Pass' WHEN Passfail='RETEST' THEN 'Retest' ELSE Passfail END "Passfail"
		  , result_data."1", result_data."2", result_data."4", result_data."5", result_data."6"
		,result_data."7", result_data."9", result_data."10"
			, CASE WHEN  Station1_Pilot = 0  AND result_data."1"= 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station1_Pilot IS NULL  AND result_data."1"= 'P' AND ExamTypeID = 2 THEN 1  WHEN result_data."1" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation1"
		, CASE WHEN  Station2_Pilot = 0  AND result_data."2" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station2_Pilot IS NULL  AND result_data."2"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."2"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation2"
		, CASE WHEN  Station4_Pilot = 0  AND result_data."4" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station4_Pilot IS NULL  AND result_data."4"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."4" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation4"
		, CASE WHEN  Station5_Pilot = 0 AND result_data."5" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station5_Pilot IS NULL  AND result_data."5"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."5"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation5"
		, CASE WHEN  Station6_Pilot = 0 AND result_data."6" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station6_Pilot IS NULL  AND result_data."6"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."6" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation6"
		, CASE WHEN  Station7_Pilot = 0 AND result_data."7" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station7_Pilot IS NULL  AND result_data."7"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."7" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation7"
		, CASE WHEN  Station9_Pilot = 0  AND result_data."9" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station9_Pilot IS NULL  AND result_data."9"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."9" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation9"
		, CASE WHEN  Station10_Pilot = 0  AND result_data."10" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station10_Pilot IS NULL  AND result_data."10"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."10" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation10"
		, "Station 1 Type"
		, "Station 2 Type"
		, "Station 4 Type"
		, "Station 5 Type"
		, "Station 6 Type"
		, "Station 7 Type"
		, "Station 9 Type"
		, "Station 10 Type"

		, CASE WHEN Station1_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station1_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END "scenario_pilot_1"
		, CASE WHEN Station2_Pilot = 0 AND ExamTypeID = 2 THEN 1  WHEN Station2_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_2"
		, CASE WHEN Station4_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station4_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_4"
		, CASE WHEN Station5_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station5_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_5"
		, CASE WHEN Station6_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station6_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_6"
		, CASE WHEN Station7_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station7_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_7"
		, CASE WHEN Station9_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station9_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_9"
		, CASE WHEN Station10_Pilot = 0 AND ExamTypeID = 2 THEN 1 WHEN Station10_Pilot IS NULL  AND ExamTypeID = 2 THEN 1 WHEN  ExamTypeID = 3 THEN 1 ELSE 0 END  "scenario_pilot_10"
		, Session
		, Centre
		, CASE WHEN  result_data."1"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."1" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation1"
		, CASE WHEN  result_data."2" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."2"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation2"
		, CASE WHEN  result_data."4" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."4" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation4"
		, CASE WHEN  result_data."5" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."5"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation5"
		, CASE WHEN  result_data."6" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."6" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation6"
		, CASE WHEN  result_data."7" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."7" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation7"
		, CASE WHEN  result_data."9" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."9" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation9"
		, CASE WHEN  result_data."10" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."10" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation10"
		, CASE WHEN Passfail = 'PASS' THEN 1 ELSE 0 END "Station_Pass_Count"
		, CASE WHEN Passfail = 'FAIL' THEN 1 ELSE 0 END "Station_Fail_Count"
		, CASE WHEN Passfail = 'RETEST' THEN 1 ELSE 0 END "Station_Retest_Count"
		, CASE WHEN ExamDate < 2018 THEN '16' ELSE '14' END "ScoreCount"
		, "Examsid" 
		FROM result_data 
		

END