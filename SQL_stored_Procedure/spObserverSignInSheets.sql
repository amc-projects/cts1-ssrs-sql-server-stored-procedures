USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spObserverSignInSheets]    Script Date: 17/11/2021 12:41:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spObserverSignInSheets]  @ExamId int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	 BEGIN
	   SELECT clinical_exams.exam_type_id "ExamID"
	, amc.cts.spare_stakeholders.contact_id ExaminerID
	, 'Yes' AM
	, '' PM
	, clinical_venues.id "VenueID"
	, 'MORNING SESSION' "AMPMSession"
	, ISNULL(Title,'')+' '+ISNULL(Initials,'')+' '+ISNULL(Surname,'') "Name"
	, clinical_exams.name "Session"
	,  CONVERT(VARCHAR(15),  clinical_exams.date, 103) "ExamDate"
	,  UPPER(clinical_venues.name) "HospitalName"
	, 'QA Examiner' Type
    FROM AMC.cts.clinical_exams
	INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
	AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
	INNER JOIN  "CTS"."dbo"."Addr_Personal" Addr_Personal ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
    WHERE clinical_exams.id = @ExamId AND Addr_Personal.Surname <> 'REST STATION'
    AND role IN ('QA Examiner')
    AND AMC.cts.exam_part_sessions.number = 1 

  UNION

  SELECT clinical_exams.exam_type_id "ExamID"
	, amc.cts.spare_stakeholders.contact_id ExaminerID
	, '' AM
	, 'Yes' PM
	, clinical_venues.id "VenueID"
	, 'AFTERNOON SESSION' "AMPMSession"
	, ISNULL(Title,'')+' '+ISNULL(Initials,'')+' '+ISNULL(Surname,'') "Name"
	, clinical_exams.name "Session"
	,  CONVERT(VARCHAR(15),  clinical_exams.date, 103) "ExamDate"
	,  UPPER(clinical_venues.name) "HospitalName"
	, 'QA Examiner' Type
    FROM AMC.cts.clinical_exams
	INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
	AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
	INNER JOIN  "CTS"."dbo"."Addr_Personal" Addr_Personal ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
    WHERE clinical_exams.id =  @ExamId AND Addr_Personal.Surname <> 'REST STATION'
    AND role IN ('QA Examiner')
    AND AMC.cts.exam_part_sessions.number != 1 
	 END
    ELSE 
	BEGIN
    -- Insert statements for procedure here
	SELECT ClinicalDates.ExamID
	, ClinicalObservers.ExaminerID
	, ClinicalVenues.AM
	, ClinicalVenues.PM
	, ClinicalVenues.VenueID
	, 'MORNING SESSION' "AMPMSession"
	, ISNULL(Title,'')+' '+ISNULL(Initials,'')+' '+ISNULL(Surname,'') "Name"
	, "Exams".Session
	, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
	, "Venues"."HospitalName"
	, 'Observer' Type
	FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
	INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
	INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalObservers.ExaminerID
	INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
	INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID   
	WHERE ClinicalDates.ExamID = @l_cts_exam_id
	And AM = 'Yes'

	UNION

	SELECT ClinicalDates.ExamID
	, ClinicalObservers.ExaminerID
	, ClinicalVenues.AM
	, ClinicalVenues.PM
	, ClinicalVenues.VenueID
	, 'AFTERNOON SESSION' "AMPMSession"
	, ISNULL(Title,'')+' '+ISNULL(Initials,'')+' '+ISNULL(Surname,'') "Name"
	, "Exams".Session
	, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
	, "Venues"."HospitalName"
	, 'Observer' Type
	FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
	INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
	INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalObservers.ExaminerID
	INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
	INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID   
	WHERE ClinicalDates.ExamID = @l_cts_exam_id And PM = 'Yes' 
	AND 
	 ClinicalObservers.ExaminerID NOT IN (
	 SELECT DISTINCT ClinicalSlotExaminers.ExaminerID
	 FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
	 INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
	 INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
	 WHERE ClinicalDates.ExamID = @l_cts_exam_id
	)
	AND ClinicalObservers.ExaminerID NOT IN 
	 (
	 SELECT  ClinicalObservers.ExaminerID
	 FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
	  INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
	  Where ClinicalDates.ExamID = @l_cts_exam_id
	)

	END
END
