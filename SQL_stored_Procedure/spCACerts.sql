USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCACerts]    Script Date: 18/11/2020 9:55:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCACerts] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

	SELECT "Candidates"."GivenNames"+' '+UPPER("Candidates"."FamilyName") "Name"
	, "Certs"."Certno"
	, "Images"."Image"
	,  "Candidates".FileNo  
	, "Candidates"."GivenNames"
	, "Candidates"."FamilyName"
	, CAST(DAY(Issuedate) AS VARCHAR(2)) + ' ' + DATENAME(MM, Issuedate) + ' ' + CAST(YEAR(Issuedate) AS VARCHAR(4)) AS "IssueDate"
	FROM   (("CTS"."dbo"."Candidates" "Candidates" INNER JOIN "CTS"."dbo"."Certs" "Certs" ON "Candidates"."Fileno"="Certs"."Fileno")
	 INNER JOIN vwCredentialCollection ON vwCredentialCollection.FileNo = Certs.FileNo
	 LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo")
	 LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
	 INNER JOIN  #TempList t ON t.FileNo = "Candidates"."Fileno"                 
	 WHERE  Certs.IssueDate IS NOT NULL AND Certs.Status = 'I' AND Certs.Issuedthrough IS NOT NULL

END
