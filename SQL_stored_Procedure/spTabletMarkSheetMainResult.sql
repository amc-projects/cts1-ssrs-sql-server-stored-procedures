USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTabletMarkSheetMainResult]    Script Date: 18/11/2020 3:33:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTabletMarkSheetMainResult] @ExamId int, @FileNo int
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT DISTINCT 
 'Station'+STR("vwResults_ClinicalStationClinicalSlotCandidates"."ClinicalSlotScenarios_Sortorder") "Station"
 , "vwResults_ClinicalStationClinicalSlotCandidates"."ClinicalSlotCandidates_Period"
 , CASE 
    WHEN  pilot= 0 AND "vwResults_ClinicalStationClinicalSlotCandidates"."Results_Clinical_Exam_Stations" = 'P' THEN 'Pass'
	WHEN  pilot=0 AND "vwResults_ClinicalStationClinicalSlotCandidates"."Results_Clinical_Exam_Stations" = 'N' THEN 'Fail'
	WHEN pilot = 1 THEN 'Assessed, not Scored'
	WHEN "vwResults_ClinicalStationClinicalSlotCandidates"."Results_Clinical_Exam_Stations" = 'REST' Then 'NA           Rest Station'  
	END "Station_Result"
  , CASE 
   WHEN  "vwResults_ClinicalStationClinicalSlotCandidates"."ScenType" IN ('Child Health')
     THEN '*   '+"vwResults_ClinicalStationClinicalSlotCandidates"."ScenType"
   WHEN  "vwResults_ClinicalStationClinicalSlotCandidates"."ScenType" IN ('Womens Health Gynaecology','Womens Health Obstetrics')
     THEN '**   '+"vwResults_ClinicalStationClinicalSlotCandidates"."ScenType"
   ELSE 
   "vwResults_ClinicalStationClinicalSlotCandidates"."ScenType"
   END "ScenType"
 , "vwResults_ClinicalStationClinicalSlotCandidates"."Results_ClinicalLastGrade"
 , "vwResults_ClinicalStationClinicalSlotCandidates"."ExamID"
 , "vwResults_ClinicalStationClinicalSlotCandidates"."Fileno"
 , "vwResults_ClinicalStationClinicalSlotCandidates"."ClinicalIndividualExam"
 , "vwResults_ClinicalStationClinicalSlotCandidates"."pilot"
 , CONVERT(VARCHAR,"vwResults_ClinicalStationClinicalSlotCandidates"."ExamsDate",106) "ExamDate"
 , "vwResults_ClinicalStationClinicalSlotCandidates".GivenNames+' '+"vwResults_ClinicalStationClinicalSlotCandidates".FamilyName "Name"
 , "vwResults_ClinicalStationClinicalSlotCandidates".ExamsCentre
 , "vwResults_ClinicalStationClinicalSlotCandidates".ExamSession
 , CASE WHEN "vwResults_ClinicalStationClinicalSlotCandidates".ClinicalSlot_AMPM = 1 THEN 'Morning Session'
        WHEN "vwResults_ClinicalStationClinicalSlotCandidates".ClinicalSlot_AMPM = 0 THEN 'Afternoon Session' 
  END "AMPM_Session"
  ,"vwResults_ClinicalStationClinicalSlotCandidates"."ClinicalSlotScenarios_Sortorder"
 FROM   "CTS"."dbo"."vwResults_ClinicalStationClinicalSlotCandidates" "vwResults_ClinicalStationClinicalSlotCandidates"
 WHERE  "vwResults_ClinicalStationClinicalSlotCandidates"."ExamID"=@ExamID AND "vwResults_ClinicalStationClinicalSlotCandidates"."Fileno"=@FileNo

END
