USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerMergeLetter]    Script Date: 18/11/2020 12:26:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerMergeLetter] @ExamId AS INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT "vwClinicalExaminerTaxInvoice"."ExamSession"
	 , "vwClinicalExaminerTaxInvoice"."ExamCentre"
	 , "vwClinicalExaminerTaxInvoice"."ExamID"
	 , "vwClinicalExaminerTaxInvoice"."PersonalID"
	 , "vwClinicalExaminerTaxInvoice"."ContentOrder"
	 , "vwClinicalExaminerTaxInvoice"."Title" + ' '+ "vwClinicalExaminerTaxInvoice"."Givennames"+' '+"vwClinicalExaminerTaxInvoice"."Surname" "Name"
	 , "vwClinicalExaminerTaxInvoice"."HospitalName"
	 , CONVERT(VARCHAR,  "vwClinicalExaminerTaxInvoice"."EventDate", 103)  AS "Eventdate" 
	 , "vwClinicalExaminerTaxInvoice"."ClinicalslotColor"
	 , "vwClinicalExaminerTaxInvoice"."ClinicalSlotExaminersGroupNo"
	 , CASE 
	    WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 0 THEN 'Chair('+"vwClinicalExaminerTaxInvoice"."TimeDurationDesc"+')'  
	    WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 3 THEN  'Observing('+"vwClinicalExaminerTaxInvoice"."TimeDurationDesc"+')'  
	   	WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 4 THEN "vwClinicalExaminerTaxInvoice"."TimeDurationDesc"
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 5 THEN "vwClinicalExaminerTaxInvoice"."TimeDurationDesc"
		ELSE ''
       END "TimeDurationDesc"
	 ,  CASE 
	   	WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 4 THEN "vwClinicalExaminerTaxInvoice"."Quantity"
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 5 THEN "vwClinicalExaminerTaxInvoice"."Quantity"
		ELSE ''
       END "Quantity"
	 , "vwClinicalExaminerTaxInvoice"."Station"
	 , CASE
	     WHEN "vwClinicalExaminerTaxInvoice"."AMPM" = 1 THEN 'AM'
		 WHEN "vwClinicalExaminerTaxInvoice"."AMPM" = 0 THEN 'PM'
       END "AM/PM"     
	 , CASE 
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 0 THEN 'Chairperson Venue'
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 1 THEN 'Examining Venue'
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 2 THEN 'Role Playing Venue'
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 3 THEN 'Observing Venue'
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 4 THEN 'Panel Meeting Attendance Venue'
		WHEN "vwClinicalExaminerTaxInvoice"."ContentOrder" = 5 THEN 'Examiner Training Workshop Attendance Venue'
	 END "Venue"
	 FROM   "CTS"."dbo"."vwClinicalExaminerTaxInvoice" "vwClinicalExaminerTaxInvoice"
	 WHERE  "vwClinicalExaminerTaxInvoice"."ExamID"= @ExamId
 
END
