USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spResultsConfirmationBOfE]    Script Date: 06/12/2021 12:12:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spResultsConfirmationBOfE] (@ExamId int)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID
	IF @l_exam_type_id = 1 
	 BEGIN
	   SELECT 
 "Candidates"."Fileno"
 ,UPPER("Candidates".FamilyName)+', '+"Candidates".GivenNames "Name"
 , Passfail
, Station1 "1"
, Station2 "2"
, Station3 "4" 
, Station4 "5" 
, Station5 "6"
, Station6 "7"
, Station7 "9"
, Station8 "10"
, Station9 "11"
, Station10 "12"
, Station11 "14"
, Station12 "15"
, Station13 "16"
, Station14 "17"
, Station15 "19"
, Station16 "20"
,  ''  "rStation1"
,  '' "rStation2"
		,  ''  "rStation4"
		,  ''  "rStation5"
		,  ''   "rStation6"
		,  ''  "rStation7"
		,  ''  "rStation9"
		,  ''  "rStation10"
		,  ''  "rStation11"
		,  ''  "rStation12"
		,  ''  "rStation14"
		,  ''  "rStation15"
		,  ''  "rStation16"
		,  ''  "rStation17"
		,  ''  "rStation19"
		,  ''  "rStation20"
		,''  "Station 1 Type"
		, '' "Station 2 Type"
		, '' "Station 4 Type"
		, '' "Station 5 Type"
		, '' "Station 6 Type"
		, '' "Station 7 Type"
		, '' "Station 9 Type"
		, '' "Station 10 Type"
		, '' "Station 11 Type"
		, '' "Station 12 Type"
		, '' "Station 14 Type"
		, '' "Station 15 Type"
		, '' "Station 16 Type"
		, '' "Station 17 Type"
		, '' "Station 19 Type"
		, '' "Station 20 Type"
		, "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_1 "scenario_pilot_1"
		, "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_2 "scenario_pilot_2"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_4 "scenario_pilot_4"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_5 "scenario_pilot_5"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_6 "scenario_pilot_6"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_7 "scenario_pilot_7"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_9 "scenario_pilot_9"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_10  "scenario_pilot_10"
		, "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_11 "scenario_pilot_11"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_12 "scenario_pilot_12"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_14 "scenario_pilot_14"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_15 "scenario_pilot_15"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_16 "scenario_pilot_16"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_17 "scenario_pilot_17"
		,  "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_19 "scenario_pilot_19"
		, "Results_ClinicalSerialize".clinicalmarksheetcandidatescenario_pilot_20 "scenario_pilot_20"
		, clinical_exams.name  Session
		, clinical_venues.name Centre
		, '' "cStation1"
		, '' "cStation2"
		, '' "cStation4"
		, '' "cStation5"
		, '' "cStation6"
		, '' "cStation7"
		, '' "cStation9"
		, '' "cStation10"
		, '' "cStation11"
		, '' "cStation12"
		, '' "cStation14"
		, '' "cStation15"
		, '' "cStation16"
		, '' "cStation17"
		, '' "cStation19"
,'' "cStation20"
, CASE WHEN Passfail = 'PASS' THEN 1 ELSE 0 END "Station_Pass_Count"
, CASE WHEN Passfail = 'FAIL' THEN 1 ELSE 0 END "Station_Fail_Count"
, CASE WHEN Passfail = 'RETEST' THEN 1 ELSE 0 END "Station_Retest_Count"
, CASE WHEN ExamDate < 2018 THEN '16' ELSE '14' END "ScoreCount"
, clinical_exams.id "Examsid" 
, CONVERT(VARCHAR(10),  clinical_exams.date, 103) "examdate"
FROM AMC.cts.clinical_exams
INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
AND exam_assignments.exam_part_session_id = exam_part_sessions.id
INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON exam_assignments.candidate_id="Candidates"."Fileno" 
INNER JOIN "CTS"."dbo"."Results_ClinicalSerialize" "Results_ClinicalSerialize" ON clinical_exams.cts_exam_id ="Results_ClinicalSerialize"."ExamID"
AND  "Candidates"."Fileno" = "Results_ClinicalSerialize".Fileno
INNER JOIN "CTS"."dbo"."Eligibility" "Eligibility" ON "Results_ClinicalSerialize"."FileNo"="Eligibility"."Fileno"
WHERE clinical_exams.id  = @ExamId
--AND  "Results_ClinicalSerialize"."Passfail" = @Grade


	 END
    ELSE

	BEGIN
	With station1_pilot AS 
	(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 1
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station1_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=1
		)
		, station2_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=2
		)
		, station4_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=4
		)
		, station5_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=5
		)
		, station6_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=6
		)
		, station7_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=7
		)
		, station9_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=9
		)
		, station10_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=10
		)
		, station11_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=11
		)
		, station12_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=12
		)
		, station14_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=14
		)
		, station15_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=15
		)
		, station16_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=16
		)
		, station17_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=17
		)
		, station19_SceneType AS
		(
			SELECT Scenario_ScenType
				, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=19
		)
		, station20_SceneType AS
		(
			SELECT Scenario_ScenType
			, ExamID
			FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
			WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@l_cts_exam_id 
			AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"=20
		)
		, station2_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 2
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, station4_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 4
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station5_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 5
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station6_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 6
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station7_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 7
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, station9_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 9
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station10_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 10
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station11_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 11
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station12_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 12
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, station14_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 14
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station15_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 15
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station16_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 16
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station17_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 17
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, station19_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 19
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)
		, station20_pilot AS 
		(
		SELECT DISTINCT ClinicalSlot.ID ClinicalSlot_id
					,ClinicalSlotScenarios.Sortorder ClinicalSlotScenario_Sortorder
					,clinicalmarksheetcandidatescenarios.pilot clinicalmarksheetcandidatescenario_pilot
					,CAST(ClinicalSlotScenarios.ScenarioID AS INT) Scenario_ID
					,ScenarioTypes.ScenType
				FROM ClinicalSlot
				INNER JOIN clinicalmarksheets ON clinicalmarksheets.ClinicalSlot_id = ClinicalSlot.ID
				INNER JOIN clinicalmarksheetcandidates ON clinicalmarksheetcandidates.clinicalmarksheet_id = clinicalmarksheets.id
				INNER JOIN clinicalmarksheetcandidatescenarios ON clinicalmarksheetcandidates.id = clinicalmarksheetcandidatescenarios.clinicalmarksheetcandidate_id
				INNER JOIN clinicalscenarios ON clinicalmarksheetcandidatescenarios.clinicalscenario_id = clinicalscenarios.id
				INNER JOIN ClinicalSlotScenarios ON ClinicalSlotScenarios.ClinicalSlotID = ClinicalSlot.ID
					AND CAST(ClinicalSlotScenarios.ScenarioID AS VARCHAR(10)) COLLATE DATABASE_DEFAULT = CAST(clinicalscenarios.scenario_id AS VARCHAR(10)) COLLATE DATABASE_DEFAULT
				INNER JOIN Scenarios..Scenario Scenario ON CAST(ClinicalSlotScenarios.ScenarioID AS INT) = Scenario.ID
				INNER JOIN Scenarios..ScenarioTypes ScenarioTypes ON ScenarioTypes.ScenID = Scenario.ID
				WHERE ClinicalSlotScenarios.Sortorder = 20
				AND ISNUMERIC(ClinicalSlotScenarios.ScenarioID) = 1
		)

		, result_data  as 
		(
		SELECT Distinct Candidates.fileno FileNo
		, FamilyName, GivenNames
		, Passfail
		, Station1 "1"
		, Station2 "2"
		, Station3 "4" 
		, Station4 "5" 
		, Station5 "6"
		, Station6 "7"
		, Station7 "9"
		, Station8 "10"
		, Station9 "11"
		, Station10 "12"
		, Station11 "14"
		, Station12 "15"
		, Station13 "16"
		, Station14 "17"
		, Station15 "19"
		, Station16 "20"
		, station1_pilot.clinicalmarksheetcandidatescenario_pilot "Station1_Pilot"
		, station2_pilot.clinicalmarksheetcandidatescenario_pilot "Station2_Pilot"
		, station4_pilot.clinicalmarksheetcandidatescenario_pilot "Station4_Pilot"
		, station5_pilot.clinicalmarksheetcandidatescenario_pilot "Station5_Pilot"
		, station6_pilot.clinicalmarksheetcandidatescenario_pilot "Station6_Pilot"
		, station7_pilot.clinicalmarksheetcandidatescenario_pilot "Station7_Pilot"
		, station9_pilot.clinicalmarksheetcandidatescenario_pilot "Station9_Pilot"
		, station10_pilot.clinicalmarksheetcandidatescenario_pilot "Station10_Pilot"
		, station11_pilot.clinicalmarksheetcandidatescenario_pilot "Station11_Pilot"
		, station12_pilot.clinicalmarksheetcandidatescenario_pilot "Station12_Pilot"
		, station14_pilot.clinicalmarksheetcandidatescenario_pilot "Station14_Pilot"
		, station15_pilot.clinicalmarksheetcandidatescenario_pilot "Station15_Pilot"
		, station16_pilot.clinicalmarksheetcandidatescenario_pilot "Station16_Pilot"
		, station17_pilot.clinicalmarksheetcandidatescenario_pilot "Station17_Pilot"
		, station19_pilot.clinicalmarksheetcandidatescenario_pilot "Station19_Pilot"
		, station20_pilot.clinicalmarksheetcandidatescenario_pilot "Station20_Pilot"
		, Exams.id "Exams_Id"
		, station1_SceneType.Scenario_ScenType "Station 1 Type"
		, station2_SceneType.Scenario_ScenType "Station 2 Type"
		, station4_SceneType.Scenario_ScenType "Station 4 Type"
		, station5_SceneType.Scenario_ScenType "Station 5 Type"
		, station6_SceneType.Scenario_ScenType "Station 6 Type"
		, station7_SceneType.Scenario_ScenType "Station 7 Type"
		, station9_SceneType.Scenario_ScenType "Station 9 Type"
		, station10_SceneType.Scenario_ScenType  "Station 10 Type"
		, station11_SceneType.Scenario_ScenType "Station 11 Type"
		, station12_SceneType.Scenario_ScenType "Station 12 Type"
		, station14_SceneType.Scenario_ScenType "Station 14 Type"
		, station15_SceneType.Scenario_ScenType "Station 15 Type"
		, station16_SceneType.Scenario_ScenType "Station 16 Type"
		, station17_SceneType.Scenario_ScenType "Station 17 Type"
		, station19_SceneType.Scenario_ScenType "Station 19 Type"
		, station20_SceneType.Scenario_ScenType "Station 20 Type"
		, Exams.ExamTypeID
		, Exams.Session
		, Exams.Centre
		, Year(Exams.Examdate) "ExamDateYear"
		, "Exams".id  "Examsid"
		, Exams.Examdate "examdate"
		FROM Exams
		INNER JOIN Scenarios..[questions_clinical_exam_groups] ON [questions_clinical_exam_groups].id = Exams.clinical_exam_group_id
		INNER JOIN ClinicalDates ON ClinicalDates.ExamID = Exams.ID
		INNER JOIN ClinicalVenues ON ClinicalVenues.ClinicalDateID = ClinicalDates.ID
		INNER JOIN ClinicalSlot ON ClinicalSlot.ClinicalVenueID = ClinicalVenues.ID
		INNER JOIN ClinicalSlotCandidates ON ClinicalSlotCandidates.ClinicalSlotID = ClinicalSlot.ID
		INNER JOIN Candidates ON ClinicalSlotCandidates.FileNo = Candidates.Fileno
		INNER JOIN Results_Clinical ON Exams.ID = Results_Clinical.ExamID AND Results_Clinical.FileNo = Candidates.Fileno
		INNER JOIN Results_Clinical_Exam ON Results_Clinical.ID = Results_Clinical_Exam.ResultID
		LEFT OUTER JOIN station1_pilot ON station1_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station2_pilot ON station2_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station4_pilot ON station4_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station5_pilot ON station5_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station6_pilot ON station6_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station7_pilot ON station7_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station9_pilot ON station9_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station10_pilot ON station10_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station11_pilot ON station11_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station12_pilot ON station12_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station14_pilot ON station14_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station15_pilot ON station15_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station16_pilot ON station16_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station17_pilot ON station17_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station19_pilot ON station19_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station20_pilot ON station20_pilot.ClinicalSlot_id = ClinicalSlot.ID
		LEFT OUTER JOIN station1_SceneType ON station1_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station2_SceneType ON station2_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station4_SceneType ON station4_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station5_SceneType ON station5_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station6_SceneType ON station6_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station7_SceneType ON station7_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station9_SceneType ON station9_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station10_SceneType ON station10_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station11_SceneType ON station11_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station12_SceneType ON station12_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station14_SceneType ON station14_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station15_SceneType ON station15_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station16_SceneType ON station16_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station17_SceneType ON station17_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station19_SceneType ON station19_SceneType.ExamID = Exams.id
		LEFT OUTER JOIN station20_SceneType ON station20_SceneType.ExamID = Exams.id
		WHERE Exams.id = @l_cts_exam_id 

	

		
		)

		SELECT 
		  FileNo,UPPER(FamilyName)+', '+GivenNames "Name"
		  ,CASE WHEN Passfail ='FAIL' THEN 'Fail' WHEN Passfail='PASS' THEN 'Pass' WHEN Passfail='RETEST' THEN 'Retest' ELSE Passfail END "Passfail"
		  , result_data."1", result_data."2", result_data."4", result_data."5", result_data."6"
		,result_data."7", result_data."9", result_data."10", result_data."11", result_data."12",result_data."14", result_data."15", result_data."16", result_data."17", result_data."19", result_data."20"
			, CASE WHEN  Station1_Pilot = 0  AND result_data."1"= 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station1_Pilot IS NULL  AND result_data."1"= 'P' AND ExamTypeID = 2 THEN 1  WHEN result_data."1" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation1"
		, CASE WHEN  Station2_Pilot = 0  AND result_data."2" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station2_Pilot IS NULL  AND result_data."2"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."2"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation2"
		, CASE WHEN  Station4_Pilot = 0  AND result_data."4" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station4_Pilot IS NULL  AND result_data."4"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."4" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation4"
		, CASE WHEN  Station5_Pilot = 0 AND result_data."5" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station5_Pilot IS NULL  AND result_data."5"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."5"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation5"
		, CASE WHEN  Station6_Pilot = 0 AND result_data."6" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station6_Pilot IS NULL  AND result_data."6"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."6" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation6"
		, CASE WHEN  Station7_Pilot = 0 AND result_data."7" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station7_Pilot IS NULL  AND result_data."7"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."7" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation7"
		, CASE WHEN  Station9_Pilot = 0  AND result_data."9" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station9_Pilot IS NULL  AND result_data."9"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."9" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation9"
		, CASE WHEN  Station10_Pilot = 0  AND result_data."10" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station10_Pilot IS NULL  AND result_data."10"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."10" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "rStation10"
		, CASE WHEN  Station11_Pilot = 0  AND result_data."11" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station11_Pilot IS NULL  AND result_data."11"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation11"
		, CASE WHEN  Station12_Pilot = 0  AND result_data."12" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station12_Pilot IS NULL  AND result_data."12"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation12"
		, CASE WHEN  Station14_Pilot = 0  AND result_data."14"= 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station14_Pilot IS NULL  AND result_data."14"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation14"
		, CASE WHEN  Station15_Pilot = 0  AND result_data."15" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station15_Pilot IS NULL  AND result_data."15"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation15"
		, CASE WHEN  Station16_Pilot = 0  AND result_data."16" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station16_Pilot IS NULL  AND result_data."16"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation16"
		, CASE WHEN  Station17_Pilot = 0  AND result_data."17" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station17_Pilot IS NULL  AND result_data."17"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation17"
		, CASE WHEN  Station19_Pilot = 0  AND result_data."19" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station19_Pilot IS NULL  AND result_data."19"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation19"
		, CASE WHEN  Station20_Pilot = 0  AND result_data."20" = 'P' AND ExamTypeID = 2 THEN 1 WHEN  Station20_Pilot IS NULL  AND result_data."20"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "rStation20"
		, "Station 1 Type"
		, "Station 2 Type"
		, "Station 4 Type"
		, "Station 5 Type"
		, "Station 6 Type"
		, "Station 7 Type"
		, "Station 9 Type"
		, "Station 10 Type"
		, "Station 11 Type"
		, "Station 12 Type"
		, "Station 14 Type"
		, "Station 15 Type"
		, "Station 16 Type"
		, "Station 17 Type"
		, "Station 19 Type"
		, "Station 20 Type"
		, CASE WHEN Station1_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station1_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END "scenario_pilot_1"
		, CASE WHEN Station2_Pilot = 0 AND ExamTypeID = 2 THEN 0  WHEN Station2_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_2"
		, CASE WHEN Station4_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station4_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_4"
		, CASE WHEN Station5_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station5_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_5"
		, CASE WHEN Station6_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station6_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_6"
		, CASE WHEN Station7_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station7_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_7"
		, CASE WHEN Station9_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station9_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_9"
		, CASE WHEN Station10_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station10_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 WHEN  ExamTypeID = 3 THEN 0 ELSE 1 END  "scenario_pilot_10"
		, CASE WHEN Station11_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station11_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_11"
		, CASE WHEN Station12_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station12_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_12"
		, CASE WHEN Station14_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station14_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_14"
		, CASE WHEN Station15_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station15_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_15"
		, CASE WHEN Station16_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station16_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_16"
		, CASE WHEN Station17_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station17_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_17"
		, CASE WHEN Station19_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station19_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_19"
		, CASE WHEN Station20_Pilot = 0 AND ExamTypeID = 2 THEN 0 WHEN Station20_Pilot IS NULL  AND ExamTypeID = 2 THEN 0 ELSE 1 END  "scenario_pilot_20"
		, Session
		, Centre
		, CASE WHEN  result_data."1"= 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."1" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation1"
		, CASE WHEN  result_data."2" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."2"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation2"
		, CASE WHEN  result_data."4" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."4" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation4"
		, CASE WHEN  result_data."5" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."5"= 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation5"
		, CASE WHEN  result_data."6" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."6" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation6"
		, CASE WHEN  result_data."7" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."7" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation7"
		, CASE WHEN  result_data."9" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."9" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation9"
		, CASE WHEN  result_data."10" = 'P' AND ExamTypeID = 2 THEN 1 WHEN result_data."10" = 'P' AND ExamTypeID = 3 THEN 1 ELSE 0 END "cStation10"
		, CASE WHEN  result_data."11" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation11"
		, CASE WHEN  result_data."12" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation12"
		, CASE WHEN  result_data."14"= 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation14"
		, CASE WHEN  result_data."15" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation15"
		, CASE WHEN  result_data."16" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation16"
		, CASE WHEN  result_data."17" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation17"
		, CASE WHEN  result_data."19" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation19"
		, CASE WHEN  result_data."20" = 'P' AND ExamTypeID = 2 THEN 1 ELSE 0 END "cStation20"
		, CASE WHEN Passfail = 'PASS' THEN 1 ELSE 0 END "Station_Pass_Count"
		, CASE WHEN Passfail = 'FAIL' THEN 1 ELSE 0 END "Station_Fail_Count"
		, CASE WHEN Passfail = 'RETEST' THEN 1 ELSE 0 END "Station_Retest_Count"
		, CASE WHEN ExamDateYear < 2018 THEN '16' ELSE '14' END "ScoreCount"
		, "Examsid" 
		, CONVERT(VARCHAR(10), examdate, 103) "examdate"
		FROM result_data 
		
   END
END