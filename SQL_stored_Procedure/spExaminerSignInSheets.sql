USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerSignInSheets]    Script Date: 09/11/2021 11:49:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerSignInSheets] @ExamId int
	-- Add the parameters for the stored procedure here
AS
DECLARE @ExamTypeId int;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;

	---This is CTS Exam Type in Face to Face
	SELECT  @ExamTypeId = ExamTypeID
	FROM dbo.Exams WHERE id = @l_cts_exam_id
	
	--This returns Online or Face to Face
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	BEGIN
	 SELECT 
	  clinical_exams.id "ExamID"
	, exam_assignments.examiner_id "ExaminerID"
	, 'BLUE' Colour
	, AMC.cts.exam_part_sessions.number "AMPM" 
	, exam_part_stations.number "SortOrder"
	, NULL "GroupText"
	, clinical_exams.venue_id "VenueID"
	, clinical_exams.name "Session"
	, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
	, UPPER(clinical_venues.name) "HospitalName" 
	, CASE 
		WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'MORNING SESSION'
		ELSE 'AFTERNOON SESSION'
		END "AMPMSession"
	, CONVERT(VARCHAR(10),  clinical_exams.date, 103)"ExamDate"
	, '' "TitleSubPart"
	FROM AMC.cts.clinical_exams
	INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
	INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
	AND exam_assignments.exam_part_session_id = exam_part_sessions.id
	INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.examiner_id
	INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	WHERE   "addr_addresses"."MailAddr"='Y'
	AND clinical_exams.id = @ExamID
	AND examiner_id IS NOT NULL

	END

    ELSE

	BEGIN

		IF @ExamTypeId = 2 
		--Main Exam
		 BEGIN
		   SELECT DISTINCT
			ClinicalDates.ExamID
		  , ClinicalSlotExaminers.ExaminerID
		  , ClinicalSlot.Colour
		  , ClinicalSlot.AMPM
		  , ClinicalSlotExaminers.SortOrder
		  , ClinicalSlot.GroupText
		  , ClinicalVenues.VenueID
		  , "Exams"."Session"
		  , ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
		  , "Venues"."HospitalName"
		  , CASE WHEN 
			ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
			ELSE
			 'AFTERNOON SESSION'
			END "AMPMSession" 
		 , CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"	
		  , '' "TitleSubPart"
		  FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		  INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
		  INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		  INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotExaminers.ExaminerID
		  INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
		  INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID   
		  WHERE ClinicalDates.ExamID = @l_cts_exam_id
		  AND ClinicalSlotExaminers.ExaminerID NOT IN (9555,14622)
		  AND ClinicalSlot.colour IN ('BLUE', 'ORANGE', 'GREEN', 'YELLOW')
		  AND ClinicalSlot.AMPM = 1 
		  UNION
		 SELECT DISTINCT
		  ClinicalDates.ExamID
		, ClinicalSlotExaminers.ExaminerID
		, ClinicalSlot.Colour
		, ClinicalSlot.AMPM
		, ClinicalSlotExaminers.SortOrder
		, ClinicalSlot.GroupText
		, ClinicalVenues.VenueID
		, "Exams"."Session"
		, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
		, "Venues"."HospitalName"
		, CASE WHEN 
		ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
		ELSE
			'AFTERNOON SESSION'
		END "AMPMSession" 
		 , CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
		, '' "TitleSubPart"
		FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
		INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotExaminers.ExaminerID
		INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
		INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID
		WHERE ClinicalDates.ExamID = @l_cts_exam_id
		AND ClinicalSlotExaminers.ExaminerID NOT IN (9555,14622)
		AND ClinicalSlot.colour IN ('BLUE', 'ORANGE', 'GREEN', 'YELLOW')
		AND ClinicalSlot.AMPM = 0 
		AND ClinicalSlotExaminers.ExaminerID
		NOT IN (SELECT DISTINCT
				ClinicalSlotExaminers.ExaminerID
				FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
				INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
				INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
				WHERE ClinicalDates.ExamID = @l_cts_exam_id
				AND ClinicalSlotExaminers.ExaminerID NOT IN (9555,14622)
				AND ClinicalSlot.colour IN ('BLUE', 'ORANGE', 'GREEN', 'YELLOW')
				AND ClinicalSlot.AMPM = 1 
		 )
		 AND ClinicalSlotExaminers.ExaminerID
		 NOT IN 
			  (SELECT DISTINCT 
			   ClinicalObservers.ExaminerID
			   FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			   INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
			   WHERE ClinicalDates.ExamID =  @l_cts_exam_id
			   AND ClinicalVenues.AM = 'Yes'
		  )
	   END
	 ELSE
   		 BEGIN
		   SELECT DISTINCT
			ClinicalDates.ExamID
		  , ClinicalSlotExaminers.ExaminerID
		  , ClinicalSlot.Colour
		  , ClinicalSlot.AMPM
		  , ClinicalSlotExaminers.SortOrder
		  , ClinicalSlot.GroupText
		  , ClinicalVenues.VenueID
		  , "Exams"."Session"
		  , ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
		  , "Venues"."HospitalName"
		  , CASE WHEN 
			ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
			ELSE
			 'AFTERNOON SESSION'
			END "AMPMSession" 
		  , CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
		  , ClinicalSlot.Colour+' GROUP '+ClinicalSlot.GroupText "TitleSubPart"
		  FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		  INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
		  INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		  INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotExaminers.ExaminerID
		  INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
		  INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID
		  WHERE ClinicalDates.ExamID = @l_cts_exam_id
		  AND ClinicalSlotExaminers.ExaminerID NOT IN (9555,14622)
		  AND ClinicalSlot.colour IN ('PINK', 'RED', 'GREY', 'LIME')
		  AND ClinicalSlot.AMPM = 1 
		  UNION
		 SELECT DISTINCT
		  ClinicalDates.ExamID
		, ClinicalSlotExaminers.ExaminerID
		, ClinicalSlot.Colour
		, ClinicalSlot.AMPM
		, ClinicalSlotExaminers.SortOrder
		, ClinicalSlot.GroupText
		, ClinicalVenues.VenueID
		, "Exams"."Session"
		, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
		, "Venues"."HospitalName"
		, CASE WHEN 
		ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
		ELSE
			'AFTERNOON SESSION'
		END "AMPMSession" 
		, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
		, ClinicalSlot.Colour+' GROUP '+ClinicalSlot.GroupText "TitleSubPart"
		FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
		INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotExaminers.ExaminerID
		INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
		INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID
		WHERE ClinicalDates.ExamID = @l_cts_exam_id
		AND ClinicalSlotExaminers.ExaminerID NOT IN (9555,14622)
		AND ClinicalSlot.colour IN ('PINK', 'RED', 'GREY', 'LIME')
		AND ClinicalSlot.AMPM = 0 
		AND ClinicalSlotExaminers.ExaminerID
		NOT IN (SELECT DISTINCT
				ClinicalSlotExaminers.ExaminerID
				FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
				INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
				INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
				WHERE ClinicalDates.ExamID = @l_cts_exam_id
				AND ClinicalSlotExaminers.ExaminerID NOT IN (9555,14622)
				AND ClinicalSlot.colour IN ('PINK', 'RED', 'GREY', 'LIME')
				AND ClinicalSlot.AMPM = 1 
		 )
		 AND ClinicalSlotExaminers.ExaminerID
		 NOT IN 
			  (SELECT DISTINCT 
			   ClinicalObservers.ExaminerID
			   FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			   INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
			   WHERE ClinicalDates.ExamID = @l_cts_exam_id
			   AND ClinicalVenues.AM = 'Yes'
		  )
	   END
    END
END
