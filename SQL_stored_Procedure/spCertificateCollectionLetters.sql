USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCertificateCollectionLetters]    Script Date: 18/11/2020 12:04:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCertificateCollectionLetters] @ExamId int
--, @CertAvailableDate nvarchar(30) = NULL
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/*IF ISDATE(@CertAvailableDate)= 1 
		BEGIN
			UPDATE Certs SET availabledate = @CertAvailableDate
			WHERE ID IN (
			  SELECT DISTINCT Certs.ID 
			  FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalVenues.ClinicalDateID = ClinicalDates.ID 
			  INNER JOIN ClinicalSlot ON ClinicalSlot.ClinicalVenueID = ClinicalVenues.ID 
			  INNER JOIN ClinicalSlotCandidates ON ClinicalSlot.ID = ClinicalSlotCandidates.ClinicalSlotID 
			  INNER JOIN Candidates ON Candidates.Fileno = ClinicalSlotCandidates.FileNo 
			  INNER JOIN Certs ON Candidates.Fileno = Certs.Fileno
			  WHERE ClinicalDates.ExamID = @ExamId  AND Certs.availabledate IS NULL AND Certs.Status = 'I'
			) 
		END*/
     SELECT 
	  "Candidates"."FamilyName"
	 , "Candidates"."GivenNames"
	 , "Candidates"."Fileno"
	 ,  SUBSTRING(candidates.Address,0,CASE WHEN CHARINDEX(CHAR(13),Candidates.Address) > 0 THEN CHARINDEX(CHAR(13),Candidates.Address) ELSE LEN(Address)+1 END) "Address"
	 , "Candidates".City+' '+ "Candidates"."State"+' '+"Candidates"."PostCode" "City"
	 , "Certs"."Certno"
	 , CONVERT(VARCHAR(10),"Certs"."Issuedate",103) AS "IssueDate"
	 , "Certs"."Issuedthrough"
	 , DATENAME(MM, "Certs"."availabledate") + RIGHT(CONVERT(VARCHAR(12), "Certs"."availabledate", 107), 9) AS "Available_date" 
	 , CAST(DAY(DATEADD(month,6,"availabledate")) AS VARCHAR(2)) + ' ' + DATENAME(MM, DATEADD(month,6,"availabledate")) + ' ' + CAST(YEAR(DATEADD(month,6,"availabledate")) AS VARCHAR(4)) AS "AvailableDAte+6months"
	 , "Images"."Image" 
	 , "iMedCountries".Description
	 , CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MM, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS "TodaysDate"
	, CASE WHEN CHARINDEX(' ',"GivenNames") > 0
	 THEN 'Dr '+LEFT(ISNULL("Candidates"."GivenNames",''),1)+ ' '+ SUBSTRING(ISNULL("GivenNames",''),CHARINDEX(' ',"GivenNames")+1,1)+' '+ISNULL("Candidates"."FamilyName",'') 
	 ELSE
	 'Dr '+LEFT(ISNULL("Candidates"."GivenNames",''),1)+ ' '+ ISNULL("Candidates"."FamilyName",'')
     END "Name"
	 FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalVenues.ClinicalDateID = ClinicalDates.ID 
     INNER JOIN ClinicalSlot ON ClinicalSlot.ClinicalVenueID = ClinicalVenues.ID 
     INNER JOIN ClinicalSlotCandidates ON ClinicalSlot.ID = ClinicalSlotCandidates.ClinicalSlotID 
     INNER JOIN Candidates ON Candidates.Fileno = ClinicalSlotCandidates.FileNo 
	 INNER JOIN Certs ON Candidates.Fileno = Certs.Fileno
	 LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
     LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
     WHERE ClinicalDates.ExamID = @ExamId  
	 --AND Certs.Status IN ('I','SAFE')   
END
