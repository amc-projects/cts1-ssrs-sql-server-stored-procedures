USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spFilesMarkedToLocation]    Script Date: 18/11/2020 2:34:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFilesMarkedToLocation] @FileNoList varchar(2000)
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		[FileNo] [varchar](255) NULL
	)

	DECLARE @FileNo varchar(255), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS varchar)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	
	SELECT "Locations"."Description"
    , "Candidates"."Fileno"
    , "Candidates"."FamilyName"
    , "Candidates"."GivenNames"
    , CONVERT(varchar,"Candidates"."MoveDate",103) "MoveDate"
    FROM   "CTS"."dbo"."Locations" "Locations"
    INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "Locations"."ID"="Candidates"."Location"
	INNER JOIN #TempList t ON t.FileNo = "Locations"."Description" COLLATE  Latin1_General_CI_AS

END
