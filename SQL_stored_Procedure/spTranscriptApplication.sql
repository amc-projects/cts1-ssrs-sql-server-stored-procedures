USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTranscriptApplication]    Script Date: 18/11/2020 3:35:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTranscriptApplication] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

	 SELECT "Candidates"."Fileno"
 , "Candidates"."FamilyName"
 , "Candidates"."GivenNames"
 , UPPER('Dr '+"Candidates".GivenNames+' '+"Candidates"."FamilyName" ) "Name"
 , "Candidates"."Sex"
 , CONVERT(varchar,"Candidates"."DateOfBirth",103) "DateOfBirth"
 , "Candidates"."CtyBirth"
 , "Candidates"."PhoneHome"
 , "Candidates"."PhoneWork"
 , ISNULL("Candidates"."Address",'')+' '+ISNULL("Candidates"."City",'')+' '+ISNULL("Candidates"."State",'')+' '+ISNULL("Candidates"."PostCode",'') "Address"
 , "iMedCountries"."Description"
 FROM   "CTS"."dbo"."Candidates" "Candidates"
LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno"
END
