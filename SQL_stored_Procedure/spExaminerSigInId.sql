USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerSigInId]    Script Date: 18/11/2020 12:29:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerSigInId] @ExamID INT
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   WITH q AS ( 
    -- Insert statements for procedure here
		SELECT DISTINCT ClinicalDates.ExamID
		, CONVERT(varchar(20),ClinicalSlotExaminers.SortOrder) "SortOrder"
		, ClinicalSlotExaminers.ExaminerID, ClinicalSlot.Colour + ' GROUP' "Colour"
		, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'Morning' WHEN ClinicalSlot.AMPM = 0 THEN 'Afternoon' END AS "Slot"
		, ISNULL(Addr_Personal.Title,'')+' '+ISNULL(Addr_Personal.Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name"
		, 'Examining' "Type"
		, addr_personal.Surname "Surname"
        , addr_personal.Givennames "Givennames"
		FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
		INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
		INNER JOIN Address.dbo.Addr_Personal ON Addr_Personal.Id = ClinicalSlotExaminers.ExaminerID
		WHERE ClinicalDates.ExamID = @ExamID And ClinicalSlotExaminers.ExaminerID != 9555

		UNION

		SELECT DISTINCT Exams.ID "ExamID"
		, '' "SortOrder"
		, ClinicalObservers.ExaminerID
		, ClinicalSlot.Colour+ ' GROUP' "Colour"
		, CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'Morning' WHEN ClinicalVenues.PM = 'Yes' THEN 'Afternoon' END AS "Slot"
		, ISNULL(Addr_Personal.Title,'')+' '+ISNULL(Addr_Personal.Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name"
		, 'Observing' "Type"
		, addr_personal.Surname "Surname"
        , addr_personal.Givennames "Givennames"
		FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID
		INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
		INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
		INNER JOIN Address.dbo.Addr_Personal ON ClinicalObservers.ExaminerID = addr_personal.Id
		INNER JOIN ClinicalObserverRotations ON ClinicalObservers.ID = ClinicalObserverRotations.ClinicalObserverID
		INNER JOIN ClinicalSlot ON ClinicalObserverRotations.SetNo = ClinicalSlot.ID
		WHERE Exams.ID = @ExamID
     )
	 SELECT "ExamID"
	 , SortOrder
	 , ExaminerID
	 , "Colour"
	 , "Slot"
	 , "Name"
	 , "Type"
	 , "Surname"
     , "Givennames"
	 ,  ROW_NUMBER() OVER(ORDER BY 	"q"."Surname", "q"."Givennames")%2  AS Row#
	 FROM q

END
