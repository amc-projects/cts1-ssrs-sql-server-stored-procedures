USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spSpecialistLogTranscript]    Script Date: 18/11/2020 2:58:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSpecialistLogTranscript] @FileNoList varchar(2000)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	


    -- Insert statements for procedure here
	SELECT DISTINCT 
	 "Candidates"."Fileno"
	, 'DR.'+ISNULL("Candidates"."GivenNames",'')+' '+ISNULL("Candidates"."FamilyName",'') "Name"
 FROM   ((((("CTS"."dbo"."Candidates" "Candidates"
 INNER JOIN "CTS"."dbo"."SpecialistEval" "SpecialistEval" ON "Candidates"."Fileno"="SpecialistEval"."FileNo")
 INNER JOIN "CTS"."dbo"."SpecialistLog" "SpecialistLog" ON "SpecialistEval"."SpecEvalID"="SpecialistLog"."SpecEvalID")
 INNER JOIN "CTS"."dbo"."SpecialistCollege" "SpecialistCollege" ON "SpecialistEval"."SpecCollCode"="SpecialistCollege"."SpecCollCode")
 INNER JOIN "CTS"."dbo"."Specialty" "Specialty" ON ("SpecialistEval"."SpecCollCode"="Specialty"."SpecCollCode") AND ("SpecialistEval"."SpecCode"="Specialty"."SpecCode"))
 INNER JOIN "CTS"."dbo"."SpecLogReasons" "SpecLogReasons" ON "SpecialistLog"."Description"="SpecLogReasons"."Description")
 LEFT OUTER JOIN "CTS"."dbo"."SpecialistQual" "SpecialistQual" ON "SpecialistLog"."SpecQualID"="SpecialistQual"."SpecQualID"
 INNER JOIN #TempList t ON t.FileNo = "Candidates"."Fileno" 
-- ORDER BY "Candidates"."Fileno", "SpecialistCollege"."CollegeName", "SpecialistQual"."SpecQualType", "SpecialistLog"."Date"

END
