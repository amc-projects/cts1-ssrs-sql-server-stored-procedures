USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateListing]    Script Date: 29/11/2021 3:39:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateListing] @ExamID int, @Grade nvarchar(5)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ExamTypeId int;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;

	---This is CTS Exam Type in Face to Face
	SELECT  @ExamTypeId = ExamTypeID
	FROM dbo.Exams WHERE id = @l_cts_exam_id
	
	--This returns Online or Face to Face
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	BEGIN
	  	WITH q AS (
		-- Insert statements for procedure here
		 SELECT "Candidates"."Fileno"
		 , UPPER("Candidates"."FamilyName")+', '+"Candidates"."GivenNames" "Names"
		 , CASE WHEN "Eligibility"."Englishseen" = 0 THEN 'L' ELSE '' END "oet"
		 , CASE WHEN "Eligibility"."WHO"= 0 THEN 'W' ELSE '' END "WHO"
		 , CASE WHEN "Eligibility"."Diploma"= 0 THEN 'D' ELSE '' END "Diploma"
		 , CASE WHEN "Eligibility"."ID"= 0 THEN 'Y' ELSE '' END "ID"
		 , CASE WHEN "Eligibility"."Forms"= 0 THEN 'F' ELSE '' END "Forms"
		 , CASE WHEN "Eligibility"."Name"= 0 THEN 'N' ELSE '' END "qName"
		 , CASE WHEN "Eligibility"."Photo" = 0 THEN 'P' ELSE '' END "qPhoto"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Speccogs" = 0 THEN 'S' ELSE '' END "cogs"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Speccv" = 0 THEN 'H' ELSE '' END "cv"
		 ,  CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Specdip" = 0 THEN 'B' ELSE '' END "specdip"
		 , CASE WHEN "Eligibility"."Ineligible" = 1 THEN 'I' ELSE '' END "inelig"
		 , CASE WHEN "Eligibility"."Scanned" IS NULL THEN 'T' ELSE '' END "qscanned" 
		 , "Candidates"."TypeOfReg"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Specforms" IS NULL THEN 'M' ELSE '' END "specforms"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Specphoto" IS NULL THEN 'O' ELSE '' END "specphoto"
		 , clinical_exams.name "Session"
		 , CONVERT(varchar,"Candidates"."DateOfBirth",103) "DOB"
		 , "Results_ClinicalSerialize"."Passfail"
		 , "Candidates"."Warning"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station1" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_1" = 0 THEN 1 ELSE 0 END "1"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station2" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_2" = 0 THEN 1 ELSE 0 END "2"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station3" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_4" = 0 THEN 1 ELSE 0 END "4"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station4" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_5" = 0 THEN 1 ELSE 0 END "5"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station5" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_6" = 0 THEN 1 ELSE 0 END "6"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station6" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_7" = 0 THEN 1 ELSE 0 END "7"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station7" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_9" = 0 THEN 1 ELSE 0 END "9"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station8" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_10" = 0 THEN 1 ELSE 0 END "10"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station9" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_11" = 0 THEN 1 ELSE 0 END "11"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station10" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_12" = 0 THEN 1 ELSE 0 END "12"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station11" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_14" = 0 THEN 1 ELSE 0 END "14"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station12" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_15" = 0 THEN 1 ELSE 0 END "15"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station13" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_16" = 0 THEN 1 ELSE 0 END "16"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station14" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_17" = 0 THEN 1 ELSE 0 END "17"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station15" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_19" = 0 THEN 1 ELSE 0 END "19"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station16" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_20" = 0 THEN 1 ELSE 0 END "20"
		 , "Candidates"."FamilyName"
		 , "Candidates"."GivenNames"	 
        FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON exam_assignments.candidate_id="Candidates"."Fileno" 
        INNER JOIN "CTS"."dbo"."Results_ClinicalSerialize" "Results_ClinicalSerialize" ON clinical_exams.cts_exam_id ="Results_ClinicalSerialize"."ExamID"
        AND  "Candidates"."Fileno" = "Results_ClinicalSerialize".Fileno
		INNER JOIN "CTS"."dbo"."Eligibility" "Eligibility" ON "Results_ClinicalSerialize"."FileNo"="Eligibility"."Fileno"
		WHERE clinical_exams.id  = @ExamID
		AND  "Results_ClinicalSerialize"."Passfail" = @Grade
     )
	 SELECT *, 
	 CASE WHEN q.TypeOfReg = 'G' THEN WHO + Diploma + ID + Forms + qName + oet + qPhoto + qscanned + cogs + cv + inelig
	 ELSE WHO + Diploma + ID + Forms + qName + oet + qPhoto + qscanned + cogs + cv + specdip + inelig + specforms + specphoto END "EligibilityCodes"
	  FROM q
	END
	ELSE
	BEGIN
	WITH q AS (
		-- Insert statements for procedure here
		 SELECT "Candidates"."Fileno"
		 , UPPER("Candidates"."FamilyName")+', '+"Candidates"."GivenNames" "Names"
		 , CASE WHEN "Eligibility"."Englishseen" = 0 THEN 'L' ELSE '' END "oet"
		 , CASE WHEN "Eligibility"."WHO"= 0 THEN 'W' ELSE '' END "WHO"
		 , CASE WHEN "Eligibility"."Diploma"= 0 THEN 'D' ELSE '' END "Diploma"
		 , CASE WHEN "Eligibility"."ID"= 0 THEN 'Y' ELSE '' END "ID"
		 , CASE WHEN "Eligibility"."Forms"= 0 THEN 'F' ELSE '' END "Forms"
		 , CASE WHEN "Eligibility"."Name"= 0 THEN 'N' ELSE '' END "qName"
		 , CASE WHEN "Eligibility"."Photo" = 0 THEN 'P' ELSE '' END "qPhoto"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Speccogs" = 0 THEN 'S' ELSE '' END "cogs"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Speccv" = 0 THEN 'H' ELSE '' END "cv"
		 ,  CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Specdip" = 0 THEN 'B' ELSE '' END "specdip"
		 , CASE WHEN "Eligibility"."Ineligible" = 1 THEN 'I' ELSE '' END "inelig"
		 , CASE WHEN "Eligibility"."Scanned" IS NULL THEN 'T' ELSE '' END "qscanned" 
		 , "Candidates"."TypeOfReg"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Specforms" IS NULL THEN 'M' ELSE '' END "specforms"
		 , CASE WHEN  ("Candidates"."TypeOfReg" = 'S' OR  "Candidates"."TypeOfReg"='D') AND "Eligibility"."Specphoto" IS NULL THEN 'O' ELSE '' END "specphoto"
		 , "Exams"."Session"
		 , CONVERT(varchar,"Candidates"."DateOfBirth",103) "DOB"
		 , "Results_ClinicalSerialize"."Passfail"
		 , "Candidates"."Warning"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station1" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_1" = 0 THEN 1 ELSE 0 END "1"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station2" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_2" = 0 THEN 1 ELSE 0 END "2"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station3" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_4" = 0 THEN 1 ELSE 0 END "4"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station4" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_5" = 0 THEN 1 ELSE 0 END "5"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station5" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_6" = 0 THEN 1 ELSE 0 END "6"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station6" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_7" = 0 THEN 1 ELSE 0 END "7"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station7" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_9" = 0 THEN 1 ELSE 0 END "9"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station8" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_10" = 0 THEN 1 ELSE 0 END "10"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station9" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_11" = 0 THEN 1 ELSE 0 END "11"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station10" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_12" = 0 THEN 1 ELSE 0 END "12"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station11" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_14" = 0 THEN 1 ELSE 0 END "14"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station12" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_15" = 0 THEN 1 ELSE 0 END "15"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station13" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_16" = 0 THEN 1 ELSE 0 END "16"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station14" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_17" = 0 THEN 1 ELSE 0 END "17"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station15" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_19" = 0 THEN 1 ELSE 0 END "19"
		 ,  CASE WHEN "Results_ClinicalSerialize"."Station16" = 'P' AND "Results_ClinicalSerialize"."clinicalmarksheetcandidatescenario_pilot_20" = 0 THEN 1 ELSE 0 END "20"
		 , "Candidates"."FamilyName"
		 , "Candidates"."GivenNames"
		 FROM   (("CTS"."dbo"."Exams" "Exams" INNER JOIN "CTS"."dbo"."Results_ClinicalSerialize" "Results_ClinicalSerialize" ON "Exams"."ID"="Results_ClinicalSerialize"."ExamID")
		 INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "Results_ClinicalSerialize"."FileNo"="Candidates"."Fileno")
		 INNER JOIN "CTS"."dbo"."Eligibility" "Eligibility" ON "Results_ClinicalSerialize"."FileNo"="Eligibility"."Fileno"
		 WHERE "Exams".ID = @l_cts_exam_id
		 AND  "Results_ClinicalSerialize"."Passfail" = @Grade
     )
	 SELECT *, 
	 CASE WHEN q.TypeOfReg = 'G' THEN WHO + Diploma + ID + Forms + qName + oet + qPhoto + qscanned + cogs + cv + inelig
	 ELSE WHO + Diploma + ID + Forms + qName + oet + qPhoto + qscanned + cogs + cv + specdip + inelig + specforms + specphoto END "EligibilityCodes"
	  FROM q
     END
END
