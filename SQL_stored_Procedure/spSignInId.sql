USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spSignInId]    Script Date: 13/01/2022 11:20:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSignInId] @ExamId int, @SignInType varchar(20)
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	
	IF @l_exam_type_id = 1
	 BEGIN
		 IF @SignInType = 'Examiner' 
	      BEGIN
			SELECT 
				 clinical_exams.cts_exam_id "ExamID"
			   , exam_part_stations.number SortOrder
			   , exam_assignments.examiner_id ExaminerID
			   , '' "ColourGroup"
			   , CASE 
				 WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'Morning'
				 ELSE 'Afternoon'
				 END "Slot"
			   , ISNULL("Addr_Personal".Title,'')+' '+ISNULL("Addr_Personal".Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name" 
			   , 'Examining' "Type"
			   , "Addr_Personal".Surname "Surname"
			   , "Addr_Personal".Givennames "Givennames"
			   , '' "Color"
			   ,  ROW_NUMBER() OVER(ORDER BY addr_personal.Surname , addr_personal.Givennames)%2  AS Row#
			FROM AMC.cts.clinical_exams
			INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
			INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
			INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
			INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
			INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
			INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
			AND exam_assignments.exam_part_session_id = exam_part_sessions.id
			INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.examiner_id
				INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
				INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
				WHERE   "addr_addresses"."MailAddr"='Y'
			 AND clinical_exams.id = @ExamId
			 AND examiner_id IS NOT NULL

			 UNION

			 SELECT
                 clinical_exams.cts_exam_id "ExamID"
			   , '' SortOrder
			   , amc.cts.spare_stakeholders.contact_id ExaminerID
			   , '' "ColourGroup"
			   , CASE 
				 WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'Morning'
				 ELSE 'Afternoon'
				 END "Slot"
			   , ISNULL("Addr_Personal".Title,'')+' '+ISNULL("Addr_Personal".Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name" 
			   , 'Observing' "Type"
			   , "Addr_Personal".Surname "Surname"
			   , "Addr_Personal".Givennames "Givennames"
			   , '' "Color"
			   ,  ROW_NUMBER() OVER(ORDER BY addr_personal.Surname , addr_personal.Givennames)%2  AS Row#
			FROM AMC.cts.clinical_exams
			INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
			INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
			INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
			--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
			INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
			--AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
			--INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
			INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
			INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
			INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
			WHERE   "addr_addresses"."MailAddr"='Y'
			AND  clinical_exams.id = @ExamId
     		AND role IN ('QA Examiner')

		 END
		 ELSE IF  @SignInType = 'RolePlayer'
		 BEGIN
		    SELECT 
             clinical_exams.cts_exam_id "ExamID"
	       , exam_part_stations.number SortOrder
	       , exam_assignments.simulated_patient_id ExaminerID
	       , '' "ColourGroup"
	       , CASE 
		     WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'Morning'
		     ELSE 'Afternoon'
		     END "Slot"
	       , ISNULL("Addr_Personal".Title,'')+' '+ISNULL("Addr_Personal".Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name" 
	       , 'Role-playing' "Type"
	       , "Addr_Personal".Surname "Surname"
	       , "Addr_Personal".Givennames "Givennames"
	       , '' "Color"
	       ,  ROW_NUMBER() OVER(ORDER BY addr_personal.Surname , addr_personal.Givennames)%2  AS Row#
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		--AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.simulated_patient_id
	        INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	        INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	        WHERE   "addr_addresses"."MailAddr"='Y'
		 AND clinical_exams.id = @ExamId
	   END
     END
	 ELSE
		IF @SignInType = 'Examiner'
		BEGIN
			 WITH q AS ( 
		-- Insert statements for procedure here
				SELECT DISTINCT ClinicalDates.ExamID
				, CONVERT(varchar(20),ClinicalSlotExaminers.SortOrder) "SortOrder"
				, ClinicalSlotExaminers.ExaminerID, ClinicalSlot.Colour + ' GROUP' "ColourGroup"
				, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'Morning' WHEN ClinicalSlot.AMPM = 0 THEN 'Afternoon' END AS "Slot"
				, ISNULL(Addr_Personal.Title,'')+' '+ISNULL(Addr_Personal.Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name"
				, 'Examining' "Type"
				, addr_personal.Surname "Surname"
				, addr_personal.Givennames "Givennames"
				, ClinicalSlot.Colour "Color"
				FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
				INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
				INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
				INNER JOIN Address.dbo.Addr_Personal ON Addr_Personal.Id = ClinicalSlotExaminers.ExaminerID
				WHERE ClinicalDates.ExamID = @l_cts_exam_id And ClinicalSlotExaminers.ExaminerID != 9555

				UNION

				SELECT DISTINCT Exams.ID "ExamID"
				, '' "SortOrder"
				, ClinicalObservers.ExaminerID
				, ClinicalSlot.Colour+ ' GROUP' "ColourGroup"
				, CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'Morning' WHEN ClinicalVenues.PM = 'Yes' THEN 'Afternoon' END AS "Slot"
				, ISNULL(Addr_Personal.Title,'')+' '+ISNULL(Addr_Personal.Initials,'')+' '+ISNULL(Addr_Personal.Surname,'') "Name"
				, 'Observing' "Type"
				, addr_personal.Surname "Surname"
				, addr_personal.Givennames "Givennames"
				, ClinicalSlot.Colour "Color"
				FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID
				INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
				INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
				INNER JOIN Address.dbo.Addr_Personal ON ClinicalObservers.ExaminerID = addr_personal.Id
				INNER JOIN ClinicalObserverRotations ON ClinicalObservers.ID = ClinicalObserverRotations.ClinicalObserverID
				INNER JOIN ClinicalSlot ON ClinicalObserverRotations.SetNo = ClinicalSlot.ID
				WHERE Exams.ID = @l_cts_exam_id
			 )
			 SELECT "ExamID"
			 , SortOrder
			 , ExaminerID
			 , "ColourGroup"
			 , "Slot"
			 , "Name"
			 , "Type"
			 , "Surname"
			 , "Givennames"
			 ,  ROW_NUMBER() OVER(ORDER BY 	"q"."Surname", "q"."Givennames")%2  AS Row#
			 , "Color" 
			 FROM q
		END
	
		ELSE IF  @SignInType = 'RolePlayer'
		BEGIN
			SELECT ClinicalDates.ExamID
			, ClinicalSlotRolePlayers.SortOrder
			, ClinicalSlotRolePlayers.ExaminerID
			, ClinicalSlot.Colour+' '+'GROUP' "ColourGroup"
			, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'Morning' WHEN ClinicalSlot.AMPM = 0 THEN 'Afternoon' END AS "Slot"
			, Addr_Personal.Title+' '+Addr_Personal.Givennames+' '+Addr_Personal.Surname "Name"
			--, ClinicalSlot.AMPM
			, 'Role-playing' "Type"
			, addr_personal.Surname "Surname"
			, addr_personal.Givennames "Givennames"
			,  ROW_NUMBER() OVER(ORDER BY 	addr_personal."Surname", addr_personal."Givennames")%2  AS Row#
			, ClinicalSlot.Colour "Color"
			 FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			 INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			 INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			 INNER JOIN Address.dbo.Addr_Personal ON Addr_Personal.Id = ClinicalSlotRolePlayers.ExaminerID
			WHERE ClinicalDates.ExamID = @l_cts_exam_id AND ClinicalSlotRolePlayers.ExaminerID NOT IN (9555,14622)
		END
		ELSE IF @SignInType = 'Marshall'
		  BEGIN
			SELECT ClinicalDates.ExamID
			, ClinicalSlotMarshalls.SortOrder
			, ClinicalSlotMarshalls.MarshallID as ExaminerID 
			, CASE 
				WHEN Addr_ML.MasterId IS NULL THEN ClinicalSlot.Colour
				ELSE ''
			END "ColourGroup"
			, CASE 
				WHEN ClinicalSlot.AMPM = 1 AND Addr_ML.MasterId IS NULL THEN 'Morning'
				WHEN ClinicalSlot.AMPM = 0 AND Addr_ML.MasterId IS NULL THEN 'Afternoon'
			END AS "Slot"
			, Addr_Personal.Title+' '+Addr_Personal.Givennames+' '+Addr_Personal.Surname "Name"
			, CASE 
				WHEN Addr_ML.MasterId IS NULL THEN 'Examination Marshall' 
				ELSE 'AMC Staff Member'
			END   "Type"
			, addr_personal.Surname "Surname"
			, addr_personal.Givennames "Givennames"
			, CASE 
				WHEN Addr_ML.MasterId IS NULL THEN ClinicalSlot.Colour
				ELSE ''
			END "Color"
		,  ROW_NUMBER() OVER(ORDER BY 	addr_personal."Surname", addr_personal."Givennames")%2  AS Row#
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID 
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotMarshalls ON ClinicalSlot.ID = ClinicalSlotMarshalls.ClinicalSlotID
			INNER JOIN Address.dbo.Addr_Personal ON Addr_Personal.Id = ClinicalSlotMarshalls.MarshallID
			LEFT OUTER JOIN Address.dbo.Addr_Ml ON MasterID = ClinicalSlotMarshalls.MarshallID AND MlCatID = 107 And Previous IS NULL
			WHERE ClinicalDates.ExamID = @l_cts_exam_id And ClinicalSlotMarshalls.MarshallID NOT IN (9555,14622)
		  END
END
