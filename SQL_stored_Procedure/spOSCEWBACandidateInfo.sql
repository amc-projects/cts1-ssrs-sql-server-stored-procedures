USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spOSCEWBACandidateInfo]    Script Date: 18/11/2020 2:42:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spOSCEWBACandidateInfo] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Candidates.GivenNames+' '+Candidates.FamilyName "Name"
	,	Candidates.FileNo
	,	Candidates.Sex Gender
	,	CONVERT(VARCHAR(11), Candidates.DateOfBirth, 106) "DateOfBirth"
	,CASE WHEN 
	ISNULL(iMedCountriesBirthCtyCode.[Description],
	ISNULL(iMedCountriesCtyBirth.[Description], 
	ISNULL(Candidates.CtyBirth, 'UNKNOWN')))
	= '^UNKNOWN^' THEN 'UNKNOWN'
	ELSE 
	ISNULL(iMedCountriesBirthCtyCode.[Description],
	ISNULL(iMedCountriesCtyBirth.[Description], 
	ISNULL(Candidates.CtyBirth, 'UNKNOWN')))
	END
	CountryOfBirth,
	CASE WHEN 
	ISNULL(iMedCountriesQualTrainCtyCode.Description,
	ISNULL(iMedCountriesQualCtyTrain.Description,
	ISNULL(Qualification.CtyTrained, 'UNKNOWN')))
	= '^UNKNOWN^' THEN 'UNKNOWN'
	ELSE 
	ISNULL(iMedCountriesQualTrainCtyCode.Description,
	ISNULL(iMedCountriesQualCtyTrain.Description,
	ISNULL(Qualification.CtyTrained, 'UNKNOWN')))
	END
	CountryOfTraining
	, Qualification.iMedQualName "Primary Qualification"
	, CONVERT(VARCHAR(11),Qualification.DegreeDate,106) "Primary qualification date"
	, iMedSchools.Name "Primary Qualification Institution" 
	,Candidates.Address+' '+Candidates.City+' '+Candidates.State+' '+Candidates.PostCode "Address location"
	, CoutryOfResidence.Description "Country Of Residence"
	, CONVERT(VARCHAR(11), Candidates.Created_at, 106) "Date of Creation"
	, CONVERT(VARCHAR(11), Candidates.Updated_at, 106) "Date of update"
	, LastExams.MCQTotal MCQAttemptCount
	, MCQPassExam.ExamDate AS MCQPassExamDate
	, Results_MCQ.AMCAll MCQ_Result_Score
	, Results_MCQ.Perc_Score "Score(%)"
	, Results_MCQ.PassFail MCQ_Result
	, LastExams.ClinTotal ClinAttemptCount
	, ClinMaxExam.Examdate ClinLastExamDate
	, CASE WHEN Results_Clinical_Exam.Passfail IS NOT NULL THEN Results_Clinical_Exam.Passfail ELSE Results_Clinical_Retest.Passfail END ClinPassFail,
	wba_placements.WBAAttemptCount,
	wba_placements.WBALastStartDate,
	wba_placements.WBALastEndDate,
	wba_placements.WBAResult
	, wba_placements.name "WBA Assessment Provider"
	FROM LastExams 
	INNER JOIN Candidates ON LastExams.FileNo = Candidates.Fileno
	INNER JOIN Exams MCQPassExam ON LastExams.MCQExamIDPass = MCQPassExam.ID
	INNER JOIN Results_MCQ ON Results_MCQ.ExamID = LastExams.MCQExamIDPass AND Results_MCQ.FileNo = Candidates.Fileno
	LEFT JOIN Exams ClinPassExam on LastExams.ClinExamIDPass = ClinPassExam.ID
	LEFT JOIN Exams ClinMaxExam ON LastExams.ClinExamIDMax = ClinMaxExam.ID
	LEFT JOIN 
	(
	SELECT Results_Clinical_Exam.ResultID, Results_Clinical.FileNo, Results_Clinical.ExamID, Results_Clinical_Exam.Passfail
	FROM Results_Clinical INNER JOIN Results_Clinical_Exam ON Results_Clinical_Exam.ResultID = Results_Clinical.ID
	) Results_Clinical_Exam ON Results_Clinical_Exam.ExamID = ClinMaxExam.ID AND Results_Clinical_Exam.FileNo = Candidates.Fileno 
	LEFT JOIN 
	(
	SELECT Results_Clinical_Retest.ResultID, Results_Clinical.FileNo, Results_Clinical_Retest.RetestExamID ExamID, Results_Clinical_Retest.Passfail
	FROM Results_Clinical INNER JOIN Results_Clinical_Retest ON Results_Clinical_Retest.ResultID = Results_Clinical.ID
	) Results_Clinical_Retest ON Results_Clinical_Retest.ExamID = ClinMaxExam.ID AND Results_Clinical_Retest.FileNo = Candidates.Fileno 
	LEFT JOIN Qualification ON Candidates.Fileno = Qualification.FileNo
	LEFT JOIN iMedCountries iMedCountriesQualTrainCtyCode ON Qualification.iMedCountryCode = iMedCountriesQualTrainCtyCode.Code
	LEFT JOIN iMedCountries iMedCountriesQualCtyTrain On Qualification.CtyTrained = iMedCountriesQualCtyTrain.[Description]
	LEFT JOIN iMedCountries iMedCountriesBirthCtyCode ON dbo.Candidates.iMedCountryCodeBirth = iMedCountriesBirthCtyCode.Code
	LEFT JOIN iMedCountries iMedCountriesCtyBirth ON Candidates.CtyBirth = iMedCountriesCtyBirth.[Description]
	LEFT JOIN iMedSchools ON iMedSchools.Code = Qualification.iMedSchoolCode 
	LEFT JOIN iMedCountries CoutryOfResidence ON CoutryOfResidence.Code = Candidates.iMedCountryCodeResidence 
	LEFT JOIN 
	(SELECT 
	wba_placementid_max.Fileno FileNo,
	wba_placementid_max.PlacementIdCount WBAAttemptCount, 
	wba_placements.start_date WBALastStartDate, 
	wba_placements.end_date WBALastEndDate, 
	CASE 
	WHEN wba_placements.status IS NULL THEN NULL
	WHEN wba_placements.status = 'complete' THEN
	CASE
	WHEN wba_placements.final_result = 1 THEN 'PASS'
	ELSE 'FAIL'
	END
	--WHEN wba_placements.status IN ('pending','pending_result') THEN 'PENDING'
	--WHEN wba_placements.status IN ('pending_authority_acceptance', 'pending_candidate_acceptance') THEN 'PENDING_ACCEPTANCE'
	--WHEN wba_placements.status IN ('withdrawn') THEN 'WITHDRAWN'
	ELSE wba_placements.status
	END WBAResult
	, "wba_authorities".name 
	FROM 
	(SELECT FileNo, MAX(PlacementId) PlacementIdMax, COUNT(PlacementId) PlacementIdCount
	FROM WBA
	GROUP BY Fileno
	) wba_placementid_max
	INNER JOIN amc.cts.wba_placements ON wba_placements.id = wba_placementid_max.PlacementIdMax
	INNER JOIN [AMC].[cts].[wba_authorities] "wba_authorities" ON "wba_placements".wba_authority_id = "wba_authorities".id
	)
	wba_placements 
	ON Candidates.Fileno = wba_placements.FileNo
	LEFT JOIN Certs ON Certs.Fileno = Candidates.Fileno
	WHERE 
	MCQExamIDPass IS NOT NULL
	--AND "Candidates".FileNo = 2172421
	AND Candidates.terminated_at IS NULL
	AND (CASE WHEN Results_Clinical_Exam.Passfail IS NOT NULL THEN Results_Clinical_Exam.Passfail ELSE Results_Clinical_Retest.Passfail END IS NOT NULL OR wba_placements.FileNo IS NOT NULL)
	AND Certs.deleted_at IS NULL 
	--AND Certs.Status = 'I'
	AND (YEAR("wba_placements".WBALastEndDate) = '2019' OR YEAR("wba_placements".WBALastStartDate) = '2019')
END
