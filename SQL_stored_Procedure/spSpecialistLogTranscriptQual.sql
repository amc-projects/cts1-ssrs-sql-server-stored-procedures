USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spSpecialistLogTranscriptQual]    Script Date: 18/11/2020 3:18:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSpecialistLogTranscriptQual] @FileNo int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT "SpecialistQual"."SpecQual", "SpecialistQual"."CtySpecTrained", "SpecialistQual"."SpecYear", "SpecialistQual"."SpecInst", "SpecialistQual"."SpecEvalID", "SpecialistQual"."SpecQualType"
    FROM  "CTS"."dbo"."Candidates" "Candidates"
    INNER JOIN "CTS"."dbo"."SpecialistEval" "SpecialistEval" ON "Candidates"."Fileno"="SpecialistEval"."FileNo"
    INNER JOIN "CTS"."dbo"."SpecialistQual" "SpecialistQual" ON "SpecialistQual"."SpecEvalID" = "SpecialistEval".SpecEvalID
    WHERE "Candidates".Fileno = @FileNo
END
