USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spLetterCertificatesAMCSafe]    Script Date: 18/11/2020 2:41:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLetterCertificatesAMCSafe] @ReturnDate NVARCHAR(30) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT "Candidates"."Fileno"
 , "Candidates"."GivenNames"
 , "Candidates"."FamilyName"
 , 'Dr '+"Candidates"."GivenNames"+' '+"Candidates"."FamilyName" "Name"
 , "Candidates"."State"+' '+"Candidates"."PostCode" "State"
 , "Certs"."Status"
 , "Eligibility"."Diploma"
 , "Eligibility"."Name" "EligibilityName"
 , "Eligibility"."ID"
 , "Eligibility"."Englishseen"
 , "Candidates"."Address"
 , "Candidates"."City"
 , "Candidates"."PostCode"
 , "iMedCountries"."Description"
 , DATENAME(dw,GETDATE())+', ' +CAST(DAY(GETDATE()) AS VARCHAR(20)) + ' ' + DATENAME(MM, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS "TodaysDate"
 , CAST(DAY('03/04/2017') AS VARCHAR(20)) + ' ' + DATENAME(MM, '03/04/2017') + ' ' + CAST(YEAR('03/04/2017') AS VARCHAR(4)) "ReturnDate"
 FROM   (("CTS"."dbo"."Candidates" "Candidates" INNER JOIN "CTS"."dbo"."Certs" "Certs" ON "Candidates"."Fileno"="Certs"."Fileno")
 INNER JOIN "CTS"."dbo"."Eligibility" "Eligibility" ON "Candidates"."Fileno"="Eligibility"."Fileno")
 LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
 WHERE  "Certs"."Status"='SAFE'
 --AND "Candidates".FileNo = @FileNo
 --ORDER BY "Candidates"."FamilyName", "Candidates"."GivenNames", "Candidates"."State"
END
