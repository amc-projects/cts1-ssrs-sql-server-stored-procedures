USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptMCQ]    Script Date: 18/11/2020 10:46:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptMCQ] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
   SELECT "Exams"."Session"
 , Exams.ExamTypeID
 , COnVERT(varchar,"Exams"."Examdate",103) "Examdate"
 , "Exams"."Examdate" "Exam_date"
 , "Results_MCQ"."FileNo"
 , "Exams"."Centre"
 , "Results_MCQ"."PassFail"
 , "Results_MCQ"."PlaceRank"
 --, "Results_MCQ"."Psy"
-- , "Results_MCQ"."Paed"
 , "Results_MCQ"."AMCMastery"
 , "Results_MCQ"."AMCAll"
 --, "Results_MCQ"."Sur"
 , "Results_MCQ"."Valid"
 , "ExamTypes"."AMCSection"
 , "ExamTypes"."sortorder"
 , "ExamTypes"."Description"
 , "Results_MCQ"."PaedAdj"
 , "Results_MCQ"."PsyAdj"
 , "Results_MCQ"."SurAdj"
 , "ExamTypes"."ID"
 , "Results_MCQ"."MedAdj"
 --, "Results_MCQ"."Med"
 , "Results_MCQ"."ObsAdj"
 --, "Results_MCQ"."Obs"
 , "Results_MCQ"."ScoreAdj"
 , "Results_MCQ"."Perc_Score"
 , CASE WHEN "Results_MCQ".PassFail NOT IN ('P','F','IF') THEN "Results_MCQ".PassFail ELSE 'F' END "Result"
 , CASE WHEN "Exams"."Session" = '2008 IMGS' OR  "Results_MCQ"."ScoreAdj" IS NULL THEN "Results_MCQ"."Perc_Score" ELSE  "Results_MCQ"."ScoreAdj" END "rank"
 , CASE WHEN "Results_MCQ".PassFail  IN ('IF') THEN '000' ELSE "Results_MCQ"."AMCAll" END "AMCSCore"
 , CASE WHEN "Results_MCQ"."MedAdj" IS NULL OR LTRIM(RTRIM("Results_MCQ"."MedAdj"))='' OR "Exams"."Session" = '2008 IMGS' THEN "Results_MCQ"."Med" ELSE "Results_MCQ"."MedAdj" END "Med"
 , CASE WHEN "Results_MCQ"."PaedAdj" IS NULL OR LTRIM(RTRIM("Results_MCQ"."PaedAdj"))='' OR "Exams"."Session" = '2008 IMGS' THEN "Results_MCQ"."Paed" ELSE "Results_MCQ"."PaedAdj" END "Paed"
 , CASE WHEN "Results_MCQ"."PsyAdj" IS NULL OR LTRIM(RTRIM("Results_MCQ"."PsyAdj"))='' OR "Exams"."Session" = '2008 IMGS' THEN "Results_MCQ"."Psy" ELSE "Results_MCQ"."PsyAdj" END "Psy"
 , CASE WHEN "Results_MCQ"."SurAdj" IS NULL OR LTRIM(RTRIM("Results_MCQ"."SurAdj"))='' OR "Exams"."Session" = '2008 IMGS' THEN "Results_MCQ"."Sur" ELSE "Results_MCQ"."SurAdj" END "Sur"
  , CASE WHEN "Results_MCQ"."ObsAdj" IS NULL OR LTRIM(RTRIM("Results_MCQ"."ObsAdj"))='' OR "Exams"."Session" = '2008 IMGS' THEN "Results_MCQ"."Obs" ELSE "Results_MCQ"."ObsAdj" END "Obs"
 FROM   ("CTS"."dbo"."Results_MCQ" "Results_MCQ" INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Results_MCQ"."ExamID"="Exams"."ID")
 INNER JOIN "CTS"."dbo"."ExamTypes" "ExamTypes" ON "Exams"."ExamTypeID"="ExamTypes"."ID"
 WHERE FileNo = @FileNo
 AND "ExamTypes"."AMCSection"=N'MCQ' AND "Results_MCQ"."Valid"=1
 --ORDER BY "ExamTypes"."sortorder", "Exams"."Examdate"

END
