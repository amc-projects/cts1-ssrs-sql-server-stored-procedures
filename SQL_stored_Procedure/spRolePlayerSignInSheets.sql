USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spRolePlayerSignInSheets]    Script Date: 08/11/2021 11:05:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRolePlayerSignInSheets] @ExamId as int
AS
DECLARE @ExamTypeId int;
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;

	---This is CTS Exam Type in Face to Face
	SELECT  @ExamTypeId = ExamTypeID
	FROM dbo.Exams WHERE id = @l_cts_exam_id
	
	--This returns Online or Face to Face
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	BEGIN
	  SELECT 
		  clinical_exams.id "ExamID"
		, exam_assignments.simulated_patient_id "ExaminerID"
		, 'BLUE' Colour
		, AMC.cts.exam_part_sessions.number "AMPM" 
		, exam_part_stations.number "SortOrder"
		, NULL "GroupText"
		, clinical_exams.venue_id "VenueID"
		, clinical_exams.name "Session"
		, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
		, UPPER(clinical_venues.name) "HospitalName" 
		, CASE 
			WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'MORNING SESSION'
			ELSE 'AFTERNOON SESSION'
			END "AMPMSession"
		, CONVERT(VARCHAR(10),  clinical_exams.date, 103)"ExamDate"
		, '' "TitleSubPart"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.simulated_patient_id
		INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
		INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
		WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId

		UNION

		 SELECT DISTINCT
		  clinical_exams.id "ExamID"
		,  spare_stakeholders.contact_id "ExaminerID"
		, 'BLUE' Colour
		, AMC.cts.exam_part_sessions.number "AMPM" 
		, NULL "SortOrder"
		, NULL "GroupText"
		, clinical_exams.venue_id "VenueID"
		, clinical_exams.name "Session"
		, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
		, UPPER(clinical_venues.name) "HospitalName" 
		, CASE 
			WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'MORNING SESSION'
			ELSE 'AFTERNOON SESSION'
			END "AMPMSession"
		, CONVERT(VARCHAR(10),  clinical_exams.date, 103)"ExamDate"
		, '' "TitleSubPart"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
		AND  spare_stakeholders.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = spare_stakeholders.contact_id
		INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
		INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
		WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId
		AND spare_stakeholders.role = 'Simulated Patient'

	END
	ELSE

	BEGIN

		IF @ExamTypeId = 2 
		--Main Exam
		 BEGIN

			-- Insert statements for procedure here
			SELECT ClinicalDates.ExamID
			, ClinicalSlot.Colour
			, ClinicalSlot.AMPM
			, ClinicalSlotRolePlayers.ExaminerID
			, ClinicalSlotRolePlayers.SortOrder
			, ClinicalSlot.GroupText
			, ClinicalVenues.VenueID
			, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
			, "Venues"."HospitalName"
			, CASE WHEN 
			ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
			ELSE
				'AFTERNOON SESSION'
			END "AMPMSession" 
			, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
			, "Exams"."Session"
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotRolePlayers.ExaminerID
			INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
			INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID 
			WHERE ClinicalDates.ExamID = @l_cts_exam_id
			And ClinicalSlotRolePlayers.ExaminerID NOT IN(9555,14622)
			And ClinicalSlot.Colour IN ('BLUE', 'ORANGE', 'GREEN', 'YELLOW')
			And ClinicalSlot.AMPM = 1 

			UNION

			SELECT ClinicalDates.ExamID
			, ClinicalSlot.Colour
			, ClinicalSlot.AMPM
			, ClinicalSlotRolePlayers.ExaminerID
			, ClinicalSlotRolePlayers.SortOrder
			, ClinicalSlot.GroupText
			, ClinicalVenues.VenueID
			, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
			, "Venues"."HospitalName"
			, CASE WHEN 
			ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
			ELSE
				'AFTERNOON SESSION'
			END "AMPMSession"
			, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
			, "Exams"."Session"
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotRolePlayers.ExaminerID
			INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
			INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID 
			WHERE ClinicalDates.ExamID = @l_cts_exam_id
			And ClinicalSlotRolePlayers.ExaminerID NOT IN (9555,14622)
			And ClinicalSlot.Colour IN ('BLUE', 'ORANGE', 'GREEN', 'YELLOW') 
			And ClinicalSlot.AMPM = 0 
			AND ClinicalSlotRolePlayers.ExaminerID NOT IN
			( SELECT
			 ClinicalSlotRolePlayers.ExaminerID
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			WHERE ClinicalDates.ExamID = @l_cts_exam_id
			And ClinicalSlotRolePlayers.ExaminerID NOT IN(9555,14622)
			And ClinicalSlot.Colour IN ('BLUE', 'ORANGE', 'GREEN', 'YELLOW')
			And ClinicalSlot.AMPM = 1 
			)
		END
		ELSE
		BEGIN

			-- Insert statements for procedure here
			SELECT ClinicalDates.ExamID
			, ClinicalSlot.Colour
			, ClinicalSlot.AMPM
			, ClinicalSlotRolePlayers.ExaminerID
			, ClinicalSlotRolePlayers.SortOrder
			, ClinicalSlot.GroupText
			, ClinicalVenues.VenueID
			, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
			, "Venues"."HospitalName"
			, CASE WHEN 
			ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION'
			ELSE
				'AFTERNOON SESSION'
			END "AMPMSession" 
			, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
			, "Exams"."Session"
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotRolePlayers.ExaminerID
			INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
			INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID 
			WHERE ClinicalDates.ExamID = @l_cts_exam_id
			And ClinicalSlotRolePlayers.ExaminerID NOT IN(9555,14622)
			And ClinicalSlot.Colour IN ('PINK', 'RED', 'GREY', 'LIME')
			And ClinicalSlot.AMPM = 1 

			UNION

			SELECT ClinicalDates.ExamID
			, ClinicalSlot.Colour
			, ClinicalSlot.AMPM
			, ClinicalSlotRolePlayers.ExaminerID
			, ClinicalSlotRolePlayers.SortOrder
			, ClinicalSlot.GroupText
			, ClinicalVenues.VenueID
			, ISNULL(Title,'')+' '+ISNULL(Givennames,'')+' '+ISNULL(Surname,'') "Name"
			, "Venues"."HospitalName"
			, CASE WHEN 
			ClinicalSlot.AMPM = 1 THEN 'MORNING SESSION' 
			ELSE
				'AFTERNOON SESSION'
			END "AMPMSession"
			, CONVERT(VARCHAR,"ClinicalDates"."ExamDate",103) "ExamDate"
			, "Exams"."Session"
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			INNER JOIN "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON "Addr_Personal".Id = ClinicalSlotRolePlayers.ExaminerID
			INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Exams".Id = ClinicalDates.ExamID
			INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "Venues".Id = ClinicalVenues.VenueID 
			WHERE ClinicalDates.ExamID = @l_cts_exam_id
			And ClinicalSlotRolePlayers.ExaminerID NOT IN (9555,14622)
			And ClinicalSlot.Colour IN ('PINK', 'RED', 'GREY', 'LIME')
			And ClinicalSlot.AMPM = 0 
			AND ClinicalSlotRolePlayers.ExaminerID NOT IN
			( SELECT
			 ClinicalSlotRolePlayers.ExaminerID
			FROM ClinicalDates INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			WHERE ClinicalDates.ExamID = @l_cts_exam_id
			And ClinicalSlotRolePlayers.ExaminerID NOT IN(9555,14622)
			And ClinicalSlot.Colour IN ('PINK', 'RED', 'GREY', 'LIME')
			And ClinicalSlot.AMPM = 1 
			)
		END
     END
END
