USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCredentialingCollectionLetters]    Script Date: 18/11/2020 12:19:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCredentialingCollectionLetters] @FileNoList varchar(2000)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

	SELECT  
	  "Candidates"."FamilyName"
	 , "Candidates"."Fileno"
	 ,  SUBSTRING(candidates.Address,0,CASE WHEN CHARINDEX(CHAR(13),Candidates.Address) > 0 THEN CHARINDEX(CHAR(13),Candidates.Address) ELSE LEN(Address)+1 END) "Address"
	 , "Candidates".City+' '+ "Candidates"."State"+' '+"Candidates"."PostCode" "City"
	 , "Certs"."Certno"
	 , CONVERT(VARCHAR(10),"Certs"."Issuedate",103) AS "IssueDate"
	 , "Certs"."Issuedthrough"
	 , DATENAME(MM, "Certs"."availabledate") + RIGHT(CONVERT(VARCHAR(12), "Certs"."availabledate", 107), 9) AS "Available_date" 
	 , CAST(DAY(DATEADD(month,6,"availabledate")) AS VARCHAR(2)) + ' ' + DATENAME(MM, DATEADD(month,6,"availabledate")) + ' ' + CAST(YEAR(DATEADD(month,6,"availabledate")) AS VARCHAR(4)) AS "AvailableDAte+6months"
	 , "Images"."Image" 
	 , "iMedCountries".Description
	 , CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MM, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS "TodaysDate"
	 , CASE WHEN CHARINDEX(' ',"GivenNames") > 0
		   THEN 'Dr '+LEFT(ISNULL("Candidates"."GivenNames",''),1)+ ' '+ SUBSTRING(ISNULL("GivenNames",''),CHARINDEX(' ',"GivenNames")+1,1)+' '+ISNULL("Candidates"."FamilyName",'') 
		   ELSE
		   'Dr '+LEFT(ISNULL("Candidates"."GivenNames",''),1)+ ' '+ ISNULL("Candidates"."FamilyName",'')
	  END "Name"
	 FROM   (("CTS"."dbo"."Candidates" "Candidates" INNER JOIN "CTS"."dbo"."Certs" "Certs" ON "Candidates"."Fileno"="Certs"."Fileno")
	 INNER JOIN vwCredentialCollection ON vwCredentialCollection.FileNo = Certs.FileNo
	 LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo")
	 LEFT OUTER JOIN "CTS"."dbo"."iMedCountries" "iMedCountries" ON "Candidates"."iMedCountryCodeResidence"="iMedCountries"."Code"
	 INNER JOIN  #TempList t ON t.FileNo = "Candidates"."Fileno"                 
	 WHERE  Certs.IssueDate IS NOT NULL AND Certs.Status = 'I' AND Certs.Issuedthrough IS NOT NULL
END
