USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCertificates]    Script Date: 18/11/2020 12:08:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCertificates] @ExamId int
AS
	-- Add the parameters for the stored procedure here 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT
	 "Candidates"."GivenNames"+' '+ UPPER("Candidates"."FamilyName") "Name"
	, "Certs"."Certno"
	, 'this '+CAST(DAY(Issuedate) AS VARCHAR(2)) + ' ' + DATENAME(MM, Issuedate) + ' ' + CAST(YEAR(Issuedate) AS VARCHAR(4)) AS "IssueDate"
    , "Images"."Image"
	, "Candidates"."GivenNames"
	, "Candidates"."FamilyName"
	, Certs.Status
	, Candidates.Fileno
	 FROM Certs
	 INNER JOIN Candidates ON Candidates.Fileno = Certs.FileNo
	 LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
	 WHERE CONVERT(varchar,Certs.Issuedate,103) = ( Select CONVERT(varchar,Exams.Examdate,103) FROM  Exams WHERE id = @ExamId)

        
END
