USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTabletMarkSheet]    Script Date: 18/11/2020 3:29:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTabletMarkSheet] @ExamId int
--, @Fileno int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT DISTINCT "vwResults_ClinicalClinicalSlotCandidates"."GivenNames"
 , "vwResults_ClinicalClinicalSlotCandidates"."FamilyName"
 , "vwResults_ClinicalClinicalSlotCandidates"."Results_ClinicalLastGrade"
 , "vwResults_ClinicalClinicalSlotCandidates"."ExamID"
 , "vwResults_ClinicalClinicalSlotCandidates"."Fileno"
 , "vwResults_ClinicalClinicalSlotCandidates"."ClinicalSlotCandidate_id"
 , "Exams".Centre
 , "Exams".Session
 , "vwResults_ClinicalClinicalSlotCandidates".clinicalscenarios_id
 , CONVERT(varchar,"Exams".Examdate,106) "ExamDate"
 , CASE WHEN "vwResults_ClinicalClinicalSlotCandidates".ClinicalSlot_AMPM = 1 THEN 'Morning Session'
        WHEN "vwResults_ClinicalClinicalSlotCandidates".ClinicalSlot_AMPM = 0 THEN 'Afternoon Session' 
  END "AMPM_Session" 
 FROM   "CTS"."dbo"."vwResults_ClinicalClinicalSlotCandidates" "vwResults_ClinicalClinicalSlotCandidates"
 INNER JOIN "CTS"."dbo"."Exams" ON "Exams".Id = "vwResults_ClinicalClinicalSlotCandidates"."ExamID"
 WHERE  "vwResults_ClinicalClinicalSlotCandidates"."ExamID"= @ExamId
 ---AND  "vwResults_ClinicalClinicalSlotCandidates"."Fileno" = @Fileno

	
END
