USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerTaxInvoice]    Script Date: 16/11/2021 12:02:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spExaminerTaxInvoice] @ExamId int
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	 BEGIN
	 SELECT 
  DISTINCT
	 CASE WHEN  Addr_Personal.ABNChecked='Y' THEN 'Checked' ELSE "Addr_Personal"."ABNChecked" END "ABNChecked"
 , "Addr_Personal"."ExaminersCode"
 , clinical_exams.name "ExamSession"
 , "Addr_Personal"."Company" 
 ,CAST(CASE 
			WHEN ISNUMERIC(REPLACE(REPLACE("Addr_Personal"."ABN", ' ', ''), '-', '')) = 0
				THEN NULL
			ELSE REPLACE(REPLACE(Addr_Personal.ABN, ' ', ''), '-', '')
   END AS VARCHAR(50)) ABN
    , ISNULL(Addr_Personal."Title",'')+' '+ISNULL(Addr_Personal."Initials",'')+' '+ISNULL(Addr_Personal."Surname",'') "TitleName"
 , Addr_Personal."Givennames"+' '+ Addr_Personal."Surname" "Name"
 , Addr_Personal."Surname"
 , Addr_Personal.Givennames
 , ContactData.ContactPaymentTypeID
 , exam_assignments.examiner_id "PersonalID"
 , clinical_exams.id "ExamID"
 , 1 "ContentOrder"
 , UPPER(clinical_venues.name) "HospitalName"
 , CONVERT(VARCHAR(10),  clinical_exams.date, 103) "EventDate"
 ,CASE 
    WHEN  AMC.cts.exam_part_sessions.number = 1
	THEN 'AM'
	ELSE 'PM'
  END TimeDurationDesc
	,TaxInvoiceFeetypeUnits.amount
	,TaxInvoiceFeetypeUnits.unit_numerator
	,TaxInvoiceFeetypeUnits.unit_denominator
 , Addr_Personal.GSTreg
 , TaxInvoiceFeetypes.unit TaxInvoiceFeetypes_unit
 ,addr_personal.f1_creditorid
 , 'Examiners' "TableName"
 , CAST(1 AS REAL) DurationTotal
 , '1' Quantity
 ,CASE 
			WHEN  AMC.cts.exam_part_sessions.number = 1
				THEN 'AM'
			ELSE 'PM'
   END AMPM
 	,CASE 
		WHEN ISNUMERIC(REPLACE(REPLACE(Addr_Personal.ABN, ' ', ''), '-', '')) = 0
			AND WithAusAddress.IsAusAddress >= 1
			THEN 1
		ELSE 0
		END WithholdTax
 , ISNULL(Line1,'')+' '+ISNULL(line2,'')+' '+ISNULL(line3,'')+' '+ISNULL(line4,'') "lineAddress"
 , ISNULL(Suburb,'')+' '+ISNULL(State,'')+' '+ISNULL(Postcode,'') "Suburb"
  , 'Examining Venue' "Venue"
  , CAST(1 AS REAL)*amount "Total"
 FROM AMC.cts.clinical_exams
	 INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	 INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	 INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	 INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	 INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
	 INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
	 AND exam_assignments.exam_part_session_id = exam_part_sessions.id
	 INNER JOIN  "CTS"."dbo"."Addr_Personal" Addr_Personal ON Addr_Personal.Id = exam_assignments.examiner_id
  INNER JOIN Address..TaxInvoiceFeetypeUnits TaxInvoiceFeetypeUnits ON clinical_exams.date BETWEEN TaxInvoiceFeetypeUnits.started_at
		AND TaxInvoiceFeetypeUnits.ended_at
  INNER JOIN Address..TaxInvoiceFeetypes TaxInvoiceFeetypes ON TaxInvoiceFeetypeUnits.TaxInvoiceFeetype_id = TaxInvoiceFeetypes.ID
	AND TaxInvoiceFeetypes.ID = 29
  INNER JOIN Address..Contactdata Contactdata ON Contactdata.PersonalID = exam_assignments.examiner_id
  LEFT JOIN (
	SELECT MasterId
		,SUM(CASE 
				WHEN (
						addr_addresses.TaxAddr = 'Y'
						AND (
							addr_addresses.Country IS NULL
							OR addr_addresses.Country = 'AUSTRALIA'
							)
						)
					THEN 1
				ELSE 0
				END) IsAusAddress
	FROM Address..addr_addresses addr_addresses
	GROUP BY MasterId
	) WithAusAddress ON exam_assignments.examiner_id  = WithAusAddress.MasterId
	LEFT OUTER  JOIN "CTS"."dbo"."Addr_Addresses" "Address" ON "Address".MasterId = exam_assignments.examiner_id AND MailAddr='Y'
  WHERE clinical_exams.id = @ExamId AND Addr_Personal.Surname <> 'REST STATION'
  UNION
   SELECT 
  DISTINCT
	 CASE WHEN  Addr_Personal.ABNChecked='Y' THEN 'Checked' ELSE "Addr_Personal"."ABNChecked" END "ABNChecked"
 , "Addr_Personal"."ExaminersCode"
 , clinical_exams.name "ExamSession"
 , "Addr_Personal"."Company" 
 ,CAST(CASE 
			WHEN ISNUMERIC(REPLACE(REPLACE("Addr_Personal"."ABN", ' ', ''), '-', '')) = 0
				THEN NULL
			ELSE REPLACE(REPLACE(Addr_Personal.ABN, ' ', ''), '-', '')
   END AS VARCHAR(50)) ABN
    , ISNULL(Addr_Personal."Title",'')+' '+ISNULL(Addr_Personal."Initials",'')+' '+ISNULL(Addr_Personal."Surname",'') "TitleName"
 , Addr_Personal."Givennames"+' '+ Addr_Personal."Surname" "Name"
 , Addr_Personal."Surname"
 , Addr_Personal.Givennames
 , ContactData.ContactPaymentTypeID
 , amc.cts.spare_stakeholders.contact_id"PersonalID"
 , clinical_exams.id "ExamID"
 , 1 "ContentOrder"
 , UPPER(clinical_venues.name) "HospitalName"
 , CONVERT(VARCHAR(10),  clinical_exams.date, 103) "EventDate"
 ,CASE 
    WHEN  AMC.cts.exam_part_sessions.number = 1
	THEN 'AM'
	ELSE 'PM'
  END TimeDurationDesc
	,TaxInvoiceFeetypeUnits.amount
	,TaxInvoiceFeetypeUnits.unit_numerator
	,TaxInvoiceFeetypeUnits.unit_denominator
 , Addr_Personal.GSTreg
 , TaxInvoiceFeetypes.unit TaxInvoiceFeetypes_unit
 ,addr_personal.f1_creditorid
 , role "TableName"
 , CAST(1 AS REAL) DurationTotal
 , '1' Quantity
 ,CASE 
			WHEN  AMC.cts.exam_part_sessions.number = 1
				THEN 'AM'
			ELSE 'PM'
   END AMPM
 	,CASE 
		WHEN ISNUMERIC(REPLACE(REPLACE(Addr_Personal.ABN, ' ', ''), '-', '')) = 0
			AND WithAusAddress.IsAusAddress >= 1
			THEN 1
		ELSE 0
		END WithholdTax
 , ISNULL(Line1,'')+' '+ISNULL(line2,'')+' '+ISNULL(line3,'')+' '+ISNULL(line4,'') "lineAddress"
 , ISNULL(Suburb,'')+' '+ISNULL(State,'')+' '+ISNULL(Postcode,'') "Suburb"
  , role+' Venue' "Venue"
  , CAST(1 AS REAL)*amount "Total"
FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
		--AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
	 INNER JOIN  "CTS"."dbo"."Addr_Personal" Addr_Personal ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
  INNER JOIN Address..TaxInvoiceFeetypeUnits TaxInvoiceFeetypeUnits ON clinical_exams.date BETWEEN TaxInvoiceFeetypeUnits.started_at
		AND TaxInvoiceFeetypeUnits.ended_at
  INNER JOIN Address..TaxInvoiceFeetypes TaxInvoiceFeetypes ON TaxInvoiceFeetypeUnits.TaxInvoiceFeetype_id = TaxInvoiceFeetypes.ID
  AND TaxInvoiceFeetypes.description = 'AZM '+role+' (day/session)'
  INNER JOIN Address..Contactdata Contactdata ON Contactdata.PersonalID = amc.cts.spare_stakeholders.contact_id
  LEFT JOIN (
	SELECT MasterId
		,SUM(CASE 
				WHEN (
						addr_addresses.TaxAddr = 'Y'
						AND (
							addr_addresses.Country IS NULL
							OR addr_addresses.Country = 'AUSTRALIA'
							)
						)
					THEN 1
				ELSE 0
				END) IsAusAddress
	FROM Address..addr_addresses addr_addresses
	GROUP BY MasterId
	) WithAusAddress ON amc.cts.spare_stakeholders.contact_id = WithAusAddress.MasterId
	LEFT OUTER  JOIN "CTS"."dbo"."Addr_Addresses" "Address" ON "Address".MasterId = amc.cts.spare_stakeholders.contact_id AND MailAddr='Y'
  WHERE clinical_exams.id = @ExamId AND Addr_Personal.Surname <> 'REST STATION'
  AND role IN ('QA Examiner', 'Chair')
  END
  ELSE

  BEGIN
    -- Insert statements for procedure here
	 SELECT DISTINCT
	 CASE WHEN  "vwClinicalExaminerTaxInvoice"."ABNChecked"='Y' THEN 'Checked' ELSE "vwClinicalExaminerTaxInvoice"."ABNChecked" END "ABNChecked"
 , "vwClinicalExaminerTaxInvoice"."ExaminersCode"
 , "vwClinicalExaminerTaxInvoice"."ExamSession"
 , "vwClinicalExaminerTaxInvoice"."Company"
 , "vwClinicalExaminerTaxInvoice"."ABN"
 , ISNULL("vwClinicalExaminerTaxInvoice"."Title",'')+' '+ISNULL("vwClinicalExaminerTaxInvoice"."Initials",'')+' '+ISNULL("vwClinicalExaminerTaxInvoice"."Surname",'') "TitleName"
 , "vwClinicalExaminerTaxInvoice"."Givennames"+' '+ "vwClinicalExaminerTaxInvoice"."Surname" "Name"
 , "vwClinicalExaminerTaxInvoice"."Surname"
 , "vwClinicalExaminerTaxInvoice"."Givennames"
 , "vwClinicalExaminerTaxInvoice"."ContactPaymentTypeID"
 , "vwClinicalExaminerTaxInvoice"."PersonalID"
 , "vwClinicalExaminerTaxInvoice"."ExamID"
 , "vwClinicalExaminerTaxInvoice"."ContentOrder"
 , "vwClinicalExaminerTaxInvoice"."HospitalName"
 , CONVERT(VARCHAR,"vwClinicalExaminerTaxInvoice"."EventDate",103) "EventDate"
 , "vwClinicalExaminerTaxInvoice"."TimeDurationDesc"
 , "vwClinicalExaminerTaxInvoice"."amount"
 , "vwClinicalExaminerTaxInvoice"."unit_numerator"
 , "vwClinicalExaminerTaxInvoice"."unit_denominator"
 , "vwClinicalExaminerTaxInvoice"."GSTreg"
 , "vwClinicalExaminerTaxInvoice"."TaxInvoiceFeetypes_unit"
 , "vwClinicalExaminerTaxInvoice"."f1_creditorid"
 , "vwClinicalExaminerTaxInvoice"."TableName"
 , "vwClinicalExaminerTaxInvoice"."DurationTotal"
 , "vwClinicalExaminerTaxInvoice"."Quantity"
 , "vwClinicalExaminerTaxInvoice"."AMPM"
 , "vwClinicalExaminerTaxInvoice"."WithholdTax"
 , ISNULL(Line1,'')+' '+ISNULL(line2,'')+' '+ISNULL(line3,'')+' '+ISNULL(line4,'') "lineAddress"
 , ISNULL(Suburb,'')+' '+ISNULL(State,'')+' '+ISNULL(Postcode,'') "Suburb"
  , CASE 
   WHEN TableName='ClinicalSlotExaminers' THEN 'Examining Venue'
   WHEN TableName='ClinicalObservers' THEN 'Observing Venue'
   WHEN TableName='PanelAttendees' THEN 'Panel Meeting Attendance Venue'
   WHEN TableName='ClinicalSlotRolePlayers' THEN 'Role Player Venue'
   WHEN TableName='ClinicalChairperson' THEN 'Chairperson Venue'
   WHEN TableName='ClinicalTrainingAttend' THEN 'Examiner Training Workshop Attendance Venue'
   END "Venue"
  , DurationTotal*amount "Total"
 FROM   "CTS"."dbo"."vwClinicalExaminerTaxInvoice" "vwClinicalExaminerTaxInvoice"
 LEFT OUTER  JOIN "CTS"."dbo"."Addr_Addresses" "Address" ON "Address".MasterId = "vwClinicalExaminerTaxInvoice".PersonalID AND MailAddr='Y'
 WHERE  "vwClinicalExaminerTaxInvoice"."ExamID"= @l_cts_exam_id
 END
END
