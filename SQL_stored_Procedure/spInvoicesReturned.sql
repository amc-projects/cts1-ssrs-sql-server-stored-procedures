USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spInvoicesReturned]    Script Date: 16/11/2021 2:42:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInvoicesReturned] @ExamId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	 BEGIN
	 SELECT 
  DISTINCT
	clinical_exams.id "ExamID"
    , ISNULL(Addr_Personal."Title",'')+' '+ISNULL(Addr_Personal."Initials",'')+' '+ISNULL(Addr_Personal."Surname",'') "Name"
    , clinical_exams.name "ExamSession"
	, ISNULL(Addr_Personal."Surname",'') "Surname"
 FROM AMC.cts.clinical_exams
	 INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	 INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	 INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	 INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	 INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
	 INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
	 AND exam_assignments.exam_part_session_id = exam_part_sessions.id
	 INNER JOIN  "CTS"."dbo"."Addr_Personal" Addr_Personal ON Addr_Personal.Id = exam_assignments.examiner_id
  INNER JOIN Address..TaxInvoiceFeetypeUnits TaxInvoiceFeetypeUnits ON clinical_exams.date BETWEEN TaxInvoiceFeetypeUnits.started_at
		AND TaxInvoiceFeetypeUnits.ended_at
  INNER JOIN Address..TaxInvoiceFeetypes TaxInvoiceFeetypes ON TaxInvoiceFeetypeUnits.TaxInvoiceFeetype_id = TaxInvoiceFeetypes.ID
	AND TaxInvoiceFeetypes.ID = 11
  INNER JOIN Address..Contactdata Contactdata ON Contactdata.PersonalID = exam_assignments.examiner_id
  WHERE clinical_exams.id = @ExamId AND Addr_Personal.Surname <> 'REST STATION'

  UNION
   SELECT 
   DISTINCT
	clinical_exams.id "ExamID"
    , ISNULL(Addr_Personal."Title",'')+' '+ISNULL(Addr_Personal."Initials",'')+' '+ISNULL(Addr_Personal."Surname",'')+' '+ISNULL(Addr_Personal."Givennames",'') "Name"
    , clinical_exams.name "ExamSession"
	, ISNULL(Addr_Personal."Surname",'') "Surname"
   FROM AMC.cts.clinical_exams
	INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
	INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
	INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
	--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
	INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
	--AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
	INNER JOIN  "CTS"."dbo"."Addr_Personal" Addr_Personal ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
	INNER JOIN Address..TaxInvoiceFeetypeUnits TaxInvoiceFeetypeUnits ON clinical_exams.date BETWEEN TaxInvoiceFeetypeUnits.started_at
	AND TaxInvoiceFeetypeUnits.ended_at
	INNER JOIN Address..TaxInvoiceFeetypes TaxInvoiceFeetypes ON TaxInvoiceFeetypeUnits.TaxInvoiceFeetype_id = TaxInvoiceFeetypes.ID
	AND TaxInvoiceFeetypes.ID = 11
  INNER JOIN Address..Contactdata Contactdata ON Contactdata.PersonalID = amc.cts.spare_stakeholders.contact_id
  WHERE clinical_exams.id = @ExamId AND Addr_Personal.Surname <> 'REST STATION'
  AND role IN ('QA Examiner', 'Chair')
  END
  ELSE
  BEGIN
    -- Insert statements for procedure here
	SELECT DISTINCT "vwClinicalExaminerTaxInvoice"."ExamID"
	, ISNULL("vwClinicalExaminerTaxInvoice"."Title",'')+' ' +ISNULL("vwClinicalExaminerTaxInvoice"."Initials",'')+' '+ISNULL("vwClinicalExaminerTaxInvoice"."Surname",'')+' '+ISNULL("vwClinicalExaminerTaxInvoice"."Givennames",'') "Name"
      , "vwClinicalExaminerTaxInvoice"."ExamSession"
	  , ISNULL("vwClinicalExaminerTaxInvoice"."Surname",'') "Surname"
	FROM   "CTS"."dbo"."vwClinicalExaminerTaxInvoice" "vwClinicalExaminerTaxInvoice"
	WHERE  "vwClinicalExaminerTaxInvoice"."ExamID"=  @l_cts_exam_id
 END
END
