USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spExaminerRolePlayerListing]    Script Date: 25/02/2022 2:33:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spExaminerRolePlayerListing] @ExamId int, @ListingType varchar(20)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
	
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	 BEGIN
	 IF @ListingType = 'Examiner' 
	   BEGIN
	   SELECT 
	      "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') +' ('+ ISNULL("addr_speciality"."Speciality",'')+')' "Name"
		 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
		 , ISNULL("addr_addresses"."Line1",'')+' '+ISNULL("addr_addresses"."Line2",'')+' '+ ISNULL("addr_addresses"."Line3",'')+' '+ISNULL("addr_addresses"."Line4",'') "Line Address"
		 , ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ISNULL("addr_addresses"."Postcode",'') "State Address"
		 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
		 , CONVERT(VARCHAR(10),  clinical_exams.date, 103)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 , 'Examining' Type
		 , CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		   END "AM/PM"
         , 'BLUE' "Group"
		 , '' Length
		 , exam_part_stations.number "SortOrder"
		 ,  ROW_NUMBER() OVER(ORDER BY "Addr_Personal"."Surname","Addr_Personal"."Givennames" ASC)%2  AS Row#
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.examiner_id
	    INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	    INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	    WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId
		AND examiner_id IS NOT NULL

		UNION 

		SELECT
         "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') +' ('+ ISNULL("addr_speciality"."Speciality",'')+')' "Name"
		 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
		 , ISNULL("addr_addresses"."Line1",'')+' '+ISNULL("addr_addresses"."Line2",'')+' '+ ISNULL("addr_addresses"."Line3",'')+' '+ISNULL("addr_addresses"."Line4",'') "Line Address"
		 , ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ISNULL("addr_addresses"."Postcode",'') "State Address"
		 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
		 , CONVERT(VARCHAR(10),  clinical_exams.date, 103)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 , role Type
		 , CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		   END "AM/PM"
         , 'BLUE' "Group"
		 , '' Length
		 , 100 "SortOrder"
		 ,  ROW_NUMBER() OVER(ORDER BY "Addr_Personal"."Surname","Addr_Personal"."Givennames" ASC)%2  AS Row#
        FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		--INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN amc.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
		--AND spare_stakeholders.exam_part_session_id = exam_part_sessions.id
		--INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = amc.cts.spare_stakeholders.contact_id
	    INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	    INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	    WHERE   "addr_addresses"."MailAddr"='Y'
        AND  clinical_exams.id =@ExamId
     	AND role IN ('QA Examiner', 'Chair') 
      END
	  ELSE IF @ListingType = 'RolePlayer'
		BEGIN
		WITH q AS
		(
		 SELECT 
	      "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'')  "Name"
		 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
		 , ISNULL("addr_addresses"."Line1",'')+' '+ISNULL("addr_addresses"."Line2",'')+' '+ ISNULL("addr_addresses"."Line3",'')+' '+ISNULL("addr_addresses"."Line4",'') "Line Address"
		 , ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ISNULL("addr_addresses"."Postcode",'') "State Address"
		 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
		 , CONVERT(VARCHAR(10),  clinical_exams.date, 103)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 ,'Role Playing' "Type"
		 , CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		   END "AM/PM"
         , 'BLUE' "Group"
		 , '' Length
		 , exam_part_stations.number "SortOrder"
		 , "Addr_Personal"."Givennames"
		 , "Addr_Personal"."Surname"
		 --,  ROW_NUMBER() OVER(ORDER BY "Addr_Personal"."Surname","Addr_Personal"."Givennames" ASC)%2  AS Row#
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = exam_assignments.simulated_patient_id
	    INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
	    INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
	    WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId

		UNION

		SELECT 
	      "Addr_Personal"."Id"
		 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'')  "Name"
		 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
		 , ISNULL("addr_addresses"."Line1",'')+' '+ISNULL("addr_addresses"."Line2",'')+' '+ ISNULL("addr_addresses"."Line3",'')+' '+ISNULL("addr_addresses"."Line4",'') "Line Address"
		 , ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ISNULL("addr_addresses"."Postcode",'') "State Address"
		 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
		 , CONVERT(VARCHAR(10),  clinical_exams.date, 103)"Examsdate"
		 , clinical_exams.name "Session"
		, UPPER(clinical_venues.name) "Centre"
		 , UPPER(clinical_venues.name) "HospitalName"  
		 ,'Role Playing' "Type"
		 , CASE 
		   WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM'
		   ELSE 'PM'
		   END "AM/PM"
         , 'BLUE' "Group"
		 , '' Length
		 , 100 "SortOrder"
		 , "Addr_Personal"."Givennames"
		 , "Addr_Personal"."Surname"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.spare_stakeholders ON spare_stakeholders.exam_part_id = exam_parts.id
		--AND  spare_stakeholders.exam_part_session_id = exam_part_sessions.id
		INNER JOIN  "CTS"."dbo"."Addr_Personal" "Addr_Personal" ON Addr_Personal.Id = spare_stakeholders.contact_id
		INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
		INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
		WHERE   "addr_addresses"."MailAddr"='Y'
		AND clinical_exams.id = @ExamId
		AND spare_stakeholders.role = 'Simulated Patient'
		)
		SELECT 
	     q."Id"
		 , q."Name"
		 ,q."MailAddr", q."MasterId"
		 , q."Line Address"
		 , q."State Address"
		 , q."Mobile", q."Phone2", q."Phone1"
		 , q."Examsdate"
		 , q."Session"
		, q."Centre"
		 , q."HospitalName"  
		 ,q."Type"
		 , q."AM/PM"
         , q."Group"
		 , q.Length
		 , q."SortOrder"
		 ,  ROW_NUMBER() OVER(ORDER BY q."Surname",q."Givennames" ASC)%2  AS Row#
		 FROM q

      END

    END
	ELSE
		IF @ListingType = 'Examiner' 
		BEGIN 
			-- Insert statements for procedure here
			WITH examiners AS 
			(
				SELECT DISTINCT Exams.ID
				, Exams.Session
				, ClinicalSlotExaminers.ExaminerID
				, Exams.Centre
				, CONVERT(VARCHAR(10),  Exams.Examdate, 103)  AS "Examsdate"
				, ClinicalSlotExaminers.SortOrder
				, ClinicalSlot.Colour  "Group"
				, CASE WHEN ClinicalSlot.AMPM = 1 THEN 'AM' WHEN ClinicalSlot.AMPM = 0 THEN 'PM' END AS "AM/PM"
				, 'Examining' "Type"
				, Venues.HospitalName
				FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
				INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
				LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
				INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID 
				INNER JOIN ClinicalSlotExaminers ON ClinicalSlot.ID = ClinicalSlotExaminers.ClinicalSlotID
				WHERE Exams.ID = @l_cts_exam_id
				AND ExaminerID NOT IN (9175,9612,9613,9555,10213)

				UNION 

				SELECT DISTINCT Exams.ID
				, Exams.Session
				, ClinicalObservers.ExaminerID
				, Exams.Centre
				, CONVERT(VARCHAR(10),Exams.Examdate, 103)  AS "Examsdate"
				, '' "SortOrder"
				, '' "Group"
				, CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
				, 'Observing' "Type"
				, Venues.HospitalName
				FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID 
				INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
				LEFT OUTER JOIN Venues On Venues.Id = ClinicalVenues.VenueID
				INNER JOIN ClinicalObservers ON ClinicalVenues.ID = ClinicalObservers.ClinicalVenueID
				--INNER JOIN ClinicalObserverRotations ON ClinicalObservers.ID = ClinicalObserverRotations.ClinicalObserverID
				--INNER JOIN ClinicalSlot ON ClinicalObserverRotations.SetNo = ClinicalSlot.ID
				WHERE Exams.ID = @l_cts_exam_id
				AND ExaminerID NOT IN (9175,9612,9613,9555,10213)
			)
			SELECT "Addr_Personal"."Id"
			 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'') +' ('+ ISNULL("addr_speciality"."Speciality",'')+')' "Name"
			 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
			 , ISNULL("addr_addresses"."Line1",'')+' '+ISNULL("addr_addresses"."Line2",'')+' '+ ISNULL("addr_addresses"."Line3",'')+' '+ISNULL("addr_addresses"."Line4",'') "Line Address"
			 , ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ISNULL("addr_addresses"."Postcode",'') "State Address"
			 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
			 , e.Examsdate, e.Session, e.Centre
			 , e.Type
			 , e.[AM/PM]
			 , e.SortOrder
			 , e.HospitalName
			 , "Addr_Personal".Surname
			 , "Addr_Personal".Givennames
			 , e."Group"
			 , '' Length
			 ,  ROW_NUMBER() OVER(ORDER BY "Addr_Personal"."Surname","Addr_Personal"."Givennames" ASC)%2  AS Row#
			FROM   "CTS"."dbo"."Addr_Personal" "Addr_Personal"
			INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
			INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
			INNER JOIN examiners e ON e.ExaminerID = Addr_Personal.Id
			WHERE   "addr_addresses"."MailAddr"='Y'
		END

		ELSE IF @ListingType = 'RolePlayer'
		BEGIN
		 WITH roleplayers AS
		 (
			SELECT DISTINCT 
			   Exams.Id
			 , Exams.Session
			 , ClinicalSlotRolePlayers.ExaminerID
			 , Exams.Centre
			 , CONVERT(VARCHAR(10),Exams.Examdate, 103)  AS "Examsdate"
			 , ClinicalSlotRolePlayers.SortOrder "SortOrder"
			 , ClinicalSlot.Colour  "Group"
			 , CASE WHEN ClinicalVenues.AM = 'Yes' THEN 'AM' WHEN ClinicalVenues.PM = 'Yes' THEN 'PM' END AS "AM/PM"
			 , 'Role Playing' "Type"
			, Venues.HospitalName
			, '' "Length"
			 FROM Exams INNER JOIN ClinicalDates ON Exams.ID = ClinicalDates.ExamID
			 INNER JOIN ClinicalVenues ON ClinicalDates.ID = ClinicalVenues.ClinicalDateID
			 LEFT OUTER  JOIN Venues On Venues.Id = ClinicalVenues.VenueID
			 INNER JOIN ClinicalSlot ON ClinicalVenues.ID = ClinicalSlot.ClinicalVenueID
			 INNER JOIN ClinicalSlotRolePlayers ON ClinicalSlot.ID = ClinicalSlotRolePlayers.ClinicalSlotID
			 WHERE Exams.ID = @l_cts_exam_id
			 AND ClinicalSlotRolePlayers.ExaminerID NOT IN (9555,10284)

			 UNION 

			SELECT DISTINCT 
			   Exams.Id
			 , Exams.Session
			 , ClinicalTrainingExaminers.ExaminerID
			 , Exams.Centre
			 , CONVERT(VARCHAR(10),Exams.Examdate, 103)  AS "Examsdate"
			 , '' "SortOrder"
			 , '' "Group"
			 , '' "AM/PM"
			 , 'Training Night' "Type"
			, Venues.HospitalName
			, 'Length '+ClinicalTraining.TrainingLength+' hours' "Length"
			FROM Exams INNER JOIN ClinicalTraining ON Exams.ID = ClinicalTraining.ExamID
			INNER JOIN ClinicalTrainingExaminers ON ClinicalTraining.ID = ClinicalTrainingExaminers.TrainingID
			LEFT OUTER  JOIN Venues On Venues.Id = ClinicalTraining.VenueID
			WHERE Exams.ID = @l_cts_exam_id
			AND ClinicalTrainingExaminers.ExaminerID NOT IN (10432, 10316,10431,8830,9555,10284,14621) 
		   )
		   SELECT "Addr_Personal"."Id"
			 ,  ISNULL("Addr_Personal"."Title",'')+' '+ISNULL("Addr_Personal"."Givennames",'')+' '+ISNULL("Addr_Personal"."Surname",'')  "Name"
			 ,"addr_addresses"."MailAddr", "addr_addresses"."MasterId"
			 , ISNULL("addr_addresses"."Line1",'')+' '+ISNULL("addr_addresses"."Line2",'')+' '+ ISNULL("addr_addresses"."Line3",'')+' '+ISNULL("addr_addresses"."Line4",'') "Line Address"
			 , ISNULL("addr_addresses"."Suburb",'')+' '+ISNULL("addr_addresses"."State",'')+' '+ISNULL("addr_addresses"."Postcode",'') "State Address"
			 , "addr_addresses"."Mobile", "addr_addresses"."Phone2", "addr_addresses"."Phone1"
			 , r.Examsdate, r.Session, r.Centre
			 , r.Type
			 , r.[AM/PM]
			 , r.SortOrder
			 , r.HospitalName
			 , "Addr_Personal".Surname
			 , "Addr_Personal".Givennames
			 , r.Length
			 , r."Group"
			 ,  ROW_NUMBER() OVER(ORDER BY "Addr_Personal"."Surname","Addr_Personal"."Givennames" ASC)%2  AS Row#
			FROM   "CTS"."dbo"."Addr_Personal" "Addr_Personal"
			--INNER JOIN  "Address"."dbo"."addr_speciality" "addr_speciality" ON "addr_speciality"."ID"= Addr_Personal.Id
			INNER JOIN  "Address"."dbo"."addr_addresses" "addr_addresses" ON "addr_addresses".MasterId = Addr_Personal.Id
			INNER JOIN roleplayers r ON r.ExaminerID = Addr_Personal.Id
			--WHERE   "addr_addresses"."MailAddr"='Y'

	   END

END
