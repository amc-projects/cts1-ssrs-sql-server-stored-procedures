USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spAMCRegisterCertificates]    Script Date: 18/11/2020 9:54:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAMCRegisterCertificates]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	DISTINCT "Certs"."Certno"
	,	"Certs"."Fileno"
	,	UPPER("Candidates"."FamilyName")+', '+"Candidates"."GivenNames" "Name"
	,   CONVERT(VARCHAR(10),"Certs"."Issuedate",103) "IssueDate"
	,   CASE 
			WHEN "Certs"."Status" = 'I' THEN 'Issued'
			WHEN "Certs"."Status" = 'NI' THEN 'Not Issued'
			WHEN "Certs"."Status" = 'SAFE' THEN 'In AMC Safe'
			WHEN "Certs"."Status" = 'C' THEN 'Cancelled'
		 END "Status"
	,   "Certs"."Amec"
	FROM "CTS"."dbo"."Certs" "Certs"
	INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "Certs"."Fileno"="Candidates"."Fileno"
END
