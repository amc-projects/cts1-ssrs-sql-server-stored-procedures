USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptClinStage2]    Script Date: 18/11/2020 10:37:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptClinStage2] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   SELECT "Exams"."Session"
 , "Results_Clin_Stage2"."Passfail"
 , "Exams"."Examdate" "exam_date"
 , CONVERT(varchar,"Exams"."Examdate",103) "Examdate"
 , "Exams"."Centre"
 , "Results_Clin_Stage2"."Fileno"
 , "Exams"."ID"
 FROM   "CTS"."dbo"."Results_Clin_Stage2" "Results_Clin_Stage2"
 INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Results_Clin_Stage2"."ExamID"="Exams"."ID"
 WHERE  "Results_Clin_Stage2"."Fileno"= @FileNo
 



   
 --ORDER BY "ExamTypes"."sortorder"


END
