USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spEntranceTickets]    Script Date: 15/11/2021 11:18:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spEntranceTickets] @ExamId int, @AM_Reporting_Time varchar(10), @PM_Reporting_Time varchar(10)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @l_exam_id AS INT;
	DECLARE @l_cts_exam_id AS INT;
	DECLARE @l_exam_type_id AS INT;

	SELECT @l_exam_id = id, @l_cts_exam_id = cts_exam_id
	FROM AMC.cts.clinical_exams clinical_exams
	WHERE clinical_exams.id = @ExamID;
		
	--This returns Online or Face to Face
	EXEC @l_exam_type_id  = CTS.dbo.spExamType @ExamID

	IF @l_exam_type_id = 1 
	BEGIN
	  SELECT UPPER("Candidates"."GivenNames") "GivenNames"
	 , UPPER("Candidates"."FamilyName" ) "FamilyName"
	 , "Candidates"."Fileno"
	 , clinical_exams.name "Session"
	  , datename(dw,clinical_exams.date)+', '+CONVERT(VARCHAR, clinical_exams.date,106) "ExamDate"
	 , clinical_venues.name "Centre"
	 , 'BLUE' "Colour"
	 , '' "GroupText"
	 ,  AMC.cts.exam_part_sessions.number "AMPM"
	 , 'MCAT Stations' "Discipline"
	 , clinical_exams.exam_type_id "ExamTypeID"
	 , "Images".Image  "cts_image"
	 , clinical_exams.id
	 , CASE 
		WHEN  AMC.cts.exam_part_sessions.number = 1 THEN 'AM Session' 
		ELSE 'PM Session'
	   END "AMPMSession"  
	 , clinical_exams.date 
	, CASE 
		WHEN  AMC.cts.exam_part_sessions.number = 1 THEN @AM_Reporting_Time
		ELSE  @PM_Reporting_Time
	   END "ReportingTime"
		FROM AMC.cts.clinical_exams
		INNER JOIN AMC.cts.clinical_venues ON clinical_exams.venue_id = clinical_venues.id
		INNER JOIN AMC.cts.exam_parts ON exam_parts.exam_id = clinical_exams.id
		INNER JOIN AMC.cts.exam_part_sessions ON exam_part_sessions.exam_part_id = exam_parts.id
		INNER JOIN AMC.cts.exam_part_stations ON exam_part_stations.exam_part_id = exam_parts.id
		INNER JOIN AMC.exam_content.scenarios ON scenarios.id = exam_part_stations.scenario_id
		INNER JOIN AMC.cts.exam_assignments ON exam_assignments.exam_part_station_id = exam_part_stations.id
		AND exam_assignments.exam_part_session_id = exam_part_sessions.id
		INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON exam_assignments.candidate_id="Candidates"."Fileno"
		LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
		WHERE clinical_exams.id = @ExamId
		AND candidate_id IS NOT NULL
	END
	ELSE
	BEGIN
		SELECT UPPER("Candidates"."GivenNames") "GivenNames"
	 , UPPER("Candidates"."FamilyName" ) "FamilyName"
	 , "Candidates"."Fileno"
	 , "Exams"."Session"
	 , datename(dw,"Exams"."Examdate")+', '+CONVERT(VARCHAR, "Exams"."Examdate",106) "ExamDate"
	 , "Exams"."Centre"
	 , "ClinicalSlot"."Colour"
	 , "ClinicalSlot"."GroupText"
	 , "ClinicalSlot"."AMPM"
	 , "Venues"."Discipline"
	 , "Exams"."ExamTypeID"
	 , "Images".Image "cts_image"
	 , "Exams".id
	 , CASE 
		WHEN  "Exams"."ExamTypeID"=2 AND "ClinicalSlot"."AMPM" = 1 THEN 'AM Session' 
		WHEN  "Exams"."ExamTypeID"=2 AND "ClinicalSlot"."AMPM" = 0 THEN 'PM Session'
		WHEN  "Exams"."ExamTypeID"!=2 AND "ClinicalSlot"."AMPM" = 0 THEN "ClinicalSlot"."Colour"+' Group '+"ClinicalSlot"."GroupText"+' - '+'PM Session'
		WHEN  "Exams"."ExamTypeID"!=2 AND "ClinicalSlot"."AMPM" = 1 THEN "ClinicalSlot"."Colour"+' Group '+"ClinicalSlot"."GroupText"+' - '+'AM Session'
	   END "AMPMSession"  
	 , Exams.Examdate  
	 , CASE 
		WHEN  "ClinicalSlot"."AMPM" = 1 THEN @AM_Reporting_Time
		WHEN  "ClinicalSlot"."AMPM" = 0 THEN @PM_Reporting_Time
	   END "ReportingTime"		 
	 FROM   (((((("CTS"."dbo"."Exams" "Exams" INNER JOIN "CTS"."dbo"."ClinicalDates" "ClinicalDates" ON "Exams"."ID"="ClinicalDates"."ExamID") 
	 INNER JOIN "CTS"."dbo"."ClinicalVenues" "ClinicalVenues" ON "ClinicalDates"."ID"="ClinicalVenues"."ClinicalDateID")
	 INNER JOIN "CTS"."dbo"."ClinicalSlot" "ClinicalSlot" ON "ClinicalVenues"."ID"="ClinicalSlot"."ClinicalVenueID") 
	 INNER JOIN "CTS"."dbo"."Venues" "Venues" ON "ClinicalVenues"."VenueID"="Venues"."ID")
	 INNER JOIN "CTS"."dbo"."ClinicalSlotCandidates" "ClinicalSlotCandidates" ON "ClinicalSlot"."ID"="ClinicalSlotCandidates"."ClinicalSlotID")
	 INNER JOIN "CTS"."dbo"."Candidates" "Candidates" ON "ClinicalSlotCandidates"."FileNo"="Candidates"."Fileno")
	 LEFT OUTER JOIN "CTS"."dbo"."Images" "Images" ON "Candidates"."Fileno"="Images"."FileNo"
	WHERE "Exams".id = @l_cts_exam_id
	AND "Candidates"."Fileno" ! = 1234
  END
END
