USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spStationsInfo]    Script Date: 18/11/2020 3:23:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStationsInfo] @ExamId int, @reportNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @reportNo = 1 
	BEGIN
	SELECT "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder", "vwResults_ClinicalScenarioType"."Scenario_ScenType", "vwResults_ClinicalScenarioType"."ExamID"
    FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
    WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
    AND ("vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder"<=7 ) 
    AND "vwResults_ClinicalScenarioType"."Scenario_ScenType"<>'NOT DEFINED'
	END 

	IF @reportNo = 2 
	BEGIN
	SELECT "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder", "vwResults_ClinicalScenarioType"."Scenario_ScenType", "vwResults_ClinicalScenarioType"."ExamID"
    FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
    WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
    AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder">7  
	AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder" <=14
    AND "vwResults_ClinicalScenarioType"."Scenario_ScenType"<>'NOT DEFINED'
	END

	IF @reportNo = 3 
	BEGIN
	SELECT "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder", "vwResults_ClinicalScenarioType"."Scenario_ScenType", "vwResults_ClinicalScenarioType"."ExamID"
    FROM   "CTS"."dbo"."vwResults_ClinicalScenarioType" "vwResults_ClinicalScenarioType"
    WHERE  "vwResults_ClinicalScenarioType"."ExamID"=@ExamId
    AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder">14  
	AND "vwResults_ClinicalScenarioType"."ClinicalSlotScenarios_Sortorder" <=20
    AND "vwResults_ClinicalScenarioType"."Scenario_ScenType"<>'NOT DEFINED'
	END

END
