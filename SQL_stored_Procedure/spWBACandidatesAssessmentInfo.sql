USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spWBACandidatesAssessmentInfo]    Script Date: 18/11/2020 3:39:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spWBACandidatesAssessmentInfo]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH last_meeting_date AS 
	(
	SELECT
	 wba.FileNo
	 , wba_placements.id "placement_id"
	 , MAX(wba_assessment_reviews.meeting_id) meeting_id
	FROM CTS.dbo.Candidates  
	INNER JOIN CTS.dbo.WBA wba ON Candidates.Fileno = wba.Fileno
	INNER JOIN [AMC].[cts].wba_placements wba_placements ON wba.PlacementId = wba_placements.id
	LEFT JOIN [AMC].[cts].[wba_assessment_reviews] wba_assessment_reviews ON wba_assessment_reviews.placement_id = wba_placements.id
	 GROUP BY wba.FileNo
	 , wba_placements.id 
	 )
	SELECT
	   wba.FileNo
	 , Candidates.GivenNames+' '+Candidates.FamilyName "Name"
	 , wba_placements.status "WBA Status"
	 , wba_instruments.name "Assessment Type"
	 , wba_assessments.clinical_skill "Clinical skill"
	 , wba_clinical_areas.name "Clinical Area"
	 , wba_assessment_reviews.final_result
	 --, wba_placements.id "placement_id"
	 --, wba_assessment_reviews.meeting_id
	 , meetings.date "meeting_date"
	FROM CTS.dbo.Candidates  
	INNER JOIN CTS.dbo.WBA wba ON Candidates.Fileno = wba.Fileno
	INNER JOIN [AMC].[cts].wba_placements wba_placements ON wba.PlacementId = wba_placements.id
	INNER JOIN [AMC].[cts].wba_assessments wba_assessments ON wba_assessments.wba_placement_id  = wba_placements.id
	INNER JOIN [AMC].[cts].[wba_clinical_areas] wba_clinical_areas ON wba_assessments.wba_clinical_area_id = wba_clinical_areas.id
	INNER JOIN [AMC].[cts].[wba_assessment_templates] wba_assessment_templates ON wba_assessment_templates.id = wba_assessment_template_id
	INNER JOIN [AMC].[cts].[wba_instruments] wba_instruments ON wba_instruments.id = wba_assessment_templates.wba_instrument_id
	LEFT JOIN last_meeting_date ON last_meeting_date.placement_id =  wba_placements.id
	LEFT JOIN [AMC].[cts].[wba_assessment_reviews] wba_assessment_reviews ON wba_assessment_reviews.meeting_id = last_meeting_date.meeting_id AND wba_assessment_reviews.placement_id = last_meeting_date.placement_id
	LEFT JOIN [AMC].[cts].[meetings] ON meetings.id = last_meeting_date.meeting_id
	LEFT JOIN CTS.dbo.Certs ON Certs.Fileno = Candidates.Fileno
	WHERE Certs.deleted_at IS NULL 
	AND (YEAR("wba_placements".start_date) = '2019' OR YEAR("wba_placements".end_date) = '2019')
	
END
