USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptClin972001]    Script Date: 18/11/2020 10:36:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptClin972001] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT "Results_Clin"."FileNo"
	, CONVERT(varchar,"Exams"."Examdate",103) "Examdate"
	, "Exams".Examdate "exam_date"
	, "Exams"."Session"
	, "Exams"."Centre"
	, CASE WHEN RTRIM(LTRIM("Results_Clin"."PassFail"))='P' THEN 'P'
	   WHEN RTRIM(LTRIM("Results_Clin"."PassFail"))='F' THEN 'F'
	   WHEN RTRIM(LTRIM("Results_Clin"."PassFail"))='S' THEN 'S'
	   WHEN RTRIM(LTRIM("Results_Clin"."PassFail"))='N' THEN 'F'
	   ELSE 'F' END "PassFail"
	, CASE WHEN "Results_Clin"."ObsAdj" IS NULL THEN '' ELSE "Results_Clin"."ObsAdj" END "ObsAdj"
	, CASE WHEN  "Results_Clin"."MSCSAdj" IS NULL THEN '' ELSE "Results_Clin"."MSCSAdj" END "MSCSAdj"
	, CASE WHEN "Results_Clin"."SurAdj" IS NULL THEN '' ELSE "Results_Clin"."SurAdj" END "SurAdj"
	, CASE WHEN "Results_Clin"."MedAdj" IS NULL THEN '' ELSE "Results_Clin"."MedAdj" END "MedAdj"
	, "ExamTypes"."ID"
	, "ExamTypes"."sortorder"
	, CASE WHEN "Results_Clin"."PaedAdj" IS NULL THEN '' ELSE "Results_Clin"."PaedAdj" END "PaedAdj"
	, CASE WHEN "Results_Clin"."OGAdj" IS NULL THEN '' ELSE "Results_Clin"."OGAdj" END "OGAdj"
    FROM   ("CTS"."dbo"."Results_Clin" "Results_Clin" INNER JOIN "CTS"."dbo"."Exams" "Exams" ON "Results_Clin"."ExamID"="Exams"."ID")
	INNER JOIN "CTS"."dbo"."ExamTypes" "ExamTypes" ON "Exams"."ExamTypeID"="ExamTypes"."ID"
    WHERE "Results_Clin"."FileNo"=@FileNo AND
 ("ExamTypes"."ID"=6 OR "ExamTypes"."ID"=8)
 --ORDER BY "ExamTypes"."sortorder"


END
