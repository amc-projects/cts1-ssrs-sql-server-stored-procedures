USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptCert]    Script Date: 18/11/2020 10:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptCert] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT "Certs"."Certno", 
	CONVERT(varchar,"Certs"."Issuedate",103) "IssueDate"
    , "Certs"."Fileno"
    , CONVERT(varchar,"Certs"."Sentdate",103) "SentDate"
    , "Certs"."Comments"
    FROM   "CTS"."dbo"."Certs" "Certs"
	WHERE "Certs"."Fileno"=@FileNo
 --ORDER BY "vwCandidateTranscriptCA"."FileNo"



END
