USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscript]    Script Date: 18/11/2020 10:27:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscript] @FileNoList varchar(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #TempList
	(
		FileNo int
	)

	DECLARE @FileNo varchar(10), @Pos int

	SET @FileNoList = LTRIM(RTRIM(@FileNoList))+ ','
	SET @Pos = CHARINDEX(',', @FileNoList, 1)

	IF REPLACE(@FileNoList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @FileNo = LTRIM(RTRIM(LEFT(@FileNoList, @Pos - 1)))
			IF @FileNo <> ''
			BEGIN
				INSERT INTO #TempList (FileNo) VALUES (CAST(@FileNo AS int)) --Use Appropriate conversion
			END
			SET @FileNoList = RIGHT(@FileNoList, LEN(@FileNoList) - @Pos)
			SET @Pos = CHARINDEX(',', @FileNoList, 1)

		END
	END	

   SELECT "vwCandidateTranscriptMain"."FileNo"
   , UPPER('Dr '+"vwCandidateTranscriptMain"."Givennames"+' '+"vwCandidateTranscriptMain"."FamilyName") "Name"
   , "vwCandidateTranscriptMain"."Sex"
   , "vwCandidateTranscriptMain"."CtyBirth"
   , "vwCandidateTranscriptMain"."PhoneHome"
   , "vwCandidateTranscriptMain"."PhoneWork"
   , "vwCandidateTranscriptMain"."Givennames"
   , "vwCandidateTranscriptMain"."FamilyName"
   , "vwCandidateTranscriptMain"."City"
   , "vwCandidateTranscriptMain"."State"
   , "vwCandidateTranscriptMain"."PostCode"
    , ISNULL("vwCandidateTranscriptMain"."Address",'')+' '+ISNULL("vwCandidateTranscriptMain"."City",'')+' '+ISNULL("vwCandidateTranscriptMain"."State",'')+' '+ISNULL("vwCandidateTranscriptMain"."PostCode",'') "Address"
   , CONVERT(varchar,"vwCandidateTranscriptMain"."DateOfBirth",106) "DateOfBirth"
   , "vwCandidateTranscriptMain"."CountryResidence"
   , "vwCandidateTranscriptMain"."EligiCount"
   , "vwCandidateTranscriptMain"."MCQCount"
   , "vwCandidateTranscriptMain"."QualCount"
   , "vwCandidateTranscriptMain"."ClinDisStage1Count"
   , "vwCandidateTranscriptMain"."ClinStage2Count"
   , "vwCandidateTranscriptMain"."ClinStationCount"
   , "vwCandidateTranscriptMain"."WBACount"
   , "vwCandidateTranscriptMain"."SpecialistCount"
   , "vwCandidateTranscriptMain"."CertificateCount"
   , "vwCandidateTranscriptMain"."CACount"
   FROM   "CTS"."dbo"."vwCandidateTranscriptMain" "vwCandidateTranscriptMain"
   INNER JOIN #TempList t ON t.FileNo = "vwCandidateTranscriptMain"."FileNo"
END
