USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spTabletMarkSheetAssessmentDomain]    Script Date: 18/11/2020 3:29:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTabletMarkSheetAssessmentDomain] @ExamId INT, @FileNo INT, @ExaminerSortOrder int
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT  DISTINCT"vcmcdr"."Domain_List_Title"
 , "vcmcdr"."rating"
 , "vcmcdr"."ClinicalSlotCandidate_id"
 , "vcmcdr"."clinicalscenario_id"
 , "vcmcdr"."ScenarioDomains_Sortorder"
 , "vcmcdr"."cmcs_conceded_pass_at"
 FROM  "CTS"."dbo"."vwclinicalmarksheetcandidatescenarios" "vcmcs"
 INNER JOIN "CTS"."dbo"."vwResults_ClinicalStationClinicalSlotCandidates" "vrccsc" ON  "vrccsc"."ExamID"="vcmcs".ExamId  AND "vcmcs".FileNo = "vrccsc".FileNo AND "vrccsc".ClinicalSlotScenarios_Sortorder = "vcmcs".ExaminersSortOrder
 INNER JOIN  "CTS"."dbo"."vwclinicalmarksheetcandidatescenarioassessmentkeystepstatus" "vccsks" ON "vccsks".clinicalscenario_id = "vcmcs".clinicalscenarios_id AND "vccsks".ClinicalSlotCandidate_id = "vrccsc".ClinicalSlotCandidates_id
 INNER JOIN "CTS"."dbo"."vwclinicalmarksheetcandidatescenarioassessmentdomainratings" "vcmcdr" ON "vcmcdr".clinicalscenario_id = "vccsks".clinicalscenario_id AND "vcmcdr".ClinicalSlotCandidate_id =  "vccsks".ClinicalSlotCandidate_id 
 WHERE "vrccsc".ExamID = @ExamId AND "vrccsc".FileNo= @FileNo AND ExaminersSortOrder = @ExaminerSortOrder

END
