USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spCandidateResultTranscriptCA]    Script Date: 18/11/2020 10:29:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCandidateResultTranscriptCA] @FileNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   SELECT "vwCandidateTranscriptCA"."FileNo"
, "vwCandidateTranscriptCA"."PathwaySubTypeName"
, "vwCandidateTranscriptCA"."CAN_LMCCYNLabel"
, "vwCandidateTranscriptCA"."NZ_REXExamYNLabel"
, "vwCandidateTranscriptCA"."UK_PLABTestYNLabel"
, "vwCandidateTranscriptCA"."US_USMLEStepsYNLabel"
, "vwCandidateTranscriptCA"."Ireland_GradYNLabel"
, "vwCandidateTranscriptCA"."CAN_LMCCYearQ"
, "vwCandidateTranscriptCA"."NZ_REXExamYearQ"
, "vwCandidateTranscriptCA"."UK_PLAPTestYearQ"
, "vwCandidateTranscriptCA"."US_USMLEStepsYearQ"
, "vwCandidateTranscriptCA"."Ireland_GradYearQ"
, "vwCandidateTranscriptCA"."CAN_LMCCYearQLabel"
, "vwCandidateTranscriptCA"."NZ_REXExamYearQLabel"
, "vwCandidateTranscriptCA"."UK_PLAPTestYearQLabel"
, "vwCandidateTranscriptCA"."US_USMLEStepsYearQLabel"
, "vwCandidateTranscriptCA"."Ireland_GradYearQLabel"
, "vwCandidateTranscriptCA"."UK_GradYNLabel"
, "vwCandidateTranscriptCA"."UK_GradYearQLabel"
, "vwCandidateTranscriptCA"."UK_GradYearQ"
, "vwCandidateTranscriptCA"."NZ_InternEvidenceYNLabel"
, "vwCandidateTranscriptCA"."UK_FoundationYNLabel"
, "vwCandidateTranscriptCA"."UK_TrainingYNLabel"
, "vwCandidateTranscriptCA"."US_GMEResidencyYNLabel"
, "vwCandidateTranscriptCA"."Ireland_InternEvidenceYNLabel"
, "vwCandidateTranscriptCA"."NZ_InternEvidenceYearCLabel"
, "vwCandidateTranscriptCA"."UK_FoundationYearCLabel"
, "vwCandidateTranscriptCA"."UK_TrainingYearCLabel"
, "vwCandidateTranscriptCA"."US_GMEResidencyYearCLabel"
, "vwCandidateTranscriptCA"."Ireland_InternEvidenceYearCLabel"
, "vwCandidateTranscriptCA"."NZ_InternEvidenceYearC"
, "vwCandidateTranscriptCA"."UK_FoundationYearC"
, "vwCandidateTranscriptCA"."UK_TrainingYearC"
, "vwCandidateTranscriptCA"."US_GMEResidencyYearC"
, "vwCandidateTranscriptCA"."Ireland_InternEvidenceYearC"
, "vwCandidateTranscriptCA"."PathwaySubTypeId"
, "vwCandidateTranscriptCA"."CAN_LMCCYN"
, "vwCandidateTranscriptCA"."NZ_REXExamYN"
, "vwCandidateTranscriptCA"."UK_PLABTestYN"
, "vwCandidateTranscriptCA"."UK_GradYN"
, "vwCandidateTranscriptCA"."US_USMLEStepsYN"
, "vwCandidateTranscriptCA"."Ireland_GradYN"
, "vwCandidateTranscriptCA"."NZ_InternEvidenceYN"
, "vwCandidateTranscriptCA"."UK_FoundationYN"
, "vwCandidateTranscriptCA"."UK_TrainingYN"
, "vwCandidateTranscriptCA"."US_GMEResidencyYN"
, "vwCandidateTranscriptCA"."Ireland_InternEvidenceYN"
, "vwCandidateTranscriptCA"."ca_accredited_authorities_type"
, "vwCandidateTranscriptCA"."ca_accredited_authority_flag"
, CONVERT(varchar,"vwCandidateTranscriptCA"."ApplicationStateHistoryDate",103) "ApplicationStateHistoryDate"
, CONVERT(varchar,"vwCandidateTranscriptCA"."AdvStdIssueStateHistoryDate",103) "AdvStdIssueStateHistoryDate"
, CONVERT(varchar,"vwCandidateTranscriptCA"."MedicalBoardConfirmedStateHistoryDate",103) "MedicalBoardConfirmedStateHistoryDate"
 FROM   "CTS"."dbo"."vwCandidateTranscriptCA" "vwCandidateTranscriptCA"
 WHERE  "vwCandidateTranscriptCA"."FileNo"= @FileNo
 --ORDER BY "vwCandidateTranscriptCA"."FileNo"



END
