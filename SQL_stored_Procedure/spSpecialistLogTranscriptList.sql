USE [CTS]
GO
/****** Object:  StoredProcedure [dbo].[spSpecialistLogTranscriptList]    Script Date: 18/11/2020 3:17:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSpecialistLogTranscriptList] @FileNo int
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
    -- Insert statements for procedure here
	SELECT "SpecialistCollege"."CollegeName"
	, "Specialty"."SpecialityName"
	, CONVERT(varchar,"SpecialistLog"."Date",103) "Date"
	, "Candidates"."Fileno"
	, "Candidates"."GivenNames"
	, "Candidates"."FamilyName"
	, "SpecialistEval"."SpecEvalID"
	, "SpecLogReasons"."Description"
	, "SpecialistQual"."SpecQualType"
	, "SpecialistQual"."SpecQual"
	, "SpecialistLog"."Reason"
 FROM   ((((("CTS"."dbo"."Candidates" "Candidates"
 INNER JOIN "CTS"."dbo"."SpecialistEval" "SpecialistEval" ON "Candidates"."Fileno"="SpecialistEval"."FileNo")
 INNER JOIN "CTS"."dbo"."SpecialistLog" "SpecialistLog" ON "SpecialistEval"."SpecEvalID"="SpecialistLog"."SpecEvalID")
 INNER JOIN "CTS"."dbo"."SpecialistCollege" "SpecialistCollege" ON "SpecialistEval"."SpecCollCode"="SpecialistCollege"."SpecCollCode")
 INNER JOIN "CTS"."dbo"."Specialty" "Specialty" ON ("SpecialistEval"."SpecCollCode"="Specialty"."SpecCollCode") AND ("SpecialistEval"."SpecCode"="Specialty"."SpecCode"))
 INNER JOIN "CTS"."dbo"."SpecLogReasons" "SpecLogReasons" ON "SpecialistLog"."Description"="SpecLogReasons"."Description")
 LEFT OUTER JOIN "CTS"."dbo"."SpecialistQual" "SpecialistQual" ON "SpecialistLog"."SpecQualID"="SpecialistQual"."SpecQualID"
 WHERE "Candidates"."Fileno" = @FileNo 
-- ORDER BY "Candidates"."Fileno", "SpecialistCollege"."CollegeName", "SpecialistQual"."SpecQualType", "SpecialistLog"."Date"

END
